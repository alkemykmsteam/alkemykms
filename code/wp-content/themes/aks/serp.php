<?php
/*
Template Name: Serp
*/
@session_start();
get_header();
require_once ABSPATH . '/script/pdocrud.php';

//************************ GESTIONE RUOLI ********************************
if(isset($_SESSION['function'])){
    $roleId = $_SESSION['function'];
}else{
    $roleId = 0;
}
?>
    <script type="text/javascript">
        document.getElementById('menu-item-41').style.display = 'inline-block';
        document.getElementById('menu-item-42').style.left = '59%';
        if(document.getElementById('menu-item-146')!=null)
            document.getElementById('menu-item-146').style.display = 'none';
    </script>
<?php

$pdocrud = null;
if(isset($_SESSION["searched_pdocrud"])){
    //echo "isSet";
    $pdocrud = unserialize($_SESSION["searched_pdocrud"]);
    unset($_SESSION["searched_pdocrud"]);
}else{
    $pdocrud = new PDOCrud();
    $pdocrud->savePDOCrudObj();
    $pdocrud->currentPage(1);
    $pdocrud->dbTable("project_sheets");
    $pdocrud->handleOperation("CRUD", array());
}
echo $pdocrud->getJsonSearchSERP();
//$progetti->searchSERP($userFilters);
?>