<?php
/*
Template Name: indsearch
*/
require_once ABSPATH . '/script/classes/AutoCompleteManager.php';
$manager = new AutoCompleteManager();
$searchTerm = $_GET['term'];
//
$manager->openConnection();
$manager->setQueryParam($searchTerm);
$manager->makeIndustriesQuery();
echo $manager->getOutput();
?>