<?php
session_start();
require_once ABSPATH . '/script/pdocrud.php';
/*
Template Name: Load
*/

//Salvo i dati...
//$_SESSION['where'] = "where 1=1";
//prendo il ruolo dell'utente
$userRole = implode(', ', wp_get_current_user()->roles);
//echo $userRole;
//echo '-'.strcmp($userRole,'administrator').'-<br>';

switch($userRole){
    case AksRoles::Administrator:
        $_SESSION['function'] = AksRoles::AdministratorRoleId;
        break;
    case AksRoles::Vp:
        $_SESSION['function'] = AksRoles::VpRoleId;
        break;
    case AksRoles::Referent:
        $_SESSION['function'] = AksRoles::ReferentRoleId;
        break;
    case AksRoles::Employee:
        $_SESSION['function'] = AksRoles::EmployeeRoleId;
        break;
    case AksRoles::AksAdmin:
        $_SESSION['function'] = AksRoles::AksAdminRoleId;
        break;
}

/*
 * Privilegi:
 * 1 -> WP-Admin
 * 2 -> VP
 * 3 -> PR (Project Referent)
 * 4 -> Users
 * 5 -> AKS-Admin
 *
 */
header("location: /searchsheets");

?>