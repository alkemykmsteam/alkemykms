<?php
/*
Template Name: View Project
*/
$id = $_GET['id'];
$key = 'aks_v_'.$id;
$firstView = false;
if (!isset($_COOKIE['aks_v_'.$id])) {
    setcookie('aks_v_'.$id, 1);
    $_COOKIE['aks_v_'.$id] = 1;
    $firstView = true;
}

get_header();
require_once ABSPATH . '/script/pdocrud.php';

if(isset($_SESSION['function'])){
    $roleId = $_SESSION['function'];
}else{
    $roleId = 0;
}

?>
    <script type="text/javascript">
        document.getElementById('menu-item-41').style.display = 'inline-block';
        document.getElementById('menu-item-42').style.left = '59%';
        if(document.getElementById('menu-item-146')!=null)
            document.getElementById('menu-item-146').style.display = 'none';
    </script>
<?php
//work in progress


$mrWolf = new AlfrescoAttachmentsRecover();
$mrWolf->initListFiles($id);
$pdocrud = new PDOCrud();
$pdocrud->manageViewCount($id);
$pdocrud->manageRole($roleId);
$pdocrud->setUploadedAttachments($mrWolf->getAttachments());
//$pdocrud->setSettings("viewBackButton", false);
//$pdocrud->setSettings("viewEditButton", false);
//$pdocrud->setSettings("viewDelButton", false);
//$pdocrud->setSettings("viewBackButton", false);
$pdocrud->viewSheetConfiguration();
$pdocrud->visualizeVIEW($id);
get_footer();
?>