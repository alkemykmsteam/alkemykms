<?php

/*
Template Name: Dashboard
*/

get_header();
require_once ABSPATH . '/script/pdocrud.php';


$pdocrud = new PDOCrud();

$totalProjects = $pdocrud->getTotalProjects();

$views = $pdocrud->getViewsGroupByMonths();
$downloads = $pdocrud->getDownloadsGroupByMonths();

$viewData ='';
for ($i = 0; $i < count($views); ++$i) {
    $viewData=$viewData.$views[$i]['views'];
    if($i!=count($views)-1){
        $viewData=$viewData.',';
    }
}
$viewLabels ='"';
for ($i = 0; $i < count($views); ++$i) {
    $viewLabels=$viewLabels.$views[$i]['months'];
    if($i!=count($views)-1){
        $viewLabels=$viewLabels.'","';
    }
}
$viewLabels =$viewLabels.'"';
$viewChartLabel= '"Projects Visualizzations"';
$viewChartText= '"Visualizzations order by date"';

$downloadData ='';
for ($i = 0; $i < count($downloads); ++$i) {
    $downloadData=$downloadData.$views[$i]['views'];
    if($i!=count($downloads)-1){
        $downloadData=$downloadData.',';
    }
}
$downloadLabels ='"';
for ($i = 0; $i < count($downloads); ++$i) {
    $downloadLabels=$downloadLabels.$downloads[$i]['months'];
    if($i!=count($downloads)-1){
        $downloadLabels=$downloadLabels.'","';
    }
}
$downloadLabels =$downloadLabels.'"';
$downloadChartLabel= '"Projects Downloads"';
$downloadChartText= '"Downloads order by date"';

?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<table width="100%">
    <tr>
        <td style="padding-left: 2%;" align="center" colspan="2"><b># projects: <a href="/load"><?php echo $totalProjects; ?></a></b></td>
    </tr>
    <tr>
        <td style="padding-left: 2%;" align="center"><canvas id="chart-views" role="img" width="500" height="300" style="display:block;"></canvas></td>
        <td align="center"><canvas id="chart-dwnlds" role="img" width="500" height="300" style="display:block;"></canvas></td>
    </tr>
</table>

<script>
    new Chart(document.getElementById("chart-views"), {
        type: 'bar',
        data: {
            labels: [<?php echo $viewLabels; ?>],
            datasets: [
                {
                    label: <?php echo $viewChartLabel; ?>,
                    //backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                    data: [<?php echo $viewData; ?>]
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: <?php echo $viewChartText; ?>
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            },
            responsive: false
        }
    });
    new Chart(document.getElementById("chart-dwnlds"), {
        type: 'bar',
        data: {
            labels: [<?php echo $downloadLabels; ?>],
            datasets: [
                {
                    label: <?php echo $downloadChartLabel; ?>,
                    //backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                    data: [<?php echo $downloadData; ?>]
                }
            ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: <?php echo $downloadChartText; ?>
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            },
            responsive: false
        }
    });
</script>

<?php
get_footer();
?>

