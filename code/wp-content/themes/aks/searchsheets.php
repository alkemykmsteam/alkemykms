<?php
/*
Template Name: Search Sheet
*/
get_header();
require_once ABSPATH . '/script/pdocrud.php';
$progetti = new PDOCrud();//create object of PDOCrud Class
//
//************************ GESTIONE RUOLI ********************************
if(isset($_SESSION['function'])){
    $roleId = $_SESSION['function'];
}else{
    $roleId = 0;
}
$progetti->manageRole($roleId);
if ($roleId == 3 || $roleId == 2 || $roleId == 1) {
    ?>
    <script>
        jQuery(document).ready(function() {
            $('#menu-item-146').html('<a>Add project</a>');
            $('#menu-item-146').on('click',function(){
                $('.pdocrud-actions.pdocrud-button.pdocrud-button-add').click()});
        });
    </script>
    <script type="text/javascript">
        document.getElementById('menu-item-41').style.display = 'none';
    </script>
    <?php
}else {
    ?>
    <script type="text/javascript">
        document.getElementById('menu-item-41').style.display = 'none';
 		if(document.getElementById('menu-item-146')!=null)
        	document.getElementById('menu-item-146').style.display = 'inline-block';
    </script>
<?php
}



//setto la mail ricavata dalla sessione SOLO per la pagina di Nuovo Progetto
$progetti->setMailUser(wp_get_current_user()->user_email);
//
//************************ PAGINA RICERCA ********************************
$progetti->searchConfiguration();
//************************************************************************
//************************ PAGINA NEW SHEET ******************************
$progetti->newSheetConfiguration();

//************************************************************************
//************************ PAGINA VIEW ***********************************
$progetti->viewSheetConfiguration();
//************************************************************************
$progetti->visualizeCRUD();
//$progetti->createProjectFolder("prova");
//$progetti->createAksAttachmentsDocument("DemoAjeje.txt");
get_footer();
?>
<script type="text/javascript">
    document.getElementById('filterContainer').style.display='block';
</script>




