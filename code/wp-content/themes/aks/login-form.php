<?php

/*
Template Name: AKS Login
*/
?>
<head>
	<title></title>
	<link rel="stylesheet" href="https://aks.alkemylab.it/wp-content/themes/twentyseventeen/style.css"  type="text/css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style type="text/css">
		html {
			width: 100%;
		}
		body:before, body:after {
			position: relative;
		}
		body {
			background-clip: border-box;
			background-color: rgba(0, 0, 0, 0);
			background-image: url('https://aks.alkemylab.it/wp-content/uploads/2018/09/sfondo.jpg');
			background-origin: padding-box;
			background-repeat: no-repeat;
			background-size: cover;
			background-position: center;
			margin: 0;
			width: 100%;
			height: 100%;
		}
		.splash-container {
			display: table;
			max-width: 767px;
			margin: 0 auto;
			position: relative;
			text-align: center;
			width: 100%;
		}
		.splash-content {
			display: table-cell;
			vertical-align: middle;
		}
		.splash-text {
			color: #fff;
			font-size: 18px;
			font-weight: bold;
			line-height: 1;
		}
		.splash-button-area {
			margin: 30px 0 0;
		}
		.splash-button {
			cursor: default;
			color: #000;
			font-size: 20px;
			background: black;
			font-weight: bold;
			line-height: 1;
			display: inline-block;
			border-radius: 5px;
			padding: 20px;
		}

		.splash-img-logo-KS {
			margin-left: 27%;
			width: 45%;
			padding: 1% 10% 5%;
		}


		.splash-img-logoA {
			margin-left: 33%;
			width: 34%;
			padding: 9% 10% 0%;
		}




		@media screen and (max-width: 1200px) {

		}

	</style>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="wp-content/plugins/very-simple-splash-page/assets/js/jquery.vide.js"></script>
</head>
<body >
<div class="splash-container">
	<div class="splash-content">
		<div class="splash-img-logoA">
			<img src="/wp-content/uploads/2018/09/Alkemy_logo@white.png">
		</div>
		<div class="splash-img-logo-KS">
			<img src="/wp-content/uploads/2018/09/KS_long@white.png">
		</div>
		<div class="splash-text">
		</div>
		<div class="splash-button"
			<a class="splash-button" href="http://aks.alkemylab.it/">
				<?php echo do_shortcode("[mo_oauth_login]"); ?>
			</a>
	</div>
</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		var h = $(window).height();
		$('.splash-container').height(h);

		$(window).resize(function(){
			var h = $(window).height();
			$('.splash-container').height(h);
		})
		$('.splash-button').click(function(e){
			e.preventDefault();
			console.log('clicked');
			window.location.reload(true);
		})
	});
</script>
</body>
