<?php
/*
Template Name: Privacy
*/
get_header();
?>
    <script type="text/javascript">
        document.getElementById('menu-item-41').style.display = 'inline-block';
        document.getElementById('menu-item-42').style.left = '59%';
        if(document.getElementById('menu-item-146')!=null)
            document.getElementById('menu-item-146').style.display = 'none';
    </script>
        <div align="left" style="margin-left: 3%;"><br />
            <div>
            INVESTOR RELATIONS<br />
            <br />
            CORPORATE GOVERNANCE<br />
            ITENES<br />
            WE STAND AT THE INTERSECTION OF BUSINESS, TECHNOLOGY AND CREATIVITY.<br />
            BENVENUTO IN ALKEMY<br />
            <br />
            La nostra mission è accompagnare le aziende attraverso le rapide trasformazioni portate dal digitale, con l’obiettivo di contribuire significativamente alla crescita del loro business. Aiutiamo le aziende a ridefinire strategie, prodotti e servizi, strumenti di comunicazione e di vendita, coerentemente con l’evoluzione delle tecnologie e dei nuovi comportamenti dei consumatori.<br />
            <br />
            Uniamo le migliori competenze esistenti sul mercato, aggregando business di eccellenza nel digitale: Advisory, eCommerce, Creativity &amp; Brand Strategy, Content, Social, UX &amp; Design, Digital Transformation, Media &amp; Performance, Technology, Big Data &amp; Advanced Analytics.<br />
            <br />
            Nel corso del 2013 abbiamo acquisito Seolab, società leader nelle attività SEO e performance marketing e nel 2016 BizUp, web marketing agency con ulteriori competenze specialistiche quali il content marketing applicato alla performance.<br />
            <br />
            Nel dicembre 2014 dall’acquisizione di TSC Consulting è nata Alkemy Tech, una delle maggiori società italiane di tecnologie per la digital transformation. Alkemy Lab, stream di innovazione con focus su progetti di ricerca e prototipazione in ambito mobile, big data, smart object e IoT, ne completa il conferimento.<br />
            <br />
            A febbraio 2017 è nata Alkemy Play, realtà che, attraverso la sua piattaforma di local digital marketing, supporta le grandi aziende a sviluppare un’offerta mirata a PMI e/o a reti commerciali e di franchising.<br />
            <br />
            A marzo 2018 abbiamo acquisito Nunatac, società italiana specializzata nell’analisi dei dati al servizio delle aziende – data analysis, big data e predictive modelling – con 25 anni di esperienza progettuale nell’ambito degli advanced analytics.<br />
            <br />
            Dal dicembre 2017, con la quotazione al mercato AIM di Borsa Italiana, Alkemy è diventata la prima Public Company italiana nel mondo dei servizi alle aziende.<br />
            <br />
            LA NOSTRA OFFERTA<br />
            STRATEGY<br />
            Digital Check-up<br />
            Digital_enabling Strategy<br />
            Digital Organization<br />
            Education &amp; Coaching<br />
            Omnichannel Strategy<br />
            Shopper Marketing<br />
            eCommerce Entry Strategy<br />
            eCommerce Full Potential<br />
            New Product and Service Design<br />
            Innovation Gateway<br />
            Digital/Social CRM &amp; Big Data<br />
            <br />
            COMMERCE<br />
            Design &amp; Content production<br />
            Technology &amp; system Integration<br />
            Customer Care and Financial services<br />
            Logistics &amp; Distribution<br />
            Omnichannel operation<br />
            Internationalization<br />
            Online store management<br />
            Data Analytics &amp; CRM<br />
            <br />
            COMMUNICATION<br />
            CREATIVITY &amp; BRAND STRATEGY<br />
            Cross channel campaigns (ATL, BTL, digital)<br />
            Video Strategy &amp; Production<br />
            Events &amp; Activations<br />
            SOCIAL MEDIA &amp; DIGITAL PR<br />
            Social Media Strategy and Management<br />
            Social Care, Digital Reputation &amp; Digital PR<br />
            Digital Monitoring &amp; Intelligence<br />
            CONTENT<br />
            Branded &amp; Unbranded Content<br />
            Corporate Content<br />
            Native Ads<br />
            UX, DESIGN &amp; DEVELOPMENT<br />
            User Experience &amp; User Interface Design<br />
            Websites, Web and Mobile App<br />
            Service Design<br />
            <br />
            PERFORMANCE<br />
            MEDIA<br />
            Integrated Channel Planning<br />
            Digital Marketing Mix<br />
            Media Strategy Optimization<br />
            Programmatic Buying<br />
            Media Buying Across Channel<br />
            Data Analytics &amp; Insights development<br />
            PERFORMANCE MARKETING<br />
            SEM/SEO<br />
            Affiliation<br />
            Retargeting<br />
            Content Distribution &amp; Optimization<br />
            Social &amp; Viral Marketing<br />
            <br />
            TECH<br />
            Omnichannel Solutions<br />
            Multi-screen Technologies<br />
            Internet of Things<br />
            CRMs<br />
            CMSs<br />
            Portals<br />
            Apps<br />
            <br />
            DATA &amp; ANALYTICS<br />
            Advanced Analytics &amp; Predictive Modeling<br />
            Real time Next Best Action<br />
            Digital Customer Intelligence<br />
            Customer Experience Optimization<br />
            Data Environment Design &amp; Implementation<br />
            I NOSTRI VALORI<br />
            ECCELLENZA<br />
            in tutto quello che facciamo, con cura e qualità assoluta anche nel minimo dettaglio.<br />
            PASSIONE<br />
            per il nostro lavoro, per le nostre persone e, soprattutto, per i nostri clienti.<br />
            INTEGRITÀ<br />
            nel nostro comportamento e nel rispetto dei nostri valori.<br />
            CONCRETEZZA<br />
            nell'ottenere risultati concreti e duraturi nel tempo.<br />
            CODICE ETICO ALKEMY S.P.A. MODELLO ORGANIZZATIVO 231 ALKEMY S.P.A. CODICE ETICO ALKEMY TECH S.R.L. MODELLO ORGANIZZATIVO 231 ALKEMY TECH S.R.L.<br />
            I NOSTRI CLIENTI<br />
            <br />
            CONTATTI<br />
            Alkemy digital_enabler ®<br />
            <br />
            HEADQUARTERS<br />
            <br />
            Via San Gregorio, 34<br />
            20124 Milano, Italy<br />
            <br />
            info@alkemy.com<br />
            alkemy@pec.it<br />
            I NOSTRI UFFICI<br />
            <br />
            Milano Torino Cagliari Roma Prati Roma Ostiense Cosenza Madrid Belgrado<br />
            Seguici<br />
            <br />
            TWITTER<br />
            <br />
            twitter.com/alkemy<br />
            LINKEDIN<br />
            <br />
            linkedin.com/alkemy<br />
            FACEBOOK<br />
            <br />
            facebook.com/alkemy<br />
            Vuoi lavorare con noi?<br />
            <br />
            Siamo sempre alla ricerca di nuovi talenti per crescere e migliorarci.<br />
            <br />
            cv@alkemy.com<br />
            © Alkemy SpA - Sede Legale Via San Gregorio, 34 - 20124 Milano, Italy c.f e p.i. 05619950966 reg. delle imprese di Milano - Cap. Soc. 573.861 euro i.v.<br />
            PRIVACY POLICY<br />
            INFORMATIVA PRIVACY<br />
            <br />
            INFORMATIVA AI SENSI DELL'ART. 13 DEL D.LGS 30 GIUGNO 2003 N. 196 (“CODICE”) E DEL REGOLAMENTO UE 679/2016 (“GDPR”).<br />
            <br />
            Nell'osservanza del Codice e del GDPRS, Alkemy S.p.A., come meglio individuata in seguito, in qualità di "Titolare" del trattamento, Le fornisce alcune informazioni circa l'utilizzo degli eventuali dati personali conferiti dagli utenti che consultano e/o interagiscono con il sito.<br />
            <br />
            FINALITÀ DEL TRATTAMENTO DEI DATI<br />
            <br />
            L'utente è libero di fornire i suoi dati personali per l'invio del proprio curriculum vitae, per sollecitare l'invio di materiale informativo o di altre comunicazioni. Il trattamento dei dati personali potrà essere effettuato unicamente per rispondere alle richieste pervenute, ai sensi dell’art. 6.1, lett. b) del GDPR. Il conferimento di tali dati è facoltativo, ma il mancato conferimento degli stessi può comportare l'impossibilità di ottenere quanto richiesto.<br />
            <br />
            MODALITÀ DI TRATTAMENTO DEI DATI<br />
            <br />
            Il trattamento dei dati personali avviene mediante strumenti manuali, informatici e telematici e in modo da garantire la sicurezza e la riservatezza dei dati stessi. Vengono utilizzati sistemi di prevenzione e protezione, costantemente aggiornati e verificati in termini di affidabilità.<br />
            <br />
            CATEGORIE DI SOGGETTI AI QUALI I DATI POSSONO ESSERE COMUNICATI<br />
            <br />
            Per il perseguimento delle finalità di cui sopra, Alkemy SpA necessita di comunicare i dati personali ai propri "incaricati" ed a soggetti esterni "responsabili". L’elenco completo dei Responsabili è disponibile presso la sede del Titolare.<br />
            <br />
            TITOLARE DEL TRATTAMENTO<br />
            <br />
            Titolare del trattamento è Alkemy S.p.A., con sede legale in 20124 Milano, via San Gregorio 34, C.F. e P.I.V.A. 05619950966.<br />
            <br />
            DIRITTI DEGLI INTERESSATI<br />
            <br />
            Ai sensi dell’art. 7 del Codice Privacy e degli artt. 15 e ss. del GDPR, Lei ha il diritto di ottenere:<br />
            1. la conferma dell'esistenza o meno di dati personali che La riguardano, anche se non ancora registrati, e la loro comunicazione in forma intelligibile;<br />
            2. una copia dei Suoi dati personali;<br />
            3. la rettifica dei Suoi dati personali eventualmente inesatti;<br />
            4. la cancellazione dei Suoi dati personali;<br />
            5. la limitazione del trattamento dei Suoi dati personali;<br />
            6. in un formato strutturato, di uso comune e leggibile da dispositivo automatico i dati personali che Lei ci ha fornito o che Lei stesso ha creato – esclusi i giudizi creati dal Titolare e/o dagli incaricati ex art. 4 del Codice Privacy / dalle persone autorizzate a trattare i dati a nome e per conto del Titolare ex art. 4 del GDPR - e di trasmetterli, direttamente o per mezzo del Titolare, ad un altro titolare del trattamento;<br />
            7. l'indicazione:<br />
            a) dell'origine dei dati personali;<br />
            b) delle categorie di dati personali trattati;<br />
            c) delle finalità e modalità del trattamento;<br />
            d) della logica applicata in caso di trattamento effettuato con l'ausilio di strumenti elettronici;<br />
            e) degli estremi identificativi del Titolare e degli eventuali responsabili;<br />
            f) del periodo di conservazione dei Suoi dati personali o dei criteri utili per la determinazione di tale periodo;<br />
            g) dei soggetti o delle categorie di soggetti ai quali i dati personali possono essere comunicati o che possono venirne a conoscenza in qualità di rappresentante designato nel territorio dello Stato, di responsabili o di incaricati ex art. 4 del Codice Privacy / persone autorizzate a trattare i dati a nome e per conto del Titolare ex art. 4 del GDPR;<br />
            h) l'aggiornamento, la rettificazione ovvero, quando Vi ha interesse, l'integrazione dei dati;<br />
            i) la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge, compresi quelli di cui non è necessaria la conservazione in relazione agli scopi per i quali i dati sono stati raccolti o successivamente trattati;<br />
            j) l'attestazione che le operazioni di cui alle lettere a) e b) sono state portate a conoscenza, anche per quanto riguarda il loro contenuto, di coloro ai quali i dati sono stati comunicati o diffusi, eccettuato il caso in cui tale adempimento si rivela impossibile o comporta un impiego di mezzi manifestamente sproporzionato rispetto al diritto tutelato.<br />
            8. Lei, inoltre, ha diritto di opporsi, in tutto o in parte:<br />
            a) per motivi legittimi, al trattamento dei dati personali che La riguardano, ancorché pertinenti allo scopo della raccolta;<br />
            b) al trattamento di dati personali che La riguardano a fini di invio di materiale pubblicitario o di vendita diretta o per il compimento di ricerche di mercato o di comunicazione commerciale.<br />
            Per esercitare i suddetti diritti, può inviare una comunicazione all’indirizzo di posta elettronica privacy@alkemy.com indicando in oggetto "Privacy - esercizio dei diritti ex art. 7 del D.Lgs. 196/2003 ed ex artt. 15 e ss. del GDPR".<br />
            La informiamo, da ultimo, che qualora ritenga che i Suoi diritti siano stati violati dal Titolare e/o da un terzo, Lei ha il diritto di proporre reclamo al Garante per la Protezione dei Dati Personali e/o ad altra autorità di controllo competente in forza del GDPR.<br />
            <br />
            CHE COSA SONO I COOKIE?<br />
            <br />
            I cookie sono piccoli file di testo che i siti visitati dagli utenti inviano ai loro terminali, ove vengono memorizzati per essere poi ritrasmessi agli stessi siti alla visita successiva. I cookie delle c.d. “terze parti” vengono, invece, impostati da un sito web diverso da quello che l’utente sta visitando e possono essere installati quando il sito web includa componenti e/o contenuti forniti dinamicamente da terze parti. Questo perché su ogni sito possono essere presenti elementi (immagini, mappe, suoni, specifici link a pagine web di altri domini, ecc.) che risiedono su server diversi da quello del sito visitato.<br />
            <br />
            Il nostro sito utilizza le seguenti tipologie di cookie:<br />
            COOKIE TECNICI<br />
            <br />
            Questi cookie sono indispensabili al funzionamento del sito.<br />
            <br />
            COOKIE DI PROFILAZIONE E PUBBLICITARI DI TERZE PARTI<br />
            <br />
            Questi cookie sono installati da soggetti terzi e l’installazione degli stessi richiede il tuo consenso; in mancanza gli stessi non saranno installati. Ti riportiamo quindi di seguito i link alle informative privacy delle terze parti dove potrai esprimere il tuo consenso all’installazione di tali cookie evidenziando che, laddove non effettuassi alcuna scelta e decidessi di proseguire comunque con la navigazione all’interno del presente sito web, acconsentirai all’uso di tali cookie.<br />
            <br />
            GOOGLE ANALYTICS<br />
            <br />
            Privacy Policy: http://www.google.com/policies/<br />
            Tecnico: Sì<br />
            Tipologia: Analitiche<br />
            Profilazione: No<br />
            Si specifica che le informazioni acquisite dai cookie sopra individuati sono preventivamente anonimizzate prima dell’invio dei dati stessi alla terza parte proprietaria. Si tratta di informazioni che, pertanto, non consentono di identificare personalmente gli utenti.<br />
            <br />
            GESTIRE LE PREFERENZE<br />
            <br />
            In aggiunta a quanto sopra ti informiamo che la maggior parte dei browser consentono di gestire le preferenze relative ai cookie, ma questo sito funziona in modo ottimale con tutte le tipologie di cookie abilitate. Le impostazioni riguardanti i cookie possono essere controllate e modificate dalle preferenze del browser. I seguenti link mostrano come è possibile impostare le preferenze sui browser utilizzati più comunemente:<br />
            <br />
            Internet Explorer (http://windows.microsoft.com/en-US/windows-vista/Block-or-allow-cookies)<br />
            Firefox (http://support.mozilla.com/en-US/kb/Enabling+and+disabling+cookies)<br />
            Safari (http://support.apple.com/kb/index?page=search&amp;fac=all&amp;q=cookies%20safari)<br />
            Chrome (http://www.google.com/support/chrome/bin/answer.py?hl=en&amp;answer=95)
            </div>
        </div>
<?php
get_footer();
?>