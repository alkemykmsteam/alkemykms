<?php
/*
Template Name: ContactUs
*/
get_header();
?>
<script type="text/javascript">
    document.getElementById('menu-item-41').style.display = 'inline-block';
    document.getElementById('menu-item-42').style.left = '59%';
    if(document.getElementById('menu-item-146')!=null)
        document.getElementById('menu-item-146').style.display = 'none';
</script>
<?php

echo do_shortcode('[contact-form-7 id="188" title="Contact form 1"]');

get_footer();
?>