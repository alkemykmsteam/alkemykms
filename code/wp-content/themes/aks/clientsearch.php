<?php
/*
Template Name: clsearch
*/
require_once ABSPATH . '/script/classes/AutoCompleteManager.php';
//$pdocrud = new PDOCrud();
//// Database configuration
//$dbHost     = $pdocrud->getSettings("hostname");
//$dbUsername = $pdocrud->getSettings("username");
//$dbPassword = $pdocrud->getSettings("password");
//$dbName     = $pdocrud->getSettings("database");
//
//// Connect with the database
//$db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);
//if($db->connect_error) {
//    echo 'Database connection failed...' . 'Error: ' . $db->connect_errno . ' ' . $db->connect_error;
//    exit;
//} else {
//    $db->set_charset('utf8');
//}
//// Get search term
//$searchTerm = $_GET['term'];
//// Get matched data from skills table
//$query = $db->query("SELECT id, name FROM clients WHERE name LIKE '%".$searchTerm."%' ORDER BY name ASC");
//// Generate skills data array
//$skillData = array();
//if($query->num_rows > 0){
//    while($row = $query->fetch_assoc()){
//        $data['id'] = $row['id'];
//        $data['value'] = $row['name'];
//        array_push($skillData, $data);
//    }
//}
//// Return results as json encoded array
//echo json_encode($skillData);
//flush();
//$db->close();


$manager = new AutoCompleteManager();
$searchTerm = $_GET['term'];
//
$manager->openConnection();
$manager->setQueryParam($searchTerm);
$manager->makeClientQuery();
echo $manager->getOutput();
?>