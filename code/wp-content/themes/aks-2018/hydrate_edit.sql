select team_id as id, CONCAT(UPPER(tm.cognome_nome),' - ', LOWER(tm.mail)) AS NAME, "team" AS type_ from team_members tm
UNION ALL
select distinct UPPER(tag) as id, CONCAT(UCASE(LEFT(lower(tag), 1)),SUBSTRING(lower(tag), 2)) as NAME, "tag" as type_ from mydb.projects_tags
UNION ALL
SELECT DISTINCT c.id as id, CONCAT(UCASE(LEFT(lower(c.name), 1)),SUBSTRING(lower(c.name), 2)) AS NAME, "client" AS type_ FROM clients c
UNION ALL
SELECT DISTINCT team_id as id, UPPER(CONCAT(cognome_nome, ' [', job_title, ']')) AS NAME, "client_owner" AS type_ from team_members where vp_il = 'Y'
UNION ALL
SELECT DISTINCT ind.id as id, CONCAT(UCASE(LEFT(lower(ind.name), 1)),SUBSTRING(lower(ind.name), 2)) AS NAME, "industry" AS type_ FROM industries ind
UNION ALL
select 1 as id, 'Italy' as country, "country" as type_ from dual
union all
select 2 as id, 'Mexico' as country, "country" as type_ from dual
union all
select 3 as id, 'Other' as country, "country" as type_ from dual
union all
select 4 as id, 'South Eastern Europe' as country, "country" as type_ from dual
union all
select 5 as id, 'Spain' as country, "country" as type_ from dual
UNION ALL
select distinct s.id as id, CONCAT(UCASE(LEFT(lower(s.name), 1)),SUBSTRING(lower(s.name), 2)), "skills" as type_ from skills s
ORDER BY type_, NAME