<?php
?>
</main>
</div><!-- L-page -->
    <?php  get_sidebar() ?>
	<?php wp_footer(); ?>
	
	<?php if(function_exists('get_field') && get_field('html_footer', 'option')):
        the_field('html_footer', 'option'); 
    endif; ?>

</body>
</html>