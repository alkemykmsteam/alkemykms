<?php 
/* Template Name: Search Projects */
session_start();
require_once ABSPATH . '/script/pdocrud.php';
$projects = null;
if(isset($_SESSION["pdocrud_obj"])) {
    $projects = unserialize($_SESSION["pdocrud_obj"]);
}else {
    $projects = new PDOCrud();
}
$aksFunctions = $_SESSION['$aksFunctions'];
// TODO sposare in load
$myLakePages = $projects->loadMyLakePages();
$_SESSION['myLakePages'] = $myLakePages;
$user_avatar = null;
//se è settato salvo l'avatar dell'utente
if(isset($_SESSION['g_avatar_url']) && !isset($_SESSION['avatarUpdated'])) {
    $projects->saveUserAvatar(wp_get_current_user()->ID, $_SESSION['g_avatar_url']);
    //con questa variabile lo salvo solo al primo caricamento della pagina
    $_SESSION['avatarUpdated'] = true;
}
get_header();
?>
<ul class="ui-autocomplete--history" id="j-search-history" tabindex="0" style="display:none;">
    <div class="c-autocomplete-items">
        <li class="c-autocomplete-item u-template">
            <div class="c-autocomplete-item__label" data-value="">
                <span class="c-autocomplete-item__type">latest</span>
            </div>
        </li>
    </div><a href="#" class="j-advanced-search-toggle" id="j-advanced-search-toggle">Advanced Search</a>
</ul>
<header class="c-hero l-container">
    <div class="c-hero__content">
        <div class="c-hero__content__text">
            <h2 class="c-hero__content__text__lead t-title3">Hi <?php echo wp_get_current_user()->user_firstname;?> <?php echo wp_get_current_user()->user_lastname;?>,</h2>
            <h1 class="c-hero__content__text__title t-title1">Welcome to <?php echo get_bloginfo('name');?></h1>
<!--            <h1 class="c-hero__content__text__title t-title1">Welcome to <font color="yellow">my</font> lake</h1>-->
        </div>
        <div class="c-search">
            <form class="c-form" id="j-searchform" method="POST" action="<?php echo $myLakePages['serp_url']?>">
                <fieldset class="c-form__fieldset">
                    <div class="c-form__field j-search">
                        <input placeholder="Search client, project or tag" type="text" id="j-search-fulltext" name="searchterm">

                        <a href="#" id="j-searchform__submit"><span class="icon icon-search"></span></a>
                    </div>
                </fieldset>
            </form>
            
        </div>
    </header>

    <section class="c-content-block l-container">
        <div class="c-content-block__content">
        <form class="c-form">
            <fieldset class="c-form__fieldset">
                <div class="c-current-query">
                    Discover all projects developed for 
                    
                        <select class="c-select j-current-query" name="select_client" data-placeholder="Our clients">
                            <option value="all">Our clients</option>
                        </select>
            
                    in 
                    
                        <select class="c-select j-current-query" name="select_industry" data-placeholder="Every industry">
                            <option value="all">Every industry</option>
                        </select>
                    
                    across
                    
                        <select class="c-select j-current-query" name="select_country" data-placeholder="Every country">
                            <option value="all">Every country</option>
                        </select>

                    by

                        <select class="c-select j-current-query" name="select_skills" data-placeholder="Every skill">
                            <option value="all">Every skill</option>
                        </select>
                </div>
            </fieldset>
        </form>
        <div class="c-spinner">
            <img src="<?php echo get_bloginfo('template_url');?>/img/loading.png" alt="">
        </div>
        <div class="c-projects" style="opacity:0;">
            <a href="#" class="c-project-item u-template">
                <figure class="c-project-item__figure">
                </figure>
                <header class="c-project-item__header">
                    <h3 class="c-project-item__header__title t-title4"></h3>
                    <h4 class="c-project-item__header__lead t-lead2"></h4>
                </header>
                <footer class="c-project-item__footer">
                    <p></p>
                </footer>
            </a>
            
            
        </div>
                
    </div>
    </section>
    


        </main>
        
        <div class="l-footer">
                <footer class="c-site-footer">
                    <div class="l-container">
                    
                    </div>
                </footer>
            </div>
        </div>
     

        <div id="advanced-search" class="c-modal">
        <div class="c-advanced-search">
            <header class="c-advanced-search__header">
                <h2 class="c-advanced-search__header__title t-title3">
                    Advanced search
                </h2>
                <a href="#" class="c-close j-modal-close">
                <span class="icon icon-close">

                        
                </span>
                    </a>
            </header>

 
            <form class="c-form" id="j-searchform--advanced" method="POST" action="<?php echo $myLakePages['serp_url']?>">
                <input type="hidden" name="project_name" id="hidden-project_name">
                <input type="hidden" name="tag" id="hidden-tag">
                <input type="hidden" name="client" id="hidden-client_name">
                <input type="hidden" name="industry" id="hidden-client_industry">
                <input type="hidden" name="country" id="hidden-country">
                <input type="hidden" name="skills" id="hidden-skills">
                <input type="hidden" name="start_date" id="hidden-start_date">
                <input type="hidden" name="end_date" id="hidden-end_date">
                <input type="hidden" name="team" id="hidden-team">
                <input type="hidden" name="client_owner" id="hidden-client_owner">

                <div class="c-form__content l-2col">
                    <div class="c-form__field c-multiple-pillbox">
                        <label>Project name</label>
                     
                        <select class="j-multiple-pillbox" name="select_project_name" multiple="multiple" data-targetfield="project_name" data-placeholder="Add a word related to the project">
                        </select>
                    
                    </div>
                    <div class="c-form__field c-multiple-pillbox">
                        <label>Project tag</label>
                     
                        <select class="j-multiple-pillbox" name="select_tag" multiple="multiple" data-targetfield="tag" data-placeholder="Add one or more tags related">
                        </select>
                    
                    </div>
                    <div class="c-form__field c-form__field--yellow">
                        <div class="c-form__field-inline c-multiple-select">
                            <h3 class="c-multiple-select__title">Clients <span class="icon">v</span></h3>
                            <div class="c-select-container">
                                <select class="c-select j-multiple-select" name="select_client" data-targetfield="client_name" data-placeholder="All clients" data-datatype="Clients">
                                </select>
                            </div>
                        </div> 


                        <div class="c-form__field-inline c-multiple-select">
                            <h3 class="c-multiple-select__title">Industries <span class="icon">v</span></h3>
                            <div class="c-select-container">
                                <select class="c-select j-multiple-select" name="select_industry" data-targetfield="client_industry" data-placeholder="All industries" data-datatype="Industries">
                                </select>
                            </div>
                        </div> 
                    </div>
                    <div class="c-form__field c-form__field--yellow">
                        <div class="c-form__field-inline c-multiple-select">
                            <h3 class="c-multiple-select__title">Countries <span class="icon">v</span></h3>
                            <div class="c-select-container">
                                <select class="c-select j-multiple-select" name="select_country" data-targetfield="country" data-placeholder="All countries" data-datatype="Countries">
                                </select>
                            </div>
                        </div> 
                        <div class="c-form__field-inline c-multiple-select">
                            <h3 class="c-multiple-select__title">Business unit <span class="icon">v</span></h3>
                            <div class="c-select-container">
                                <select class="c-select j-multiple-select" name="select_skills" data-targetfield="skills" data-placeholder="All business unit" data-datatype="Business units">
                                </select>
                            </div>
                        </div> 
                    </div>
                    
                    <div class="c-form__field c-form__field--calendar">
                        <div class="c-form__field-inline c-multiple-select">
                            <h3 class="c-multiple-select__title">Client owner <span class="icon">v</span></h3>
                            <div class="c-select-container">
                                <select class="c-select j-multiple-select" name="select_client_owner" data-targetfield="client_owner" data-placeholder="Any client owner" data-datatype="Client owners">
                                </select>
                            </div>
                        </div> 
                        
                        
                        <div class="c-form__field-inline c-calendar">
                            <label for="start">From</label>
                                <input type="text" id="start" placeholder="dd/mm/yyyy" data-targetfield="start_date">
                        </div>
                        
                        
                    </div>
                    <div class="c-form__field c-calendar c-calendar--end">
                        <div class="c-form__field-inline">
                            <label for="end">To</label>
                            <input type="text" id="end" placeholder="dd/mm/yyyy" data-targetfield="end_date">
                        </div>
                    </div>
                    
                    <div class="c-form__field c-multiple-pillbox">
                        <label>Operative team</label>

                        <select class="j-multiple-pillbox" name="select_team" multiple="multiple" data-targetfield="team" data-placeholder="Add a name or e-mail">
                        </select>
                    
                    </div>
                    
                    
                </div>
                <footer class="c-form__footer">
                    <a href="#" class="c-reset">Reset all </a>
                    
                    <button class="c-button" type="submit">Search</button>
                </footer>
                
            </form>
            </div>
        </div>
<?php
//}
    if(isset($aksFunctions[AksFunctions::CreateProject])){ ?>
        <a href="<?php echo $myLakePages['editpage_url'] ?>" class="c-floating-action"><?xml version="1.0" encoding="UTF-8" standalone="no"?><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg width="100%" height="100%" viewBox="0 0 88 88" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;"><g><path d="M44.183,32.267l0,23.466m-11.733,-11.733l23.467,0" style="fill:none;stroke:#fff;stroke-width:3.67px;"/></g></svg></a>
    <?php } ?>
<!-- L-PAGE-->
<script>
    //LOCALSTORAGE


    jQuery(document).ready(function($) {
        //LOCALSTORAGE
        var retrievedObject = localStorage.getItem('savedSearches');
        if(retrievedObject != null){
            retrievedObject = JSON.parse(retrievedObject);
            for(var i in retrievedObject){
                var newItem = jQuery('#j-search-history .c-autocomplete-item.u-template').clone().removeClass('u-template');
                newItem.find('.c-autocomplete-item__label').attr('data-value',retrievedObject[i])
                newItem.find('.c-autocomplete-item__label').html(retrievedObject[i]+'<span class="c-autocomplete-item__type">latest</span>')
                newItem.appendTo('#j-search-history .c-autocomplete-items');
            }
            jQuery('#j-search-history .c-autocomplete-item.u-template').hide();
            
            jQuery('#j-search-history .c-autocomplete-item').on('click',function(){
                    jQuery( '#j-search-fulltext' ).css('position','initial');
                    jQuery( '#j-search-history' ).attr('style','');
                    jQuery('body').removeClass('j-overlay');
                    jQuery('#j-search-history').hide();
                    jQuery('#j-search-fulltext').val(jQuery(this).find('.c-autocomplete-item__label').attr('data-value'));
                    jQuery('#j-searchform__submit').addClass('j-active');
                    jQuery('#j-searchform__submit.j-active span').on('click', function(e) {
                        jQuery('#j-searchform').submit();
                        return false;
                    });

                })
        }else{
            retrievedObject = Array();
        }
        console.log('localstorage:'+retrievedObject );

        jQuery('#j-searchform').on('submit',function(){
            //retrievedObject.push(jQuery('#j-search-fulltext').val());
            //console.log(retrievedObject)
            //localStorage.setItem('savedSearches', JSON.stringify(retrievedObject));
            //jQuery('#j-searchform').submit();
            //return false;
        })

        //PREPOPOLAMENTO 
        jQuery.post(
        '/wp-admin/admin-ajax.php', {
            'action': 'search',
            'subaction': 'hydrate'
        },
        function (response) {
            var respJSON = JSON.parse(response);
            var autoCompleteArr = Array();

            for (var i in respJSON)
            {
                jQuery('select[name=select_'+respJSON[i]['type_']+']').append('<option>'+respJSON[i]['NAME']+'</option>')
                if(respJSON[i]['type_'] == 'client' || respJSON[i]['type_'] == 'project_name' || respJSON[i]['type_'] == 'tag' ){
                    autoCompleteArr.push({label: respJSON[i]['NAME'], value: respJSON[i]['NAME'], type: respJSON[i]['type_']})
                    //jQuery('#j-search-fulltext').append('<option>'+respJSON[i]['NAME']+'</option>');
                }
            }
            //AUTOCOMPLETE
            jQuery('#j-search-history').css('width', jQuery('#j-search-fulltext').outerWidth())
            jQuery('#j-search-history').css('top', jQuery('#j-search-fulltext').offset()['top']+jQuery('#j-search-fulltext').outerHeight())
            jQuery('#j-search-history').css('left', jQuery('#j-search-fulltext').offset()['left'])
            
            jQuery('.j-advanced-search-toggle').on('click', function(e) {
                    jQuery( '#j-search-fulltext' ).attr('style','');
                    jQuery( '#j-search-history' ).attr('style','');
                    jQuery('body').removeClass('j-overlay');
                    jQuery('#j-search-history').hide();
                    openModal('#advanced-search');
                    return false;
                });

            jQuery( "#j-search-fulltext" ).on('focus', function(){
                jQuery('#j-search-history').css('width', jQuery('#j-search-fulltext').outerWidth())
                jQuery('#j-search-history').css('top', jQuery('#j-search-fulltext').offset()['top']+jQuery('#j-search-fulltext').outerHeight())
                jQuery('#j-search-history').css('left', jQuery('#j-search-fulltext').offset()['left'])

                jQuery( '#j-search-fulltext' ).css('z-index',10);
                jQuery( '#j-search-fulltext' ).css('position','relative');
                jQuery('body').addClass('j-overlay');
                jQuery('body').append('<div id="j-overlay-clickarea">');
                jQuery('#j-search-history').show();
                jQuery('#j-overlay-clickarea').on('click', function(){
                    console.log('click')
                    jQuery( '#j-search-fulltext' ).attr('style','');
                    jQuery( '#j-search-history' ).attr('style','');
                    jQuery('body').removeClass('j-overlay');
                    jQuery('#j-search-history').hide();
                    jQuery(this).remove();
                });
                
            })
    

            jQuery( "#j-search-fulltext" ).autocomplete({
                delay: 500,
                source: autoCompleteArr,
                open: function( event, ui ) {
                    jQuery('#j-search-history').hide();
                    jQuery('body').addClass('j-overlay');
                    jQuery( '#j-search-fulltext' ).css('z-index',10);
                    jQuery( '#j-search-fulltext' ).css('position','relative');

                    jQuery('.ui-autocomplete').css('width', jQuery('#j-search-fulltext').innerWidth())
                    console.log(jQuery(ui.target).html());
                    jQuery('.ui-autocomplete .c-autocomplete-item').wrapAll('<div class="c-autocomplete-items">');
                    jQuery('.ui-autocomplete').append('<a href="#" class="j-advanced-search-toggle" id="j-advanced-search-toggle">Advanced Search</a>')
                    jQuery('.j-advanced-search-toggle').on('click', function(e) {
                        jQuery( '#j-search-fulltext' ).autocomplete('close');
                        openModal('#advanced-search');
                        return false;
                    });
                },
                close: function(){
                    jQuery( '#j-search-fulltext' ).attr('style','');
                    jQuery('body').removeClass('j-overlay');
                    jQuery('#j-overlay-clickarea').remove();
                }
            })
            .data("uiAutocomplete")._renderItem = function( ul, item ) {
                var re = new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(this.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi");
                console.log(item.value);
                var newLabel = item.value.replace(re, "<strong>$1</strong>");

                return jQuery( "<li class='c-autocomplete-item'>" )
                .append( "<div class='c-autocomplete-item__label' data-value='"+item.value+"'><span class='icon'></span>" + newLabel + "<span class='c-autocomplete-item__type'>" + item.type + "</span></div>" ).on('click',function(){
                    jQuery( '#j-search-fulltext' ).autocomplete('close');
                    jQuery('#j-search-fulltext').val(jQuery(this).find('.c-autocomplete-item__label').attr('data-value'));
                    jQuery('#j-searchform__submit').addClass('j-active');
                    jQuery('#j-searchform__submit.j-active span').on('click', function(e) {
                        retrievedObject.push(jQuery('#j-search-fulltext').val());
                        localStorage.setItem('savedSearches', JSON.stringify(retrievedObject));

                        jQuery('#j-searchform').submit();
                        return false;
                    });

                })
                .appendTo( ul );
            };
        });
        
        //PROGETTI
        jQuery.post(
            '/wp-admin/admin-ajax.php', {
                'action': 'search', 
                'subaction': 'filter', 
            },
            function(response) {
                var respJSON = JSON.parse(response);
                console.log('The server responded: ', respJSON['data']);
                for(var i in respJSON['data']){
                    var newItem = jQuery('.c-project-item.u-template').clone().removeClass('u-template');
                    var parser = new DOMParser;
                    var dom = parser.parseFromString( '<!doctype html><body>' + respJSON['data'][i]['project_name'],
    'text/html');
                    var decodedString = dom.body.textContent;

                    newItem.attr('href', '<?php echo $myLakePages['viewpage_url']?>?id='+respJSON['data'][i]['project_id']);

                    newItem.find('.c-project-item__header__title').text(decodedString);
                    newItem.find('.c-project-item__header__lead').text(respJSON['data'][i]['client']);
                    newItem.find('.c-project-item__figure').append('<img src="https://placeimg.com/480/'+Math.floor((Math.random() * 1000) + 200)+'/tech">');

                    newItem.find('.c-project-item__footer p').text(respJSON['data'][i]['email']);

                    newItem.appendTo('.c-projects');
                }
                jQuery('.c-project-item.u-template').hide();

                //MASONRY
                jQuery('.c-projects').imagesLoaded( function() {
                    console.log('all images loaded');
                    jQuery('.c-projects').attr('style','');
                    jQuery('.c-spinner').hide();
                    if (window.innerWidth < 1024) {
                        jQuery(".c-projects").shapeshift({
                            enableDrag: false,
                            columns: 2
                        });
                    }
                    else {
                        jQuery(".c-projects").shapeshift({
                            enableDrag: false,
                            columns: 4
                        });
                    }                        
                });

            } );

            jQuery('.j-current-query').on('change', function(e) {
                jQuery('.c-spinner').show();
                jQuery(".c-projects").css('opacity',0);

                jQuery('.c-project-item:not(.u-template)').remove();
                jQuery('.c-project-item.u-template').show();
                jQuery(".c-projects").trigger("ss-destroy");


                var client = jQuery('.j-current-query[name=select_client]').val();
                var industry = jQuery('.j-current-query[name=select_industry]').val();
                var country = jQuery('.j-current-query[name=select_country]').val();
                var skills = jQuery('.j-current-query[name=select_skills]').val();

                var queryObj = {
                    'action': 'search', 
                    'subaction': 'filter', 
                }

                if(client != 'all'){
                    queryObj.client_name = client;
                }
                if(industry != 'all'){
                    queryObj.client_industry = industry;
                }
                if(country != 'all'){
                    queryObj.country = country;
                }
                if(skills != 'all'){
                    queryObj.skills = skills;
                }

                jQuery.post(
                    '/wp-admin/admin-ajax.php', queryObj,
                    function(response) {
                        var respJSON = JSON.parse(response);
                        console.log('The server responded: ', respJSON);

                        for(var i in respJSON['data']){
                            var newItem = jQuery('.c-project-item.u-template').clone().removeClass('u-template');
                            var parser = new DOMParser;
                            var dom = parser.parseFromString( '<!doctype html><body>' + respJSON['data'][i]['project_name'],
            'text/html');
                            var decodedString = dom.body.textContent;

                            newItem.attr('href', '<?php echo $myLakePages['viewpage_url']?>?id='+respJSON['data'][i]['project_id']);

                            newItem.find('.c-project-item__header__title').text(decodedString);
                            newItem.find('.c-project-item__header__lead').text(respJSON['data'][i]['client']);
                            newItem.find('.c-project-item__figure').append('<img src="https://placeimg.com/480/'+Math.floor((Math.random() * 1000) + 200)+'/tech">');
                            newItem.find('.c-project-item__footer p').text(respJSON['data'][i]['email']);

                            newItem.appendTo('.c-projects');
                        }
                        jQuery('.c-project-item.u-template').hide();

                        //MASONRY
                        jQuery('.c-projects').imagesLoaded( function() {
                            jQuery('.c-projects').attr('style','');
                            jQuery('.c-spinner').hide();

                            if (window.innerWidth < 1024) {
                                jQuery(".c-projects").shapeshift({
                                    enableDrag: false,
                                    columns: 2
                                });
                            }
                            else {
                                jQuery(".c-projects").shapeshift({
                                    enableDrag: false,
                                    columns: 4
                                });
                            }                        
                        });




                    } );
            });

    });
</script>
<?php get_footer(); ?>