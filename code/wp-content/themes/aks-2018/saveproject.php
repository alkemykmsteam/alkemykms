<?php
/*
Template Name: Save Project
*/
//print_r($_POST);
session_start();
require_once ABSPATH . '/script/pdocrud.php';
$projects = null;
if(isset($_SESSION["pdocrud_obj"])) {
    $projects = unserialize($_SESSION["pdocrud_obj"]);
}else {
    $projects = new PDOCrud();
}
//$projects->aksSetUp();
$insertData = AksProjectUtility::buildDataProject($_POST);
$out = $projects->saveProject($insertData);
echo json_encode($out);
?>
