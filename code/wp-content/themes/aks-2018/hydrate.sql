SELECT DISTINCT project_id as id, CONCAT(UCASE(LEFT(lower(project_name), 1)),SUBSTRING(lower(project_name), 2)) AS NAME, "project_name" AS type_ FROM project_sheets
UNION ALL
SELECT DISTINCT tm.team_id as id, CONCAT(UPPER(tm.cognome_nome),' - ', LOWER(tm.mail)) AS NAME, "team" AS type_ FROM team_members tm join team_projects tp on (tm.team_id=tp.team_id) join project_sheets ps on (ps.project_id = tp.project_id)
UNION ALL
select distinct UPPER(tag) as id, CONCAT(UCASE(LEFT(lower(tag), 1)),SUBSTRING(lower(tag), 2)) as NAME, "tag" as type_ from mydb.projects_tags
UNION ALL
SELECT DISTINCT c.id as id, CONCAT(UCASE(LEFT(lower(c.name), 1)),SUBSTRING(lower(c.name), 2)) AS NAME, "client" AS type_ FROM project_sheets ps join clients c on (ps.client_id = c.id)
UNION ALL
SELECT DISTINCT team_id as id, UPPER(CONCAT(cognome_nome, ' [', job_title, ']')) AS NAME, "client_owner" AS type_ from team_members where vp_il = 'Y'
UNION ALL
SELECT DISTINCT ind.id as id, CONCAT(UCASE(LEFT(lower(ind.name), 1)),SUBSTRING(lower(ind.name), 2)) AS NAME, "industry" AS type_ FROM project_sheets ps join industries ind on (ind.id = ps.client_industry_id)
UNION ALL
SELECT DISTINCT UPPER(country) as id, country AS NAME, "country" AS type_ FROM project_sheets
UNION ALL
select distinct s.id as id, CONCAT(UCASE(LEFT(lower(s.name), 1)),SUBSTRING(lower(s.name), 2)) AS name, "skills" as type_ from skills s join projects_skills ps on s.id = ps.skill_id
ORDER BY type_, NAME