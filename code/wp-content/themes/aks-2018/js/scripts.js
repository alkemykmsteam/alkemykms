//MODAL
function openModal(modalId, containerId, ajaxParams) {
    console.log('op')
    jQuery('.c-modal-container').css('display', 'none');
    jQuery('.c-modal').css('display', 'none');
    jQuery('.c-modal').unwrap('c-modal-container');

    if (modalId.indexOf('#') > -1) {
        jQuery(modalId).wrap("<div class='c-modal-container'></div>");
        jQuery(modalId).css('display', 'block');
        jQuery(modalId).parent('.c-modal-container').fadeIn(200);
        jQuery('body').addClass('j-modal-opened');
        jQuery('body').css('overflow', 'hidden');
    } else {
        jQuery("body").append('<div id="' + containerId + '" class="c-modal c-modal--' + containerId + '">');
        jQuery('#' + containerId).load(modalId + '?' + ajaxParams).css('display', 'block');
        jQuery('#' + containerId).wrap("<div class='c-modal-container'></div>");
        jQuery('#' + containerId).css('display', 'block');
        jQuery('#' + containerId).parent('.c-modal-container').fadeIn(200, function() {
            jQuery('#' + containerId + ' a').click(function() {
                closeModals();
                return false;
            });
        });

        jQuery('body').addClass('j-modal-opened');
        jQuery('body').css('overflow', 'hidden');

    }
}

function closeModals() {
    jQuery('.c-modal').css('display', 'none');
    jQuery('.c-modal-container').fadeOut(200, function() {
        jQuery('.c-modal').unwrap();
    });
    jQuery('body').removeClass('j-modal-opened');
    jQuery('body').attr('style', '');
}

//PIKADAY
var startDate,
    endDate,
    updateStartDate = function() {
        startPicker.setStartRange(startDate);
        endPicker.setStartRange(startDate);
        endPicker.setMinDate(startDate);
    },
    updateEndDate = function() {
        startPicker.setEndRange(endDate);
        startPicker.setMaxDate(endDate);
        endPicker.setEndRange(endDate);
    },
    startPicker = new Pikaday({
        field: document.getElementById("start"),
        format: "DD MM YY",
        toString(date, format) {
            const day = date.getDate();
            const month = date.getMonth() + 1;
            const year = date.getFullYear();
            return `${day}/${month}/${year}`;
        },
        numberOfMonths: 2,
        onSelect: function() {
            startDate = this.getDate();
            updateStartDate();

        },

    }),
    endPicker = new Pikaday({
        field: document.getElementById("end"),
        format: "DD MM YY",
        toString(date, format) {
            const day = date.getDate();
            const month = date.getMonth() + 1;
            const year = date.getFullYear();
            return `${day}/${month}/${year}`;
        },
        numberOfMonths: 2,
        onSelect: function() {
            endDate = this.getDate();
            updateEndDate();
        }
    }),
    _startDate = startPicker.getDate(),
    _endDate = endPicker.getDate();

if (_startDate) {
    startDate = _startDate;
    updateStartDate();
}

if (_endDate) {
    endDate = _endDate;
    updateEndDate();
}
startPicker.config().i18n = {
    previousMonth : 'Previous Month',
    nextMonth     : 'Next Month',
    months        : ['January','February','March','April','May','June','July','August','September','October','November','December'],
    weekdays      : ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
    weekdaysShort : ['S','M','T','W','T','F','S']
}
endPicker.config().i18n = {
    previousMonth : 'Previous Month',
    nextMonth     : 'Next Month',
    months        : ['January','February','March','April','May','June','July','August','September','October','November','December'],
    weekdays      : ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],
    weekdaysShort : ['S','M','T','W','T','F','S']
}
//PUBLISH PROJECT
function publishProject(publishUrl, notificationBlock){
    if(notificationBlock){
        jQuery(notificationBlock).find('.c-notificiation__published').fadeIn();
    }
    jQuery.post(
        publishUrl, {
        },
        function (response) {
              console.log(response);
        });
    return false;
}
function undoPublishProject(unpublishUrl, notificationBlock){
    if(notificationBlock){
        jQuery(notificationBlock).find('.c-notificiation__published').fadeOut();
    }
    jQuery.post(
        unpublishUrl, {
        },
        function (response) {
            console.log(notificationBlock);

              console.log(response);
        });
    return false;
}


//DOCUMENT.READY
jQuery(document).ready(function($) {
    
    //Polyfill Object Fit su IE --> font-family: 'object-fit: cover;';
    if (typeof objectFitImages == 'function') {
        objectFitImages();
    }

    jQuery('.j-toggle').click(function() {
        jQuery('body').toggleClass('j-menu-open');
    });

    jQuery('.j-notification').click(function() {
        jQuery('body').addClass('j-sidebar-open');
        return false;
    });

    jQuery('.j-sidebar-close').click(function() {
        jQuery('body').removeClass('j-sidebar-open');
        return false;
    });

    jQuery('.j-modal-open').click(function() {
        var modal = jQuery(this).attr('href');
        var contId = jQuery(this).attr('data-id');
        var ajaxParams = jQuery(this).attr('data-param');

        openModal(modal, contId, ajaxParams);
        return false;
    });

    jQuery('.j-modal-close').click(function() {
        closeModals();
        return false;
    });


    jQuery('.mutliSelect input[type="checkbox"]').on('click', function() 
    {
        var title = jQuery(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
            title = jQuery(this).val() + ",";

        if (jQuery(this).is(':checked')) {
            var html = '<span title="' + title + '">' + title + '</span>';
            jQuery('.multiSel').append(html);
            jQuery(".hida").hide();
        } else {
            jQuery('span[title="' + title + '"]').remove();
            var ret = jQuery(".hida");
            jQuery('.dropdown dt a').append(ret);

        }
    });


    //CURRENT-QUERY
    jQuery('.j-current-query').select2({
        allowClear: true,
        minimumResultsForSearch: -1,
        theme: 'default current-query'
    });

    jQuery('.j-current-query').on('select2:opening', function(e) {
        jQuery('body').toggleClass('j-current-query-open');
        jQuery('.c-hero').css('z-index', -1);
    });

    jQuery('.j-current-query').on('select2:close', function(e) {
        jQuery('body').removeClass('j-current-query-open');
        jQuery('.c-hero').attr('style', '');

    });
   
    
    jQuery('#j-searchform--advanced .c-reset, .c-form--filter .c-reset').on('click', function (e) {
        jQuery('#start').val(null);
        jQuery('#end').val(null);
        jQuery('.selected.selected--default').remove();
        jQuery('#j-searchform--advanced select, .c-form--filter select').val(null).trigger('change');
        return false;
    });
    jQuery('.j-filter-toggle').click(function() {
        jQuery('body').toggleClass('j-filter-open');
        return false;
    });


    jQuery('.j-multiple-pillbox').each(function(){
        jQuery(this).select2({
            allowClear: true,
            maximumSelectionSize: 4,
            theme: 'default multiple-pillbox',
            placeholder: jQuery(this).attr('data-placeholder'),
            tags: jQuery(this).attr('data-tags') != 'false' ? true : false
        });
        
        jQuery(this).on('select2:opening', function(e) {
            jQuery(this).parent().toggleClass('j-multiple-pillbox-open');
        });

        jQuery(this).on('select2:select select2:unselect', function (e) {
            var selectedItems = '';
            if(jQuery(this).val().constructor && jQuery(this).val().constructor === Array){
                selectedItems = jQuery(this).val().join('|');
            }else{
                selectedItems = jQuery(this).val();
            }

            jQuery('#hidden-'+jQuery(this).attr('data-targetfield')).val(selectedItems);
        });

        jQuery(this).on('select2:close', function(e) {
            jQuery(this).parent().removeClass('j-multiple-pillbox-open');
        });
    });


    jQuery('.j-multiple-select, .j-single-select').each(function(){
        console.log(jQuery(this).attr('class'))
        if(jQuery(this).attr('class') == 'c-select j-multiple-select'){
            jQuery(this).select2({
                multiple: true,
                closeOnSelect: false,
                theme: 'default select-checkbox',
                allowHtml: true,
                tags: false,
                placeholder: jQuery(this).attr('data-placeholder')
            });
            jQuery(this).on('select2:opening', function(e) {
                jQuery(this).parent().parent().toggleClass('j-multiple-select-open');
                jQuery(this).parent().find('.selected--default').remove();
            });
        }else{
            jQuery(this).select2({
                multiple: false,
                closeOnSelect: true,
                theme: 'default select-radio',
                allowHtml: true,
                tags: false,
                placeholder: jQuery(this).attr('data-placeholder')
            });
            jQuery(this).on('select2:opening', function(e) {
                jQuery(this).parent().parent().toggleClass('j-single-select-open');
                jQuery(this).parent().find('.selected--default').remove();
            });

        }

        jQuery(this).on('select2:select select2:unselect', function (e) {
            if(jQuery(this).val() && jQuery(this).val().constructor === Array){
                selectedItems = jQuery(this).val().join('|');
            }else{
                selectedItems = jQuery(this).val();
            }
            jQuery('#hidden-'+jQuery(this).attr('data-targetfield')).val(selectedItems);
        });

        jQuery('.c-calendar input').on('change', function (e) {
            var dateAttr = jQuery(this).val().split('/')[2] + '-'+jQuery(this).val().split('/')[1] +'-'+jQuery(this).val().split('/')[0] 
            jQuery('#hidden-'+jQuery(this).attr('data-targetfield')).val(dateAttr);
        });
        
        jQuery(this).on('select2:select', function (e) {
            var data = e.params.data;
        });

        jQuery(this).on('select2:close', function(e) {
            jQuery(this).parent().parent().removeClass('j-multiple-select-open');
            jQuery(this).parent().parent().removeClass('j-single-select-open');
            var uldiv = jQuery(this).siblings('span.select2').find('.select2-selection__rendered')
            
            var count = uldiv.find('li').length - 1;
            if(count> 0){
                if(uldiv.find('.select2-search--inline').index() > 0){
                    uldiv.find('.select2-selection__choice').html("<span class='selected'>"+jQuery(this).attr('data-datatype')+" ("+count+")</span>");
                }else{
                    uldiv.find('.select2-selection__choice').html("<span class='placeholder'>"+jQuery(this).attr('data-placeholder')+"</span>");
                }
            }else{
                uldiv.find('.select2-selection__choice').html("<span class='placeholder'>"+jQuery(this).attr('data-placeholder')+"</span>");
            }
        });
    });
    
    //SLICK
    /*
    jQuery('.c-related-projects-slider').slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 3,
        arrows: true,
        dots:true,
        prevArrow: '<button type="button" class="slick-prev"><span class="icon icon-arrow-big"></span></button>',
        nextArrow: '<button type="button" class="slick-next"><span class="icon icon-arrow-big"></span></button>',  
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 3,
                
                }
            },
            {
                breakpoint: 480,
                settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1,
                
                }
            }
            ]

        });
        */
        

});
