<?php
/*
Template Name: Fetch
*/
session_start();
require_once ABSPATH . '/script/pdocrud.php';
$projects = null;
if(isset($_SESSION["pdocrud_obj"])) {
    $projects = unserialize($_SESSION["pdocrud_obj"]);
}else {
    $projects = new PDOCrud();
}

$functions = $_SESSION['$aksFunctions'];
if(isset($_SESSION['myLakePages'])) {
    $myLakePages = $_SESSION['myLakePages'];
}else{
    $myLakePages = $projects->loadMyLakePages();
    $_SESSION['myLakePages'] = $myLakePages;
}

if(isset($_POST['view'])){
    // $con = mysqli_connect("localhost", "root", "", "notif");
    //if($_POST["view"] != '') {
        // TODO come recupero l'id del progetto ?
        //$projects->readNotification(0);
    //}
    $email = wp_get_current_user()->user_email;
    //$notificationsContainer = $projects->loadNotification("t.stark@starkindustries.com");
    //$email = 'francesca.ciullo@alkemy.com';
    // $email = 'utentetest751@gmail.com';
    $notificationsContainer = $projects->loadNotification($email);

    $notifications = $notificationsContainer[0];
    $total = count($notifications);
    $output = '';
    if($total > 0){
        foreach ($notifications as $row) {

        $output .= '
            <div class="c-notification" id="notification'.$row["project_id"].'">
                <header class="c-notification__header">
                    <figure class="c-notification__header__figure">
                        <img src="'.$row["avatar"].'">
                    </figure>
                    <h4 class="c-notification__header__lead t-title4">'.$row["author"].' create a new project</h4>
                    <h3 class="c-notification__header__title t-title3"><a href="'.$myLakePages['viewpage_url'].'?id='.$row["project_id"].'">'.$row["project_name"].'</a></h3>
                </header>
                <div class="c-notification__content">
                    On  '.$row["info_time"].'
                </div>';

            if(isset($functions[AksFunctions::PublishProject])){
                $output.='
                <footer class="c-notification__footer">
                    <a class="c-notification__footer__link" href="'.$myLakePages['editpage_url'].'?id='.$row["project_id"].'"><span class="icon icon-modify"></span> edit</a>
                    <a class="c-notification__footer__link" href="javascript:publishProject(\''.$myLakePages['publishpage_url'].'?id='.$row["project_id"].'\', \'#notification'.$row["project_id"].'\');"><span class="icon icon-publish"></span> publish</a>
                </footer>
                <div class="c-notificiation__published" style="display:none;">
                    <span class="icon icon-hand-ok"></span>
                    <p>This project has been published!</p>
                    <a href="javascript:undoPublishProject(\''.$myLakePages['undo_publishpage_url'].'?id='.$row["project_id"].'\', \'#notification'.$row["project_id"].'\');" class="c-notificiation__published__link">Undo</a>
                </div>';
            }else if(isset($functions[AksFunctions::PublishProject]) == false && isset($functions[AksFunctions::EditProject])){
                $output.='
                <footer class="c-notification__footer">
                    <a class="c-notification__footer__link" href="'.$myLakePages['editpage_url'].'?id='.$row["project_id"].'"><span class="icon icon-modify"></span> edit draft</a>
                </footer>';
            }

            $output.='</div>';

        }
    }
    else{
        $output .= '';
    }
    $notificationumber = '';
    if(sizeof($notifications) > 1){
        $notificationumber = "You have ".sizeof($notifications)." new projects to check";
    }else {// if(sizeof($notifications) == 1){
        $notificationumber = "You have ".sizeof($notifications)." new project to check";
    }
    $output .= '
        <script>
            jQuery(document).ready(function($) {
                jQuery(".c-sidebar__lead").text("'.$notificationumber.' ");
            });
        </script>
    ';
//    $status_query = "SELECT * FROM comments WHERE comment_status=0";
//    $result_query = mysqli_query($con, $status_query);
    $count = $total; //mysqli_num_rows($result_query);
    $data = array(
        'notification' => $output, //$notifications
        'unseen_notification'  => $count
    );
    //$tmp = json_encode($data);
    echo json_encode($data);
}
?>