<?php 
/* Template Name: Login */
get_header('login'); ?>

    <div class="c-hero c-hero--login">
        <div class="c-hero__content">
            <div class="c-login">
                <div data-letter="A" class="c-letter__content"></div>

                <div class="c-login__content">
                    <header class="c-login__header">
                        <span class="c-logo">
                            <img src="<?php echo get_bloginfo('template_url'); ?>/img/logo-black.svg" alt="">
                        </span>
                    </header>
                    <form name="loginform" id="loginform" action="http://crescendosocialclub.e-coop.it/wp-login.php" method="post">
                        <?php echo do_shortcode("[mo_oauth_login]"); ?>

                    </form>
                </div>
            </div>

<!--            <div class="c-disclaimer">ALKEMY knowledge system</div>-->
            <div class="c-disclaimer"><?php echo get_bloginfo('name');?></div>
        </div>
    </div>
    </main>
    </div>
<?php get_footer(); ?>