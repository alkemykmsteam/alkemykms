<?php 
/* Template Name: Industry */
session_start();
require_once ABSPATH . '/script/pdocrud.php';
$projects = null;
if(isset($_SESSION["pdocrud_obj"])) {
    $projects = unserialize($_SESSION["pdocrud_obj"]);
}else {
    $projects = new PDOCrud();
}

if(isset($_SESSION['myLakePages'])) {
    $myLakePages = $_SESSION['myLakePages'];
}else{
    $myLakePages = $projects->loadMyLakePages();
    $_SESSION['myLakePages'] = $myLakePages;
}

get_header();
?>
<script>
    jQuery(document).ready(function($) {
        //PREPOPOLAMENTO FILTRI
        jQuery.post(
        '/wp-admin/admin-ajax.php', {
            'action': 'search',
            'subaction': 'hydrate'
        },
        function (response) {
            var respJSON = JSON.parse(response);
            for (var i in respJSON)
            {
                console.log(respJSON[i]['type_']);
                jQuery('select[name='+respJSON[i]['type_']+']').append('<option value="'+respJSON[i]['NAME']+'">'+respJSON[i]['NAME']+'</option>')
            }

            //EVENTS
            jQuery('.j-multiple-select').on('change', function(e) {
                jQuery('.c-industry:not(.u-template)').remove();
                jQuery('.c-industry.u-template').show();

                var client = jQuery('.j-multiple-select[name=client]').val();
                var country = jQuery('.j-multiple-select[name=country]').val();
                var skills = jQuery('.j-multiple-select[name=skills]').val();

                var queryObj = {
                    'action': 'search', 
                    'subaction': 'search_industries', 
                }
                console.log(jQuery(this));

                if(client != 'all' && client != null){
                    queryObj.client_name = client.join('|');
                }
                if(country != 'all' && country != null){
                    queryObj.country = country.join('|');
                }
                if(skills != 'all' && skills != null){
                    queryObj.skills = skills.join('|');
                }

                jQuery.post(
                '/wp-admin/admin-ajax.php', queryObj,
                function(response) {
                    var respJSON = JSON.parse(response);
                    console.log('The server responded: ', respJSON);
                    jQuery('.c-results__header__title .j-count').text(respJSON['data'].length);
                    for(var i in respJSON['data']){
                        var newItem = jQuery('.c-industry.u-template').clone().removeClass('u-template');
                        var parser = new DOMParser;
                        var dom = parser.parseFromString( '<!doctype html><body>' + respJSON['data'][i]['name'],'text/html');
                        var decodedString = dom.body.textContent;
                        newItem.attr('data-name', decodedString);
                        newItem.attr('data-client-id', respJSON['data'][i]['id']);
                        newItem.attr('href', newItem.attr('href') + '?industry_selected='+respJSON['data'][i]['id']);

                        newItem.find('.c-industry__title').text(decodedString);
                        if(respJSON['data'][i]['related_projects'] == '1'){
                            newItem.find('.c-industry__caption').text(respJSON['data'][i]['related_projects'] + ' Project');
                        }else{
                            newItem.find('.c-industry__caption').text(respJSON['data'][i]['related_projects'] + ' Projects');
                        }
                        newItem.appendTo('.c-results__content');
                    }
                    jQuery('.c-industry.u-template').hide();
                    
                } );
                console.log(queryObj)
            });

            jQuery('select[name=skills]').trigger('change');

        });
    });
</script>

<header class="c-hero l-container">
    <div class="c-hero__content">
        <div class="c-hero__content__text">
            <div class="c-breadcrumbs">
                <a class="c-breadcrumbs__item" href="<?php echo $myLakePages['loadpage_url']?>">Home</a>
                <a class="c-breadcrumbs__item current-item" href="<?php echo $myLakePages['industrypage_url']?>">Industry</a>
            </div>
            <h1 class="c-hero__content__text__title t-title1">Discover all the industries</h1>
            <h2 class="c-hero__content__text__lead t-title6">Last updated:</h2>

        </div>                        
    </div>
</header>
<section class="c-content-block l-container">
    <div class="c-content-block__content c-advanced-search">
        <form class="c-form c-form--filter">
            <header class="c-form__header">
                <h3 class="c-form__header__title t-title4">Filter</h3>
            </header>
            <div class="c-form__content l-2col">
                <div class="c-form__field c-form__field--yellow">
                    
                    <div class="c-form__field-inline c-multiple-select">
                        <h3 class="c-multiple-select__title">Clients <span class="icon">v</span></h3>
                        <div class="c-select-container">
                            <select class="c-select j-multiple-select" name="client" data-placeholder="All clients" data-datatype="Clients" multiple="" tabindex="-1" aria-hidden="true">
                            </select>
                        </div>
                    </div> 

                    <div class="c-form__field-inline c-multiple-select">
                        <h3 class="c-multiple-select__title">Countries <span class="icon">v</span></h3>
                        <div class="c-select-container">
                            <select class="c-select j-multiple-select" name="country" data-placeholder="All countries" data-datatype="Countries"  multiple="" tabindex="-1" aria-hidden="true">
                            </select>
                        </div>
                    </div> 
                </div>
                <div class="c-form__field c-form__field--yellow">
                    <div class="c-form__field-inline c-multiple-select">
                        <h3 class="c-multiple-select__title">Skills <span class="icon">v</span></h3>
                        <div class="c-select-container">
                            <select class="c-select j-multiple-select" name="skills" data-placeholder="All skills" data-datatype="Skills"  multiple="" tabindex="-1" aria-hidden="true">
                            </select>
                        </div>
                    </div> 
                </div>
            </div>

        </form>

        <div class="c-results">
            <header class="c-results__header">
                <h2 class="c-results__header__title c-results__header__title--default t-title3">Industries (<span class="j-count"></span>)</h2>
                <h2 class="c-results__header__title c-results__header__title--select-apply t-title3">We found <strong><span class="j-count">13</span> clients</strong>
                <small>for "Banking" and "Retail"</small>
                </h2>
                <a class="c-results__header__order" href="#">Order by A-Z<span class="icon icon-arrow-double"></span></a>
            </header>
            <div class="c-results__content l-5col">
                <a href="<?php echo $myLakePages['industrySelectedPage_url'] ?>" class="c-industry u-template">
                    <h3 class="c-industry__title t-title4"></h3>
                    <h4 class="c-industry__caption t-title5"></h4>
                </a>
            </div>
        </div>



    </div>
</section>
<?php get_footer(); ?>