<?php
/*
Template Name: Publish Project
*/
session_start();
require_once ABSPATH . '/script/pdocrud.php';
$projects = null;
if(isset($_SESSION["pdocrud_obj"])) {
    $projects = unserialize($_SESSION["pdocrud_obj"]);
}else {
    $projects = new PDOCrud();
}

$id = -1;
if(isset($_GET['id']) && empty($_GET['id']) === false) {
    $id = intval($_GET['id']);
}else if(isset($_POST[AksColumns::ColumnProjectId]) && empty($_POST[AksColumns::ColumnProjectId]) ===false) {
    $id = intval($_POST[AksColumns::ColumnProjectId]);
}
if($id>0){
    $out = $projects->publishProject($id);
    echo json_encode($out);

    //$projects->undopublishProject($id);
}
?>