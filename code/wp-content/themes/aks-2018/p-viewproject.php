<?php
/*
Template Name: View Project
*/
session_start();
require_once ABSPATH . '/script/pdocrud.php';
$projects = null;
if(isset($_SESSION["pdocrud_obj"])) {
    $projects = unserialize($_SESSION["pdocrud_obj"]);
}else {
    $projects = new PDOCrud();
}
$aksFunctions = $_SESSION['$aksFunctions'];
$editProject = false;
if(isset($_GET['id']) === false){
    $id=-1;
}else{
    $id = $_GET['id'];
    $editProject = !($projects->isProjectWaitingForApproval($id));

}

$tmp = $projects->getProjectData(intval($id));
if(isset($_SESSION['myLakePages'])) {
    $myLakePages = $_SESSION['myLakePages'];
}else{
    $myLakePages = $projects->loadMyLakePages();
    $_SESSION['myLakePages'] = $myLakePages;
}

$htmlProject = $projects->getHtmlProjectForPrint(intval($id));
$key = 'aks_v_'.$id;
$totalView = 0;
if (!isset($_COOKIE['aks_v_'.$id])) {
    setcookie('aks_v_'.$id, 1);
    $_COOKIE['aks_v_'.$id] = 1;
    $projects->manageViewCount(intval($id));
}
$totalView = $projects->getTotalViews(intval($id));
$output = json_decode($tmp);

get_header();
?>

<!--
<?php print_r($output); ?>
-->
<script>
    
    jQuery(document).ready(function($) {
        function getAvatar(img, imgid, name){
            if(img==''){
                jQuery.post(
                    '/wp-admin/admin-ajax.php', {
                        'action': 'getavatar',
                        'name': name,
                        'bgcolor': 'f6e61d',
                        'fgcolor': '000'
                    },
                    function (response) {
                        url = response;
                        jQuery('.img'+imgid).attr('src',url);
                        return url;
                    }
                );
            }else{
                url = img;
                return url;
            }
        }
        jQuery('.c-tabs__nav__menu__item a').on('click',function(){
            jQuery('.c-tabs__nav__menu__item').removeClass('j-active');
            jQuery('.c-tabs__content>*').removeClass('j-active');
            
            jQuery(this).parent().addClass('j-active');
            jQuery(jQuery(this).attr('href')).addClass('j-active');
            return false;
        })
        jQuery('#printProject').click(function(){
            var newWin = window.open('', 'Print-Window');
            newWin.document.open();
            newWin.document.write('<html><body onload="window.print()"> <?php echo $htmlProject ?></body></html>');
            newWin.document.close();
            setTimeout(function () {
                newWin.close();
            }, 10);

        })
        jQuery.post(
            '/wp-admin/admin-ajax.php', {
                'action': 'search',
                'subaction': 'get_team_by_project',
                'project_id': '<?php echo $output->project_id ?>'
            },
            function (response) {
                var retrievedObject = JSON.parse(response);
                console.log('resp:')
                console.log(retrievedObject)
                for(var i in retrievedObject['data']){
                    if(i<3){
                        var newItem = jQuery('.c-read-item__content__item.u-template').clone().removeClass('u-template');
                        newItem.find('.c-tooltip__title').text(retrievedObject['data'][i]['cognome_nome'])
                        newItem.find('.c-tooltip__lead').text(retrievedObject['data'][i]['job_title'])
                        newItem.find('img').addClass('img'+retrievedObject['data'][i]['id']);
                        newItem.find('img').each(function(){
                            jQuery(this).attr('src',getAvatar(retrievedObject['data'][i]['avatar'], retrievedObject['data'][i]['id'], retrievedObject['data'][i]['cognome_nome']) );
                        })
                    
                        newItem.prependTo('.c-read-item__content--user');
                    }
                }
                if(retrievedObject['data'].length > 3){
                    for(var i in retrievedObject['data']){
                        if(i>=3){
                            var newItem = jQuery('.c-read-item__content__item--more .c-tooltip__item.u-template').clone().removeClass('u-template');
                            newItem.find('.c-tooltip__title').text(retrievedObject['data'][i]['cognome_nome'])
                            newItem.find('img').addClass('img'+retrievedObject['data'][i]['id']);
                            newItem.find('img').each(function(){
                                jQuery(this).attr('src',getAvatar(retrievedObject['data'][i]['avatar'], retrievedObject['data'][i]['id'], retrievedObject['data'][i]['cognome_nome']) );
                            })
                            newItem.appendTo('.c-read-item__content__item--more  .c-tooltip');
                        }
                    }
                }
                jQuery('.c-read-item__content__item.u-template').remove();
                jQuery('.c-read-item__content__item--more .c-tooltip__item.u-template').remove();

            }
        );
        //RELATED
        jQuery.post(
            '/wp-admin/admin-ajax.php', {
                'action': 'search',
                'subaction': 'get_related_project_by_tag',
                'project_id': '<?php echo $output->project_id ?>'
            },
            function (response) {
                var retrievedObject = JSON.parse(response);
                console.log('related:')
                console.log(retrievedObject)
                if(retrievedObject['data'] == 0){
                    jQuery('#relatedprojects').remove();
                }
                for(var i in retrievedObject['data']){
                    var newItem = jQuery('.c-project-item.u-template').clone().removeClass('u-template');
                    newItem.attr('href','?id='+retrievedObject['data'][i]['project_id']);
                    newItem.find('.c-project-item__header__title').text(retrievedObject['data'][i]['project_name'])
                    newItem.find('.c-project-item__header__lead').text(retrievedObject['data'][i]['client'])
                    newItem.find('.c-project-item__footer p').text(retrievedObject['data'][i]['client_owner'])

                    newItem.appendTo('.c-related-projects-slider');
                }
                jQuery('.c-project-item.u-template').remove();
                jQuery('.c-related-projects-slider').slick({
                    infinite: true,
                    slidesToShow: 5,
                    slidesToScroll: 3,
                    arrows: true,
                    dots:true,
                    prevArrow: '<button type="button" class="slick-prev"><span class="icon icon-arrow-big"></span></button>',
                    nextArrow: '<button type="button" class="slick-next"><span class="icon icon-arrow-big"></span></button>',  
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 3,
                            
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 1,
                            
                            }
                        }
                        ]
                    });
            }
        );


        
    });
</script>
<header class="c-hero l-container">
                <div class="c-hero__content">
                    <div class="c-hero__content__text">
                        <div class="c-breadcrumbs">
                            <a class="c-breadcrumbs__item" href="<?php echo $myLakePages['loadpage_url'] ?>">Home</a>
                            <a class="c-breadcrumbs__item" href="<?php echo $myLakePages['clientpage_url'] ?>">Clients</a>
                            <a class="c-breadcrumbs__item current-item" href="#"><?php echo $output->client ?></a>
                        </div>
                        <h1 class="c-hero__content__text__title t-title1"><?php echo $output->project_name ?></h1>
                        <h2 class="c-hero__content__text__meta">
                            <strong><?php echo $output->client ?></strong>
                            <span><?php echo date("d/m/Y", strtotime($output->start_date)) ?> - <?php echo date("d/m/Y", strtotime($output->end_date)) ?></span>
                            <small><span class="icon icon-show"></span> <?php echo $totalView ?></small>
                        </h2>
                    </div>
                </div>
            </header>


            <section class="c-content-block c-content-block--read l-container">
                <div class="l-2col l-2col--big">
                    <div class="">
                        <div class="l-2col">
                            <div class="c-read-item">
                                <h4 class="c-read-item__title">Client owner</h4>
                                <div class="c-read-item__content">
                                    <strong><?php echo $output->project_owner ?></strong>
                                </div>
                            </div>
                            <div class="c-read-item">
                                <h4 class="c-read-item__title">Industry</h4>
                                <div class="c-read-item__content">
                                <?php echo $output->industry ?>
                                </div>
                            </div>
                            <div class="c-read-item">
                                <h4 class="c-read-item__title">Country</h4>
                                <div class="c-read-item__content">
                                <?php echo $output->country ?>
                                </div>
                            </div>
                            <div class="c-read-item c-read-item--inline">
                                <h4 class="c-read-item__title">Skills</h4>
                                <ul class="c-read-item__content">
                                    <?php 
                                        $skills = explode('|',$output->skills);
                                        foreach ($skills as $skill):
                                    ?>
                                        <li class="c-read-item__content__item c-read-item__content__item--skill">
                                            <span class="c-skill" data-skillname="<?php echo $skill ?>">
                                               <?php echo $skill[0] ?>
                                            </span>
                                            <div class="c-tooltip">
                                                <h5 class="c-tooltip__title"><?php echo $skill ?></h5>
                                            </div>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <div class="c-read-item c-read-item--inline">
                                <h4 class="c-read-item__title">Operative team</h4>
                                <ul class="c-read-item__content c-read-item__content--user">
                                        <li class="c-read-item__content__item u-template">
                                            <figure class="c-read-item__content__item__user">
                                                <img src="">
                                            </figure>
                                            <div class="c-tooltip">
                                                <figure class="c-read-item__content__item__user">
                                                    <img src="">
                                                </figure>
                                                <h5 class="c-tooltip__title"></h5>
                                                <h6 class="c-tooltip__lead"></h6>
                                            </div>
                                        </li>

                                    <?php 
                                        $team_members = explode('|',$output->team);
                                        if(sizeof($team_members)> 3):?>
                                        <li class="c-read-item__content__item c-read-item__content__item--more">
                                            <figure class="c-read-item__content__item__user">
                                            +<?php echo sizeof($team_members) - 3; ?>
                                            </figure>
                                            <div class="c-tooltip">
                                                <div class="c-tooltip__item u-template">
                                                    <figure class="c-read-item__content__item__user">
                                                        <img src="">
                                                    </figure>
                                                    <h5 class="c-tooltip__title"></h5>
                                                </div>
                                            </div>
                                        </li>
                                    <?php endif; ?>

                                </ul>
                            </div>
                            <div class="c-read-item">
                                <h4 class="c-read-item__title">Tags</h4>
                                <ul class="c-read-item__content c-read-item__content--tags">
                                    <?php 
                                        $tags = explode('|',$output->tags);
                                        foreach ($tags as $tag):
                                    ?>
                                        <li class="c-read-item__content__item"><?php echo $tag ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <footer class="c-content-block--read__footer">
                            <a class="c-content-block--read__footer__button j-overlay-attached" href="#"><span class="icon icon-text"></span> Documents ()</a>
                            <a class="c-content-block--read__footer__button j-overlay-photo" href="#"><span class="icon icon-img"></span> Photo/ video ()</a>
                        </footer>
                    </div>

                    <div class="c-tabs">
                        <nav class="c-tabs__nav">
                            <ul class="c-tabs__nav__menu">
                                <li class="c-tabs__nav__menu__item j-active"><a href="#situation">Situation</a></li>
                                <li class="c-tabs__nav__menu__item"><a href="#complication">Complication</a></li>
                                <li class="c-tabs__nav__menu__item"><a href="#solution">Solution</a></li>
                                <li class="c-tabs__nav__menu__item"><a href="#result">Result</a></li>
                            </ul>
                        </nav>
                        <div class="c-tabs__content">
                            <div id="situation" class="j-active">
                            <?php echo $output->situation ?>
                            </div>
                            <div id="complication">
                            <?php echo $output->complication ?>
                            </div>
                            <div id="solution">
                            <?php echo $output->solution ?>
                            </div>
                            <div id="result">
                            <?php echo $output->results ?>
                            </div>

                        </div>
                    </div>

                </div>
                <footer class="c-content-block__footer">
                    <a href="<?php echo $myLakePages['loadpage_url'] ?>" class="c-button c-button--light"><span class="icon icon-left"></span> Back</a>
                    <a href="#" id="printProject" class="c-button c-button--light"><span class="icon icon-print"></span> Print <span>project sheet</span></a>
                </footer>
            </section>
            <section class="c-content-block l-container" id="relatedprojects">
                <header class="c-content-block__header">
                    <h2 class="c-content-block__header__title t-title3">
                        Related projects
                    </h2>
                </header>

                <div class="c-related-projects-slider">
                
                    <a href="#" class="c-project-item u-template">
                        <figure class="c-project-item__figure">
                            <img src="https://placeimg.com/480/946/tech"></figure>
                        <header class="c-project-item__header">
                            <h3 class="c-project-item__header__title t-title4">Project name</h3>
                            <h4 class="c-project-item__header__lead t-lead2">Client</h4>
                        </header>
                        <footer class="c-project-item__footer">
                            <p>Client owner name</p>
                        </footer>
                    </a>
                    

                </div>

            </section>
        <?php if(isset($aksFunctions[AksFunctions::EditProject]) && $editProject  && $projects->isUserAuthorOrClientOwner($id, wp_get_current_user()->user_email)){ ?>
            <a href="<?php echo $myLakePages['editpage_url'] ?>?id=<?php echo $id ?>" class="c-floating-action">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="58.667" height="58.667" viewBox="0 0 58.6666667 58.6666667">
                <defs>
                    <path id="a" d="M36.798 15.042l3.404 3.405a1.222 1.222 0 0 0 1.729 0l1.694-1.694a1.222 1.222 0 0 0 0-1.728L40.22 11.62a1.222 1.222 0 0 0-1.729 0l-1.693 1.694a1.222 1.222 0 0 0 0 1.728zm8.458 3.192l-1.6 1.718a3.667 3.667 0 0 1-5.276.094l-3.29-3.288a3.667 3.667 0 0 1-.134-5.043l1.57-1.748a3.667 3.667 0 0 1 5.32-.143l3.319 3.319a3.667 3.667 0 0 1 .09 5.09zM13.444 37.173v3.16c0 .675.548 1.223 1.223 1.223h3.407c.328 0 .641-.132.871-.365l15.404-15.638a1.222 1.222 0 0 0-.007-1.722l-3.167-3.167a1.222 1.222 0 0 0-1.728 0L13.802 36.31c-.229.229-.358.54-.358.864zm19.46-18.447l3.37 3.37a3.667 3.667 0 0 1 0 5.186L20.272 43.284a2.444 2.444 0 0 1-1.729.716h-5.099A2.444 2.444 0 0 1 11 41.556v-5.099c0-.648.258-1.27.716-1.729l16.002-16.002a3.667 3.667 0 0 1 5.186 0zM13.322 48.89h32.022a1.1 1.1 0 0 1 1.1 1.1v.244a1.1 1.1 0 0 1-1.1 1.1H13.322a1.1 1.1 0 0 1-1.1-1.1v-.244a1.1 1.1 0 0 1 1.1-1.1z"/>
                </defs>
                <use fill="#FFF" fill-rule="nonzero" xlink:href="#a"/>
            </svg>

            </a>
        <?php } ?>
        </main>

<?php get_footer(); ?>