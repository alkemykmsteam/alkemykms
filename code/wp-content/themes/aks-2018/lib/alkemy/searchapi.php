<?php 
// Ajax endpoint for the search
add_action( 'wp_ajax_search', 'my_ajax_search_handler' );


function my_ajax_search_handler() {
	global $wpdb;
	if ( $_POST['subaction'] == "hydrate" ) {
		$hydrate = file_get_contents( get_template_directory() . "/hydrate.sql" );
		$results = $wpdb->get_results( $hydrate, OBJECT );
		echo json_encode( $results );
	}

	if ( $_POST['subaction'] == "hydrate_edit" ) {
		$hydrate = file_get_contents( get_template_directory() . "/hydrate_edit.sql" );
		$results = $wpdb->get_results( $hydrate, OBJECT );
		echo json_encode( $results );
	}

	if ( $_POST['subaction'] == "fulltext" ) {
		$where  = '';
		$params = [];
		$params[] = '%' . $_POST['search'] . '%';
		$params[] = '%' . $_POST['search'] . '%';
		$params[] = '%' . $_POST['search'] . '%';
		//offset e limit usati per la paginazione
		if ( isset( $_POST['offset'] ) && $_POST['offset'] ) {
			$params[] = $_POST['offset'];
		} else {
			$params[] = 0;
		}
		if ( isset( $_POST['limit'] ) && $_POST['limit'] ) {
			$params[] = $_POST['limit'];
		} else {
			$params[] = 100;
		}

		$sql = "SELECT SQL_CALC_FOUND_ROWS ps.* FROM project_sheets  AS ps
                JOIN team_projects AS tp ON tp.project_id = ps.project_id
                join team_members tm on tm.team_id = tp.team_id
                JOIN clients c on c.id = ps.client_id
				join projects_tags pt on pt.project_id = ps.project_id
                WHERE UPPER(ps.project_name) LIKE %s OR UPPER(pt.tag) LIKE %s OR UPPER(c.name) LIKE %s
                GROUP BY ps.project_id
                ORDER BY start_date DESC
                LIMIT %d,%d";
		$res = $wpdb->get_results( $wpdb->prepare( $sql, $params ), OBJECT );

		$count = $wpdb->get_results( "SELECT FOUND_ROWS() AS count;", OBJECT );

		echo json_encode( [ data => $res, count => $count] );
	}

	if ( $_POST['subaction'] == "filter" ) {
		$where  = '';
		$params = [];
		if ( isset( $_POST['project_name'] ) && empty( $_POST['project_name'] ) ===false ) {
			$tmp = "'".str_replace("|", "','", $_POST['project_name'])."'";
			//$params[] = '%' . $_POST['project_name'] . '%';
			$where .= "AND UPPER(ps.project_name) IN (".$tmp.")\n";
		}
		if ( isset( $_POST['tag']) && empty( $_POST['tag'] ) ===false ) {
			$splittedTag = explode("|", $_POST['tag']);
			$where .= "AND (";
			foreach ($splittedTag as $key=>$val){
				$where .= "UPPER(pt.tag) like '%".$splittedTag[$key]."%'";
				if($key<count($splittedTag)-1){
					$where .= " OR ";
				}
			}
			$where .= ") ";
//			$params[] = '%' . $_POST['tag'] . '%';
//			$where .= "AND tag LIKE %s\n";
		}
		if ( isset($_POST['project_id']) && empty( $_POST['project_id'] ) ===false ) {
			$params[] = $_POST['project_id'];
			$where .= "AND ps.project_id = %d\n";
		}
		if ( isset( $_POST['client_name']) && empty( $_POST['client_name'] ) ===false )  {
			$tmp = "'".str_replace("|", "','", $_POST['client_name'])."'";
			//$params[] = $_POST['client_name'];
			$where .= "AND UPPER(c.name) IN (".$tmp.")\n";
		}
		//FILTRO CLIENT_OWNER DISABILITATO PER ORA
//		if ( isset( $_POST['client_owner'] ) ) {
//			$tmp = "'".str_replace("|", "','", $_POST['client_owner'])."'";
//			//$params[] = $_POST['client_owner'];
//			$where .= "AND client_owner IN (".$tmp.")\n";
//		}
		if ( isset( $_POST['client_industry']) && empty( $_POST['client_industry'] ) ===false ) {
			$tmp = "'".str_replace("|", "','", $_POST['client_industry'])."'";
			//$params[] = $_POST['client_industry'];
			$where .= "AND UPPER(ind.name) IN (".$tmp.")\n";
		}
		if ( isset( $_POST['country']) && empty( $_POST['country'] ) ===false ) {
			$tmp = "'".str_replace("|", "','", $_POST['country'])."'";
			//$params[] = $_POST['country'];
			$where .= "AND UPPER(ps.country) IN (".$tmp.")\n";
		}
		if ( isset( $_POST['skills']) && empty( $_POST['skills'] ) ===false ) {
			$tmp = "'".str_replace("|", "','", $_POST['skills'])."'";
			$where .= "AND UPPER(sk.name) IN (".$tmp.")\n";
		}
		if ( isset( $_POST['start_date']) && empty( $_POST['start_date'] ) ===false ) {
			$dateSplitted = explode('-', $_POST['start_date']);
			//faccio un controllo di validazione della data
			if (count($dateSplitted) === 3 && checkdate($dateSplitted[2], $dateSplitted[1], $dateSplitted[0])===true) {
				$start = $_POST['start_date'];
				$params[] = $start;
				$where .= "AND (ps.start_date >= %s)\n";
			}

		}
		if ( isset( $_POST['end_date']) && empty( $_POST['end_date'] ) ===false ) {
			$dateSplitted = explode('-', $_POST['end_date']);
			//faccio un controllo di validazione della data
			if (count($dateSplitted) === 3 && checkdate($dateSplitted[2], $dateSplitted[1], $dateSplitted[0])===true) {
				$end = $_POST['end_date'];
				$params[] = $end;
				$where .= "AND (ps.end_date <= %s)\n";
			}
		}
		if ( isset( $_POST['team']) && empty( $_POST['team'] ) ===false ) {
			$splittedTeam = explode("|", $_POST['team']);
			$where .= "AND (";
			foreach ($splittedTeam as $key=>$val){
				//$where .= "tp.team like '%".$splittedTeam[$key]."%'";
				// and LOWER(tp.team) = LOWER(SUBSTRING('Marco Piras - marco.piras@alkemy.com', 1, INSTR('Marco Piras - marco.piras@alkemy.com', ' - ')))
				$where .= "LOWER(tm.cognome_nome) = LOWER(SUBSTRING('".$splittedTeam[$key]."', 1, INSTR('".$splittedTeam[$key]."', ' - ')))";
				if($key<count($splittedTeam)-1){
					$where .= " OR ";
				}
			}
			$where .= ") ";
			//$params[] = '%' . $_POST['team'] . '%';
			//$where .= "AND tp.team LIKE %s\n";
		}
		//offset e limit usati per la paginazione
		if ( isset( $_POST['offset'] ) && $_POST['offset'] ) {
			$params[] = $_POST['offset'];
		} else {
			$params[] = 0;
		}
		if ( isset( $_POST['limit'] ) && $_POST['limit'] ) {
			$params[] = $_POST['limit'];
		} else {
			$params[] = 100;
		}

		$sql = "SELECT SQL_CALC_FOUND_ROWS ps.*, c.name as client, ind.name as industry, GROUP_CONCAT(tm.cognome_nome) AS teams FROM project_sheets  AS ps
                JOIN team_projects AS tp ON tp.project_id = ps.project_id
                join team_members tm on tm.team_id = tp.team_id
                JOIN clients c on c.id = ps.client_id
				join projects_tags pt on pt.project_id = ps.project_id
				join projects_skills psk on ps.project_id = psk.project_id
				join skills sk on psk.skill_id = sk.id
				left join industries ind on ind.id = ps.client_industry_id
				join sheets_status ss on ps.project_id = ss.project_id
                WHERE 1=1
                $where
                GROUP BY ps.project_id
                ORDER BY start_date DESC
                LIMIT %d,%d ";
		$res = $wpdb->get_results( $wpdb->prepare( $sql, $params ), OBJECT );

		// $sql ="SELECT team FROM team_projects WHERE project_id = %d";
		// $teams = $wpdb->get_results( $wpdb->prepare( $sql, $res['projec'] ), OBJECT );

		$count = $wpdb->get_results( "SELECT FOUND_ROWS() AS count;", OBJECT );

		echo json_encode( [ data => $res, count => $count ] );
	}

	if ( $_POST['subaction'] == "search_clients" ) {
		$where  = '';
		$orderBy = '';
		$params = [];

		//offset e limit usati per la paginazione
		if ( isset( $_POST['offset'] ) && $_POST['offset'] ) {
			$params[] = $_POST['offset'];
		} else {
			$params[] = 0;
		}
		if ( isset( $_POST['limit'] ) && $_POST['limit'] ) {
			$params[] = $_POST['limit'];
		} else {
			$params[] = 1000;
		}
		//FILTRI
		if ( isset( $_POST['client_industry'] ) && empty( $_POST['client_industry'] ) ===false ) {
			$tmp = "'".str_replace("|", "','", $_POST['client_industry'])."'";
			$where .= "AND UPPER(ind.name) IN (".$tmp.")\n";
		}
		if ( isset( $_POST['country'] ) && empty( $_POST['country'] ) ===false) {
			$tmp = "'".str_replace("|", "','", $_POST['country'])."'";
			$where .= "AND UPPER(ps.country) IN (".$tmp.")\n";
		}
		if ( isset( $_POST['skills'] ) && empty( $_POST['skills'] ) ===false) {
			$tmp = "'".str_replace("|", "','", $_POST['skills'])."'";
			$where .= "AND UPPER(sk.name) IN (".$tmp.")\n";

		}
		//ordinamento
		if ( isset( $_POST['order_by'] ) && empty( $_POST['order_by'] ) ===false ) {
			$orderBy .= $_POST['order_by'];
		}
		else{
			$orderBy = 'ASC';
		}

		$sql = "SELECT SQL_CALC_FOUND_ROWS c.id, c.name, count(ps.project_name) as related_projects 
				FROM clients c
				left join project_sheets  AS ps on c.id = ps.client_id
				left join industries ind on ind.id = ps.client_industry_id
				left join projects_skills psk on psk.project_id = ps.project_id
				left join skills sk on sk.id = psk.skill_id
				WHERE 1=1
				$where
				GROUP BY c.id, c.name
				ORDER BY c.name $orderBy
				LIMIT %d, %d
				";

		$res = $wpdb->get_results( $wpdb->prepare( $sql, $params ), OBJECT );

		$count = $wpdb->get_results( "SELECT FOUND_ROWS() AS count;", OBJECT );

		echo json_encode( [ data => $res, count => $count] );
	}

	if ( $_POST['subaction'] == "search_industries" ) {
		$where  = '';
		$params = [];

		//offset e limit usati per la paginazione
		if ( isset( $_POST['offset'] ) && $_POST['offset'] ) {
			$params[] = $_POST['offset'];
		} else {
			$params[] = 0;
		}
		if ( isset( $_POST['limit'] ) && $_POST['limit'] ) {
			$params[] = $_POST['limit'];
		} else {
			$params[] = 1000;
		}
		//FILTRI
		if ( isset( $_POST['client_name'] ) && empty( $_POST['client_name'] ) ===false) {
			$tmp = "'".str_replace("|", "','", $_POST['client_name'])."'";
			$where .= "AND UPPER(cl.name) IN (".$tmp.")\n";
		}
		if ( isset( $_POST['country'] ) && empty( $_POST['country'] ) ===false) {
			$tmp = "'".str_replace("|", "','", $_POST['country'])."'";
			$where .= "AND UPPER(ps.country) IN (".$tmp.")\n";
		}
		if ( isset( $_POST['skills'] ) && empty( $_POST['skills'] ) ===false) {
			$tmp = "'".str_replace("|", "','", $_POST['skills'])."'";
			$where .= "AND UPPER(sk.name) IN (".$tmp.")\n";
		}
		//ordinamento
		if ( isset( $_POST['order_by'] ) && empty( $_POST['order_by'] ) ===false ) {
			$orderBy .= $_POST['order_by'];
		}
		else{
			$orderBy = 'ASC';
		}

		$sql = "SELECT SQL_CALC_FOUND_ROWS i.id, i.name, count(ps.project_name) as related_projects 
				FROM industries i
				left join project_sheets  AS ps on i.id = ps.client_industry_id
				left join clients cl on cl.id = ps.client_id
				left join projects_skills psk on psk.project_id = ps.project_id
				left join skills sk on sk.id = psk.skill_id
				WHERE 1=1
				$where
				GROUP BY i.id, i.name
				ORDER BY i.name $orderBy
				LIMIT %d, %d
				";
		$res = $wpdb->get_results( $wpdb->prepare( $sql, $params ), OBJECT );

		$count = $wpdb->get_results( "SELECT FOUND_ROWS() AS count;", OBJECT );

		echo json_encode( [ data => $res, count => $count] );
	}

	if ( $_POST['subaction'] == "all_skills" ) {

		$sql = "select id, UPPER(name) name from skills order by name";
		$res = $wpdb->get_results($wpdb->prepare( $sql, $params ), OBJECT );
		echo json_encode( [ data => $res] );
	}
	if ( $_POST['subaction'] == "all_clients" ) {

		$sql = "select id, UPPER(name) name from clients order by name";
		$res = $wpdb->get_results($wpdb->prepare( $sql, $params ), OBJECT );
		echo json_encode( [ data => $res] );
	}
	if ( $_POST['subaction'] == "all_industries" ) {

		$sql = "select id, UPPER(name) name from industries order by name";
		$res = $wpdb->get_results($wpdb->prepare( $sql, $params ), OBJECT );
		echo json_encode( [ data => $res] );
	}
	if ( $_POST['subaction'] == "all_resources" ) {
		$sql = "select team_id as id, cognome_nome as name from team_members order by cognome_nome";
		$res = $wpdb->get_results($wpdb->prepare( $sql, $params ), OBJECT );
		echo json_encode( [ data => $res] );
	}
	if ( $_POST['subaction'] == "all_countries" ) {
		$sql = "select 1 as id, 'Italy' as country from dual union all select 2 as id, 'Mexico' as country from dual union all select 3 as id, 'Other' as country from dual
				union all select 4 as id, 'South Eastern Europe' as country from dual union all select 5 as id, 'Spain' as country from dual";
		$res = $wpdb->get_results($wpdb->prepare( $sql, $params ), OBJECT );
		echo json_encode( [ data => $res] );
	}

	if ( $_POST['subaction'] == "view_client" ) {
		$where  = '';
		$params = [];
		$params[] = $_POST['client_selected'];
		//paginazione
		if ( isset( $_POST['offset'] ) && $_POST['offset'] ) {
			$params[] = $_POST['offset'];
		} else {
			$params[] = 0;
		}
		if ( isset( $_POST['limit'] ) && $_POST['limit'] ) {
			$params[] = $_POST['limit'];
		} else {
			$params[] = 100;
		}
		if ( isset( $_POST['client_industry'] ) && empty( $_POST['client_industry'] ) ===false) {
			$tmp = "'".str_replace("|", "','", $_POST['client_industry'])."'";
			//$params[] = $_POST['client_industry'];
			$where .= "AND UPPER(ind.name) IN (".$tmp.")\n";
		}
		if ( isset( $_POST['country'] ) && empty( $_POST['country'] ) ===false ) {
			$tmp = "'".str_replace("|", "','", $_POST['country'])."'";
			//$params[] = $_POST['country'];
			$where .= "AND UPPER(ps.country) IN (".$tmp.")\n";
		}
		if ( isset( $_POST['skills'] ) && empty( $_POST['skills'] ) ===false) {
			$tmp = "'".str_replace("|", "','", $_POST['skills'])."'";
			$where .= "AND UPPER(sk.name) IN (".$tmp.")\n";
		}
		if ( isset( $_POST['start_date'] ) && empty( $_POST['start_date'] ) ===false) {
			$dateSplitted = explode('-', $_POST['start_date']);
			//faccio un controllo di validazione della data
			if (count($dateSplitted) === 3 && checkdate($dateSplitted[2], $dateSplitted[1], $dateSplitted[0])===true) {
				$start = $_POST['start_date'];
				$params[] = $start;
				$where .= "AND (ps.start_date >= %s)\n";
			}
		}
		if ( isset( $_POST['end_date'] ) && empty( $_POST['end_date'] ) ===false ) {
			$dateSplitted = explode('-', $_POST['end_date']);
			//faccio un controllo di validazione della data
			if (count($dateSplitted) === 3 && checkdate($dateSplitted[2], $dateSplitted[1], $dateSplitted[0])===true) {
				$end = $_POST['end_date'];
				$params[] = $end;
				$where .= "AND (ps.end_date <= %s)\n";
			}
		}

		$sql = "SELECT SQL_CALC_FOUND_ROWS c.id, c.name as client_name, 
				if(ind.name is null,'',ind.name) industry,
				ps.project_id,
				if(ps.project_name is null,'',ps.project_name) project_name,
				if(ps.client_owner is null,'',ps.client_owner) client_owner, 
				if(ps.email is null,'',ps.email) email
				FROM clients c
				LEFT JOIN  project_sheets  AS ps on c.id = ps.client_id
				LEFT join industries ind on ind.id = ps.client_industry_id
				LEFT join projects_skills psk on psk.project_id = ps.project_id
				LEFT join skills sk on sk.id = psk.skill_id
				WHERE 1=1
				and c.id=%d
				$where
				GROUP BY c.id, ps.project_id
				ORDER BY ps.start_date DESC
				LIMIT %d, %d
				";
		$res = $wpdb->get_results( $wpdb->prepare( $sql, $params ), OBJECT );

		$count = $wpdb->get_results( "SELECT FOUND_ROWS() AS count;", OBJECT );

		echo json_encode( [ data => $res, count => $count] );
	}

	if ( $_POST['subaction'] == "view_industry" ) {
		$where  = '';
		$params = [];
		$params[] = $_POST['industry_selected'];
		//paginazione
		if ( isset( $_POST['offset'] ) && $_POST['offset'] ) {
			$params[] = $_POST['offset'];
		} else {
			$params[] = 0;
		}
		if ( isset( $_POST['limit'] ) && $_POST['limit'] ) {
			$params[] = $_POST['limit'];
		} else {
			$params[] = 100;
		}
		if ( isset( $_POST['client_industry'] ) && empty( $_POST['client_industry']) ===false) {
			$tmp = "'".str_replace("|", "','", $_POST['client_industry'])."'";
			//$params[] = $_POST['client_industry'];
			$where .= "AND UPPER(ind.name) IN (".$tmp.")\n";
		}
		if ( isset( $_POST['country'] ) && empty( $_POST['country']) ===false) {
			$tmp = "'".str_replace("|", "','", $_POST['country'])."'";
			//$params[] = $_POST['country'];
			$where .= "AND UPPER(ps.country) IN (".$tmp.")\n";
		}
		if ( isset( $_POST['skills'] ) && empty( $_POST['skills']) ===false) {
			$tmp = "'".str_replace("|", "','", $_POST['skills'])."'";
			$where .= "AND UPPER(sk.name) IN (".$tmp.")\n";
		}
		if ( isset( $_POST['start_date'] )&& empty( $_POST['start_date']) ===false) {
			$dateSplitted = explode('-', $_POST['start_date']);
			//faccio un controllo di validazione della data
			if (count($dateSplitted) === 3 && checkdate($dateSplitted[2], $dateSplitted[1], $dateSplitted[0])===true) {
				$start = $_POST['start_date'];
				$params[] = $start;
				$where .= "AND (ps.start_date >= %s)\n";
			}
		}
		if ( isset( $_POST['end_date'] )  && empty( $_POST['end_date']) ===false) {
			$dateSplitted = explode('-', $_POST['end_date']);
			//faccio un controllo di validazione della data
			if (count($dateSplitted) === 3 && checkdate($dateSplitted[2], $dateSplitted[1], $dateSplitted[0])===true) {
				$end = $_POST['end_date'];
				$params[] = $end;
				$where .= "AND (ps.end_date <= %s)\n";
			}
		}

		$sql = "SELECT SQL_CALC_FOUND_ROWS ind.id, ind.name as industry, 
				if(c.name is null,'',c.name) client_name, 
				ps.project_id,
				if(ps.project_name is null,'',ps.project_name) project_name, 
				if(ps.client_owner is null,'',ps.client_owner) client_owner,  
				if(ps.email is null,'',ps.email) email
				FROM industries ind
				LEFT JOIN  project_sheets  AS ps on ind.id = ps.client_industry_id
				LEFT join  clients c on c.id = ps.client_id
				WHERE 1=1
				and ind.id= %d
				$where
				GROUP BY ind.id, ps.project_id
				ORDER BY ps.start_date DESC
				LIMIT %d, %d
				";
		$res = $wpdb->get_results( $wpdb->prepare( $sql, $params ), OBJECT );

		$count = $wpdb->get_results( "SELECT FOUND_ROWS() AS count;", OBJECT );

		echo json_encode( [ data => $res, count => $count] );
	}

	if ( $_POST['subaction'] == "get_team_by_project" ) {
		$where  = '';
		$params = [];
		if ( isset( $_POST['project_id'] ) ) {
			$params[] = $_POST['project_id'];
		}
		$sql = "select tm.team_id as id, tm.cognome_nome, tm.job_title, if(mlui.avatar is null,'',mlui.avatar) avatar 
				from team_members tm
				left join team_projects tp on tp.team_id = tm.team_id
				left join wp_users wpu on wpu.user_email = tm.mail
				left join mylake_user_info mlui on mlui.id_user = wpu.id 
				where tp.project_id = %d";
		$res = $wpdb->get_results( $wpdb->prepare( $sql, $params ), OBJECT );
		echo json_encode( [ data => $res] );
	}
	//progetti correlati di un progetto visualizzato (tutti i progetti con almeno un tag uguale del progetto visualizzato)
	if ( $_POST['subaction'] == "get_related_project_by_tag" ) {
		$where  = '';
		$params = [];
		if ( isset( $_POST['project_id'] ) ) {
			$params[] = $_POST['project_id'];
			$params[] = $_POST['project_id'];
		}
		//esempio id 33
		$sql = "select ps.*, c.name as client, ind.name as industry
				FROM project_sheets  AS ps
				JOIN team_projects AS tp ON tp.project_id = ps.project_id
				JOIN clients c on c.id = ps.client_id
				join projects_tags pt on pt.project_id = ps.project_id
				join projects_skills psk on ps.project_id = psk.project_id
				join skills sk on psk.skill_id = sk.id
				join industries ind on ind.id = ps.client_industry_id
				where ps.project_id != %d and pt.tag in 
				(select tag from projects_tags 
				where project_id = %d)
				GROUP BY ps.project_id";
		$res = $wpdb->get_results( $wpdb->prepare( $sql, $params ), OBJECT );
		echo json_encode( [ data => $res] );
	}

  wp_die();
}

// Ajax endpoint for the search
add_action( 'wp_ajax_getavatar', 'my_ajax_getavatar_handler' );

function my_ajax_getavatar_handler() {
	require_once ABSPATH . '/script/pdocrud.php';

	global $wpdb;
	if(isset($_SESSION["pdocrud_obj"])) {
		$projects = unserialize($_SESSION["pdocrud_obj"]);
	}else {
		$projects = new PDOCrud();
	}
	$resp = $projects->getAvatarApiLink($_POST['name'], $_POST['bgcolor'] , $_POST['fgcolor']);
	print_r($resp);
	wp_die();
}

?>