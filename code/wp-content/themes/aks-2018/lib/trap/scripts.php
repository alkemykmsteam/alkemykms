<?php 

	function site_scripts_and_css() {
        $dati_tema = wp_get_theme();
        //FILE CSS
        //wp_enqueue_style( 'jqui', get_template_directory_uri() . "/jquery-ui.css", array(), $dati_tema->Version);

        wp_enqueue_style( 'select2', get_template_directory_uri() . "/select2.min.css", array(), $dati_tema->Version);
        wp_enqueue_style( 'pikaday', get_template_directory_uri() . "/pikaday.css", array(), $dati_tema->Version);
        wp_enqueue_style( 'typography', get_template_directory_uri() . "/typography.css", array(), $dati_tema->Version);
        wp_enqueue_style( 'icons', get_template_directory_uri() . "/icons.css", $dati_tema->Version);
        wp_enqueue_style( 'summernote', get_template_directory_uri() . "/summernote-lite.css", array('style'), $dati_tema->Version);
        wp_enqueue_style( 'slick', get_template_directory_uri() . "/slick.css", array('style'), $dati_tema->Version);

        wp_enqueue_style( 'style', get_stylesheet_uri(), $dati_tema->Version);
        wp_enqueue_style( 'responsive', get_template_directory_uri() . "/responsive.css", array('style'), $dati_tema->Version, '(min-width: 480px)');

        //FILE SCRIPTS
        wp_register_script( 'jqueryui', get_template_directory_uri() . "/js/jquery-ui.min.js", array('jquery'), '1.0', true);
        wp_register_script( 'imagesloaded', get_template_directory_uri() . "/js/select2.multi-checkboxes.js", array('jquery'), '1.0', true);

        wp_register_script( 'ofi', get_template_directory_uri() . "/js/ofi.min.js", array('jquery'), '1.0', true);
        wp_register_script( 'pikaday', get_template_directory_uri() . "/js/pikaday.js", array('jquery'), '1.0', true);
        wp_register_script( 'touch-punch', get_template_directory_uri() . "/js/jquery.touch-punch.min.js", array('jquery'), '1.0', true);
        wp_register_script( 'shapeshift', get_template_directory_uri() . "/js/jquery.shapeshift.js", array('jquery'), '1.0', true);
        wp_register_script( 'select2', get_template_directory_uri() . "/js/select2.min.js", array('jquery'), '1.0', true);
        wp_register_script( 'select2multicheck', get_template_directory_uri() . "/js/select2.multi-checkboxes.js", array('jquery'), '1.0', true);
        wp_register_script( 'summernote', get_template_directory_uri() . "/js/summernote-lite.min.js", array('jquery'), '1.0', true);
        wp_register_script( 'slick', get_template_directory_uri() . "/js/slick.min.js", array('jquery'), '1.0', true);

        wp_register_script( 'siteScripts', get_template_directory_uri() . "/js/scripts.js", array('jquery'), '1.0', true );
            
        wp_register_script( 'cmisPolyfills', get_template_directory_uri() . "/js/cmis-js/dist/cmis.polyfills.js", array('jquery'), '1.0', true );
        wp_register_script( 'cmisBundle', get_template_directory_uri() . "/js/cmis-js/dist/cmis.bundle.js", array('jquery'), '1.0', true );



            //VARIABILI DA PASSARE A JS
        $var_array = array(
            'home' => get_bloginfo('url')
        );
        wp_enqueue_script('jqueryui');
        wp_enqueue_script('imagesloaded');
        wp_localize_script('siteScripts', 'php_vars', $var_array );
        wp_enqueue_script('ofi');
        wp_enqueue_script('pikaday');
		wp_enqueue_script('touch-punch');
        wp_enqueue_script('shapeshift');
        wp_enqueue_script('select2');
        wp_enqueue_script('select2multicheck');
        wp_enqueue_script('summernote');
        wp_enqueue_script('slick');

		wp_enqueue_script('siteScripts');
        wp_enqueue_script('cmisPolyfills');
        wp_enqueue_script('cmisBundle');
	}
    add_action( 'wp_enqueue_scripts', 'site_scripts_and_css' );


    	
?>