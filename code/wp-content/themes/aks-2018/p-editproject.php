<?php
/*
Template Name: Edit Project
*/
session_start();
require_once ABSPATH . '/script/pdocrud.php';
$projects = null;
if(isset($_SESSION["pdocrud_obj"])) {
    $projects = unserialize($_SESSION["pdocrud_obj"]);
}else {
    $projects = new PDOCrud();
}
$aksFunctions = $_SESSION['$aksFunctions'];
$editProject = false;
$waitingForApproval = false;
if(isset($_GET['id']) === false){
    $id=-1;
}else{
    $id = $_GET['id'];
    $editProject = $projects->isProjectEditable($id);
    $waitingForApproval = $projects->isProjectWaitingForApproval($id);
}
$tmp = $projects->getProjectData(intval($id), ($editProject && !$waitingForApproval));
$output = json_decode($tmp);
if(isset($_SESSION['myLakePages'])) {
    $myLakePages = $_SESSION['myLakePages'];
}else{
    $myLakePages = $projects->loadMyLakePages();
    $_SESSION['myLakePages'] = $myLakePages;
}
get_header();
?>

<!--
<?php print_r($output); ?>
-->
<script>
    jQuery(document).ready(function($) {
        jQuery('.page-template-p-editproject .c-tabs__item').on('click',function(){
            jQuery('.page-template-p-editproject .c-tabs__item').removeClass('j-active');
            jQuery('.c-wysiwyg-container').hide();
            jQuery(this).addClass('j-active');
            jQuery('.c-wysiwyg-container').filter('[data-textarea="'+jQuery(this).attr('href')+'"]').show();
            return false;
        })
        jQuery('#situation, #complication, #solution, #results').summernote({
            height: 585,
            disableResizeEditor: true,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline']],
                ['link']
            ]
        });
        jQuery('.c-wysiwyg-container').hide();
        jQuery('.c-wysiwyg-container:first').show();
        jQuery('.c-approval__footer__back').on('click', function(){
            jQuery('body').removeClass('j-approval-open');
        })
        jQuery("#editproject").on('submit',function(e) {
            var form = jQuery(this);
            var url = form.attr('action');
            //console.log(form.serialize());
            //console.log(url);
            jQuery.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                success: function(response)
                    {
                        var respJSON = JSON.parse(response);
                        console.log(respJSON);
                        if(respJSON['error'] == false){
                            jQuery('.c-approval__title').text('Great!');
                            jQuery('.c-approval__lead').text(respJSON['message']);
                            jQuery('.c-approval__footer__buttons .c-button').attr('href', jQuery('.c-approval__footer__buttons .c-button').attr('data-url')+'?id='+respJSON['projectId']);
                            jQuery('body').addClass('j-approval-open');
                            if(respJSON['message'] == 'Project successfully published!'){
                                var unpublishUrl = '<?php echo $myLakePages["undo_publishpage_url"] ?>';
                                jQuery('#j-undo').show();
                                jQuery('#j-undo').on('click',function(){
                                    console.log(unpublishUrl+'?id='+respJSON['projectId']);
                                    undoPublishProject(unpublishUrl+'?id='+respJSON['projectId']);
                                })
                            }
                        }else{
                            jQuery('.c-approval__title').text('Error');
                            jQuery('.c-approval__lead').text(respJSON['message']);
                            jQuery('.c-approval__footer__buttons .c-button').attr('href', jQuery('.c-approval__footer__buttons .c-button').attr('data-url')+'?id='+respJSON['projectId']);
                            jQuery('body').addClass('j-approval-open');
                        }
                    }
                });

                e.preventDefault(); // avoid to execute the actual submit of the form.
            });
        jQuery('#savedraft').click(function(){
            jQuery("#operationType").val('<?php echo AksColumns::ColumnOperationTypeBozza; ?>');
            if(jQuery('#project_name').val() != '' && jQuery('select[name=client]').val().length > 0 && jQuery('select[name=client_owner]').val().length > 0  && jQuery('select[name=country]').val().length > 0  && jQuery('select[name=client_owner]').val().length > 0 && jQuery('select[name=team]').val().length > 0 && jQuery('select[name=tag]').val().length > 0 && jQuery('#start').val() != ''  && jQuery('#end').val() != ''){
                jQuery("#editproject").submit();
            }else{
                alert('Please fill in all the fields');
            }
            return false;
        })
        jQuery('#submitproject').click(function(){
            jQuery("#operationType").val('<?php echo AksColumns::ColumnOperationTypeApproval; ?>');
            if(jQuery('#project_name').val() != '' && jQuery('select[name=client]').val().length > 0 && jQuery('select[name=client_owner]').val().length > 0  && jQuery('select[name=country]').val().length > 0  && jQuery('select[name=client_owner]').val().length > 0 && jQuery('select[name=team]').val().length > 0 && jQuery('select[name=tag]').val().length > 0 && jQuery('#start').val() != ''  && jQuery('#end').val() != ''){
                jQuery("#editproject").submit();
            }else{
                alert('Please fill in all the fields');
            }
            return false;
        })
        jQuery('#publishproject').click(function(){
            //jQuery('#editproject').attr('action', '../<?php //echo $myLakePages['publishpage_url'] ?>');
            jQuery('#editproject').attr('action', '<?php echo $myLakePages['publishpage_url'] ?>');
            if(jQuery('#project_name').val() != '' && jQuery('select[name=client]').val().length > 0 && jQuery('select[name=client_owner]').val().length > 0  && jQuery('select[name=country]').val().length > 0  && jQuery('select[name=client_owner]').val().length > 0 && jQuery('select[name=team]').val().length > 0 && jQuery('select[name=tag]').val().length > 0 && jQuery('#start').val() != ''  && jQuery('#end').val() != ''){
                jQuery("#editproject").submit();
            }else{
                alert('Please fill in all the fields');
            }
            return false;
        }) //
        //PREPOPOLAMENTO FILTRI
        jQuery.post(
            '/wp-admin/admin-ajax.php', {
                'action': 'search',
                'subaction': 'hydrate_edit'
            },
            function (response) {
                var respJSON = JSON.parse(response);
                console.log(respJSON)

                //CHANGE ELEMENTI
                jQuery('.j-multiple-select, .j-single-select, input').on('change', function(e) {
                    startDateAttr = jQuery('#start').val().split('/')[2] + '-'+jQuery('#start').val().split('/')[1] +'-'+jQuery('#start').val().split('/')[0] 
                    //queryObj.start_date = startDateAttr;
                    endDateAttr = jQuery('#end').val().split('/')[2] + '-'+jQuery('#end').val().split('/')[1] +'-'+jQuery('#end').val().split('/')[0] 
                    //queryObj.end_date = endDateAttr;
                    jQuery(this).parent().parent().find('label').show()
                });

                for (var i in respJSON)
                {
                    console.log(respJSON[i]['type_']);
                    if(respJSON[i]['type_'] != 'country'){
                        jQuery('select[name='+respJSON[i]['type_']+']').append('<option value="'+respJSON[i]['id']+'">'+respJSON[i]['NAME']+'</option>')
                    }else{
                        jQuery('select[name='+respJSON[i]['type_']+']').append('<option value="'+respJSON[i]['NAME']+'">'+respJSON[i]['NAME']+'</option>')
                    }
                }

                <?php if($output->start_date): ?>
                    var dateAttr = '<?php echo $output->start_date ?>';
                    startDate = new Date(dateAttr);
                    updateStartDate();
                    dateAttr = dateAttr.split('-')[2] + '/'+dateAttr.split('-')[1] +'/'+dateAttr.split('-')[0] 
                    jQuery('#start').val(dateAttr);
                <?php endif; ?>
                <?php if($output->end_date): ?>
                    var dateAttr = '<?php echo $output->end_date ?>';
                    endDate = new Date(dateAttr);
                    updateEndDate();
                    dateAttr = dateAttr.split('-')[2] + '/'+dateAttr.split('-')[1] +'/'+dateAttr.split('-')[0] 
                    jQuery('#end').val(dateAttr);
                <?php endif; ?>

                //CAMPI CHECKED
                <?php if($editProject === true){ ?>
                    <?php if($output->client_id): ?>
                        var str = '<?php echo($output->client); ?>';
                        var str2 = '<?php echo($output->client_id); ?>';

                        var arr = str.toUpperCase().split('|');
                        var arr2 = str2.toUpperCase().split('|');

                        for(var c in arr2){
                            jQuery('select[name=client]').val(arr2);
                        }
                        jQuery('select[name=client]').trigger('change');
                        jQuery('select[name=client]').trigger('select2:unselect');
                        jQuery('select[name=client]').siblings('span.select2').append("<span class='selected selected--edit selected--default'>"+arr[0]+"</span>").on('click',function(){
                            jQuery('select[name=client]').select2('open');
                        });
                    <?php endif; ?>
                    <?php if($output->client_owner): ?>
                        var str = '<?php echo($output->client_owner); ?>';
                        var arr = str.split('|');
                        console.log(arr);
                        for(var c in arr){
                            jQuery('select[name=client_owner]').val(arr);
                        }
                        jQuery('select[name=client_owner]').trigger('change');
                        jQuery('select[name=client_owner]').trigger('select2:unselect');
/*
                        jQuery('select[name=client_owner]').siblings('span.select2').append("<span class='selected selected--default'>("+arr.length+") "+jQuery('select[name=client_owner]').attr('data-datatype')+"</span>").on('click',function(){
                            jQuery('select[name=client_owner]').select2('open');
                        });*/
                    <?php endif; ?>
                    <?php if($output->country): ?>
                        var str = '<?php echo($output->country); ?>';
                        var arr = str.split('|');
                        console.log(arr);

                        for(var c in arr){
                            jQuery('select[name=country]').val(arr);
                        }
                        jQuery('select[name=country]').trigger('change');
                        jQuery('select[name=country]').trigger('select2:unselect');
/*
                        jQuery('select[name=country]').siblings('span.select2').append("<span class='selected selected--edit selected--default'>("+arr.length+") "+arr+"</span>").on('click',function(){
                            jQuery('select[name=country]').select2('open');
                        });
*/
                    <?php endif; ?>
                    <?php if($output->skills): ?>
                        var str = '<?php echo($output->skills); ?>';
                        var str2 = '<?php echo($output->skills_id); ?>';

                        var arr = str.split('|');
                        var arr2 = str2.split('|');

                        console.log(arr);

                        for(var c in arr2){
                            jQuery('select[name=skills]').val(arr2);
                        }
                        jQuery('select[name=skills]').trigger('change');
                        jQuery('select[name=skills]').trigger('select2:unselect');

                        jQuery('select[name=skills]').siblings('span.select2').append("<span class='selected selected--edit selected--default'>("+arr.length+") "+arr+"</span>").on('click',function(){
                            jQuery('select[name=skills]').select2('open');
                        });

                    <?php endif; ?>
                    <?php if($output->team): ?>
                        var str = '<?php echo($output->team); ?>';
                        var str2 = '<?php echo($output->teams_id); ?>';
                        var arr = str.split('|');
                        var arr2 = str2.split('|');

                        for(var c in arr2){
                            jQuery('select[name=team]').val(arr2);
                        }
                        jQuery('select[name=team]').trigger('change');
                        jQuery('select[name=team]').trigger('select2:unselect');

                        /*
                        jQuery('select[name=team]').siblings('span.select2').append("<span class='selected selected--default'>("+arr.length+") "+jQuery('select[name=team]').attr('data-datatype')+"</span>").on('click',function(){
                            jQuery('select[name=team]').select2('open');
                        });*/

                    <?php endif; ?>
                    <?php if($output->tags): ?>
                        var str = '<?php echo($output->tags); ?>';
                        var arr = str.split('|');
                        console.log(arr);

                        for(var c in arr){
                            jQuery('select[name=tag]').val(arr);
                        }
                        jQuery('select[name=tag]').trigger('change');
                        jQuery('select[name=tag]').trigger('select2:unselect');

                        /*
                        jQuery('select[name=tag]').siblings('span.select2').append("<span class='selected selected--default'>("+arr.length+") "+jQuery('select[name=tag]').attr('data-datatype')+"</span>").on('click',function(){
                            jQuery('select[name=tag]').select2('open');
                        });*/

                    <?php endif; ?>

                    <?php if($output->start_date): ?>
                    jQuery('#start').trigger('change');
                    <?php endif; ?>

                    <?php if($output->end_date): ?>
                    jQuery('#end').trigger('change');
                    <?php endif; ?>

                <?php } ?>


            });
        });
</script>
<section class="c-content-block c-advanced-search l-container">

    <form class="c-form c-form--edit" id="editproject" action="<?php echo $myLakePages['savepage_url']?>">
        <input type="hidden" name="project_id" value="<?php if($editProject === true){ echo $_GET['id']; }?>">
        <input type="hidden" name="operation_type" id="operationType" value="">
        <input type="hidden" name="hidden-tag" id="hidden-tag" value="">
        <input type="hidden" name="hidden-skills" id="hidden-skills" value="">
        <input type="hidden" name="hidden-team" id="hidden-team" value="">
        <input type="hidden" name="hidden-start_date" id="hidden-start_date">
        <input type="hidden" name="hidden-end_date" id="hidden-end_date">

        <header class="c-form__header">
            <div class="c-form__field">
                <label style="display:none">Project name (*)</label>
                <textarea class="j-autoexpand" id="project_name" name="project_name" rows="1" placeholder="Project name (*)" ><?php if($output->project_name): echo $output->project_name; endif;?></textarea>
            </div>
        </header>
        <div class="c-form__content">
            <fieldset class="c-form__fieldset">
                <div class="c-form__field">
                    <div class="c-form__field-inline c-single-select">
                        <label style="display:none">Client (*)</label>
                        <h3 class="c-single-select__title">Client <span class="icon icon-down"></span></h3>
                        <div class="c-select-container">
                            <select class="c-select j-single-select" name="client" data-placeholder="Client (*)" data-datatype="Clients">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="c-form__field">
                    <div class="c-form__field-inline c-single-select">
                        <label style="display:none">Client owner (*)</label>
                        <h3 class="c-single-select__title">Client owner<span class="icon icon-down"></span></h3>
                        <div class="c-select-container">

                        <select class="j-single-select" name="client_owner" multiple="multiple"  data-tags="false" data-placeholder="Client owner (*)" data-datatype="client owner">
                        </select>
                        </div>
                    </div>
                </div>

                <div class="c-form__field">
                    <div class="c-form__field-inline c-single-select">
                        <label style="display:none">Country (*)</label>
                        <h3 class="c-single-select__title">Country <span class="icon icon-down"></span></h3>
                        <div class="c-select-container">
                            <select class="c-select j-single-select" name="country"  data-placeholder="Country (*)" data-datatype="country">
                            </select>
                        </div>
                    </div>
                    <div class="c-form__field-inline c-multiple-select">
                        <label style="display:none">Skills (*)</label>
                        <h3 class="c-multiple-select__title">Skills <span class="icon icon-down"></span></h3>
                        <div class="c-select-container">
                            <select class="c-select j-multiple-select" name="skills"  data-targetfield="skills" data-placeholder="Skills (*)" data-datatype="skills">
                            </select>
                        </div>
                    </div>
                </div>

                <div class="c-form__field c-form__field--calendar">
                    <div class="c-form__field-inline c-calendar">
                        <label for="start">Start date (*)</label>
                        <input type="text" id="start" class="input--edit" name="start_date" data-targetfield="start_date" placeholder="dd/mm/yyyy">
                    </div>
                    <div class="c-form__field-inline c-calendar c-calendar--end">
                        <label for="end">End date (*)</label>
                        <input type="text" id="end" class="input--edit" name="end_date" data-targetfield="end_date" placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="c-form__field c-multiple-pillbox">
                    <label>Operative team (*)</label>
                    <select class="j-multiple-pillbox" name="team" data-tags="false" multiple="multiple" data-targetfield="team" data-placeholder="Operative team" data-datatype="operative team">
                    </select>
                </div>
                <div class="c-form__field c-multiple-pillbox">
                    <label>Tags (*)</label>
                    <select class="j-multiple-pillbox" name="tag" multiple="multiple"  data-targetfield="tag" data-placeholder="Tag" data-datatype="tag">
                    </select>
                </div>
                <footer class="c-form__fieldset__footer">
                    <a class="c-form__fieldset__footer__button" href="#"><span class="icon icon-text"></span> Add
                        documents</a>
                    <a class="c-form__fieldset__footer__button" href="#"><span class="icon icon-img"></span> Add
                        photo/video</a>
                </footer>
            </fieldset>
            <fieldset class="c-form__fieldset">
                <nav class="c-tabs">
                    <a href="#situation" class="c-tabs__item j-active">Situation</a>
                    <a href="#complication" class="c-tabs__item">Complication</a>
                    <a href="#solution" class="c-tabs__item">Solution</a>
                    <a href="#results" class="c-tabs__item">Result</a>
                </nav>

                <div class="c-wysiwyg-container" data-textarea="#situation"><textarea placeholder="500 words maximum" name="situation" id="situation"><?php if($output->situation): echo $output->situation; endif;?></textarea></div>
                <div class="c-wysiwyg-container" data-textarea="#complication"><textarea placeholder="500 words maximum" name="complication" id="complication"><?php if($output->complication): echo $output->complication; endif;?></textarea></div>
                <div class="c-wysiwyg-container" data-textarea="#results"><textarea placeholder="500 words maximum" name="results" id="results"><?php if($output->results): echo $output->results; endif;?></textarea></div>
                <div class="c-wysiwyg-container" data-textarea="#solution"><textarea placeholder="500 words maximum" name="solution" id="solution"><?php if($output->solution): echo $output->solution; endif;?></textarea></div>

            </fieldset>

        </div>
        <footer class="c-form__footer">
            <a href="<?php echo $myLakePages['searchsheets_url']?>" class="c-button c-button--cancel j-edit-no-complet"><span class="icon icon-cancel"></span>
                cancel</a>
            <div class="c-form__footer__buttons">
                <?php
                if($editProject === true || $id < 0) { ?>
                    <a href="#" class="c-button c-button--ghost" id="savedraft"><span class="icon icon-save"></span>
                        Save Draft</a>
                    <?php
                }
                    if(isset($aksFunctions[AksFunctions::EditProject]) && isset($aksFunctions[AksFunctions::PublishProject]) === false && $id > 0 && $waitingForApproval === false){ ?>
                    <a href="#" class="c-button c-button--green j-approval" id="submitproject"><span class="icon icon-publish"></span> Submit for approval</a>
                <?php
                    }
                    if(isset($aksFunctions[AksFunctions::PublishProject]) && $id > 0){ ?>
                    <a href="#" class="c-button c-button--green j-approval" id="publishproject"><span class="icon icon-publish"></span> Publish</a>
                <?php
                    }?>
            </div>

        </footer>
    </form>

</section>
</main>

<div class="c-edit-no-complet">
    <a class="c-edit-no-complet__content__close j-edit-no-complet-close" href="#"><span class="icon icon-close"></span></a>
    <div class="c-edit-no-complet__content">
        <span class="icon icon-project-edit"></span>
        <h2 class="c-edit-no-complet__title t-title2">Editing is not completed</h2>
        <h3 class="c-edit-no-complet__lead t-title4">Are you sure to leave?</h3>
        <footer class="c-edit-no-complet__footer">
            <div class="c-edit-no-complet__footer__buttons">
                <a href="#" class="c-button c-button--ghost">Yes, take me out</a>
                <a href="#" class="c-button">Save Draft</a>
            </div>
<!--            <a class="c-edit-no-complet__footer__back j-edit-no-complet-close" href="#">Back to edit</a>-->
        </footer>
    </div>
</div>

<div class="c-approval">
    <a href="http://localhost" class="c-logo">
        <img src="<?php echo get_bloginfo('template_url'); ?>/img/logo.svg" alt="">
    </a>
    <div class="c-approval__content">
        <span class="icon icon-publish"></span>
        <h2 class="c-approval__title t-title2"></h2>
        <h3 class="c-approval__lead t-title4"></h3>
        <footer class="c-approval__footer">
            <div class="c-approval__footer__buttons">
                <a href="#" data-url="<?php echo $myLakePages['viewpage_url'] ?>" class="c-button c-button--ghost">Watch project</a>
            </div>
            <a class="c-approval__footer__back j-approval-close" id="j-undo" style="display:none;" href="#">Undo</a>
        </footer>
    </div>
</div>
</div>

<?php get_footer(); ?>