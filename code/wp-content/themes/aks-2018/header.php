<?php @session_start(); ?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
  	
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="theme-color" content="#000000">
    
    <title><?php wp_title(' - ', true, 'right'); ?> <?php bloginfo('name'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://use.typekit.net/xbp8tlm.css">

    
	<?php wp_head(); ?>
    
</head>

<body <?php body_class(); ?>>

    <?php if(is_page_template('p-serp.php') || is_page_template('p-clienti.php')  || is_page_template('p-industry.php')  || is_page_template('p-industry-selected.php') ): ?>
        <div class="l-page l-page--small">
    <?php else: ?>
        <div class="l-page">
    <?php endif; ?>
            <div class="l-header">
            
                <header class="c-site-header">
                    <div class="l-container">
                        <a href="<?php echo get_bloginfo('url'); ?>" class="c-logo">
                            <img src="<?php echo get_bloginfo('template_url'); ?>/img/logo.svg" alt="">
                        </a>
                
                        <a class="c-site-header__toggle j-toggle">
                            <span class="c-site-header__toggle__item"></span>
                            <span class="c-site-header__toggle__item"></span>
                            <span class="c-site-header__toggle__item"></span>
                        </a>
                
                        <nav class="c-site-header__nav">
                            <ul class="c-site-header__nav__menu">
                                <?php
                                    $args = array(
                                    'theme_location' => 'Primario',
                                    'depth'    => 1,
                                    'items_wrap' => '%3$s',
                                    'container' => ''
                                    );

                                    wp_nav_menu($args);
                                ?>
                                <li>
                                    <a class="c-nav-user j-notification" href="<?php echo wp_logout_url() ?>" data-notifications="0">
                                        <figure class="c-nav-user__picture j-notification-count" >
                                            <?php if(isset($_SESSION['g_avatar_url'])): ?>
                                                <img src="<?php echo $_SESSION['g_avatar_url'];?>"/>
                                            <?php endif; ?>
                                        </figure>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </header>
            </div>

        <main class="l-main">
