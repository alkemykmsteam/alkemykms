<?php
?>
<!-- SIDEBAR-->
<aside class="c-sidebar">
        <header class="c-sidebar__header">
            <h2 class="c-sidebar__title t-title2">Hi <?php echo wp_get_current_user()->user_firstname;?> <?php echo wp_get_current_user()->user_lastname;?></h2>
            <h3 class="c-sidebar__lead t-title5"></h3>
            <a class="j-sidebar-close c-sidebar__header__close"><span class="icon icon-close"></span></a>
        </header>
        <div class="c-sidebar__content j-sidebar__content">
            <!--
            <div class="c-notification">
                <header class="c-notification__header">
                    <figure class="c-notification__header__figure">
                        <img src="http://www.ra.ac.ae/wp-content/uploads/2017/02/user-icon-placeholder.png">
                    </figure>
                    <h4 class="c-notification__header__lead t-title4">Sandro Maradei create a new project</h4>
                        <h3 class="c-notification__header__title t-title3">Vodafone Community</h3>
                </header>
                <div class="c-notification__content">
                    On Monday 09/11/2018, at 4.30 pm
                </div>
                <footer class="c-notification__footer">
                    <a class="c-notification__footer__link" href="#"><span class="icon icon-modify"></span> edit</a>
                    <a class="c-notification__footer__link" href="#"><span class="icon icon-publish"></span> publish</a>
                </footer>
            </div>
            <div class="c-notification c-notification--small">
                <header class="c-notification__header">
                    <h3 class="c-notification__header__title t-title3">Vodafone Community</h3>
                </header>
                <div class="c-notification__content">
                    On Monday 09/11/2018, at 4.30 pm
                </div>
                <footer class="c-notification__footer">
                    <a class="c-notification__footer__link" href="#"><span class="icon icon-modify"></span> edit draft</a>
                </footer>
            </div>
            <div class="c-notification">
                <header class="c-notification__header">
                    <figure class="c-notification__header__figure">
                        <img src="http://www.ra.ac.ae/wp-content/uploads/2017/02/user-icon-placeholder.png">
                    </figure>
                    <h4 class="c-notification__header__lead t-title4">Sandro Maradei create a new project</h4>
                        <h3 class="c-notification__header__title t-title3">Vodafone Community</h3>
                </header>
                <div class="c-notification__content">
                    On Monday 09/11/2018, at 4.30 pm
                </div>
                <footer class="c-notification__footer">
                    <a class="c-notification__footer__link" href="#"><span class="icon icon-modify"></span> edit</a>
                    <a class="c-notification__footer__link" href="#"><span class="icon icon-publish"></span> publish</a>
                </footer>
            </div>
            <div class="c-notification">
                <header class="c-notification__header">
                    <figure class="c-notification__header__figure">
                        <img src="http://www.ra.ac.ae/wp-content/uploads/2017/02/user-icon-placeholder.png">
                    </figure>
                    <h4 class="c-notification__header__lead t-title4">Sandro Maradei create a new project</h4>
                        <h3 class="c-notification__header__title t-title3">Vodafone Community</h3>
                </header>
                <div class="c-notification__content">
                    On Monday 09/11/2018, at 4.30 pm
                </div>
                <footer class="c-notification__footer">
                        <a class="c-notification__footer__link" href="#"><span class="icon icon-modify"></span> edit</a>
                        <a class="c-notification__footer__link" href="#"><span class="icon icon-publish"></span> publish</a>
                </footer>
                <div class="c-notificiation__published">
                    <span class="icon icon-hand-ok"></span>
                    <p>This project has been published!</p>
                    <a href="#" class="c-notificiation__published__link">Undo</a>
                </div>
            </div>
            <div class="c-notification">
                <header class="c-notification__header">
                    <figure class="c-notification__header__figure">
                        <img src="http://www.ra.ac.ae/wp-content/uploads/2017/02/user-icon-placeholder.png">
                    </figure>
                    <h4 class="c-notification__header__lead t-title4">Sandro Maradei create a new project</h4>
                        <h3 class="c-notification__header__title t-title3">Vodafone Community</h3>
                </header>
                <div class="c-notification__content">
                    On Monday 09/11/2018, at 4.30 pm
                </div>
                <footer class="c-notification__footer">
                    <a class="c-notification__footer__link" href="#"><span class="icon icon-modify"></span> edit</a>
                    <a class="c-notification__footer__link" href="#"><span class="icon icon-publish"></span> publish</a>
                </footer>
            </div>
            -->
        </div>
        <a href="<?php echo wp_logout_url( ); ?> " class="c-sidebar__footer">
            <h2 class="c-sidebar__footer__title t-title2">
                Logout
            </h2>
            <div class="c-sidebar__footer__link"><span class="icon icon-logout"></span></div>
        </a>
    </aside>
    <!--FINE SIDEBAR-->
    <script>

    jQuery(document).ready(function(){
    // updating the view with notifications using ajax
        function load_unseen_notification(view = ''){
            jQuery.ajax({
                url:"../fetch",
                method:"POST",
                data:{view:view},
                dataType:"json",
                success:function(data){
                    //console.log(data);
                    jQuery('.j-sidebar__content').html(data.notification);
                    if(data.unseen_notification > 0){
                        jQuery('.j-notification').attr('data-notifications',data.unseen_notification);
                    }
                }
            });
        }
        load_unseen_notification();

// load new notifications
        jQuery(document).on('click', '.dropdown-toggle', function(){
            jQuery('.count').html('');
            load_unseen_notification('yes');
        });
        setInterval(function(){
            load_unseen_notification();;
        }, 30000);
    });

</script>