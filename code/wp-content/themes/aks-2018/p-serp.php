<!--
<?php print_r($_POST); ?>
-->

<?php
session_start();
require_once ABSPATH . '/script/pdocrud.php';
$projects = null;
if(isset($_SESSION["pdocrud_obj"])) {
    $projects = unserialize($_SESSION["pdocrud_obj"]);
}else {
    $projects = new PDOCrud();
}

if(isset($_SESSION['myLakePages'])) {
    $myLakePages = $_SESSION['myLakePages'];
}else{
    $myLakePages = $projects->loadMyLakePages();
    $_SESSION['myLakePages'] = $myLakePages;
}

/* Template Name: SERP */
get_header(); ?>
<script>
    jQuery(document).ready(function($) {
        //PREPOPOLAMENTO FILTRI
        jQuery.post(
        '/wp-admin/admin-ajax.php', {
            'action': 'search',
            'subaction': 'hydrate'
        },
        function (response) {
            var respJSON = JSON.parse(response);
            for (var i in respJSON)
            {
                console.log(respJSON[i]['type_']);
                jQuery('select[name='+respJSON[i]['type_']+']').append('<option value="'+respJSON[i]['NAME']+'">'+respJSON[i]['NAME']+'</option>')
            }
            
            <?php if (isset($_POST['start_date']) && $_POST['start_date'] != ''): ?>
                var dateAttr = '<?php echo $_POST['start_date'] ?>';
                startDate = new Date(dateAttr);
                updateStartDate();
                dateAttr = dateAttr.split('-')[2] + '/'+dateAttr.split('-')[1] +'/'+dateAttr.split('-')[0] 
                jQuery('#start').val(dateAttr);
                //queryObj.start_date = '<?php echo $_POST['start_date'] ?>';
            <?php endif; ?>
            <?php if (isset($_POST['end_date']) && $_POST['end_date'] != ''): ?>
                var dateAttr = '<?php echo $_POST['end_date'] ?>';
                endDate = new Date(dateAttr);
                updateEndDate();
                dateAttr = dateAttr.split('-')[2] + '/'+dateAttr.split('-')[1] +'/'+dateAttr.split('-')[0] 
                jQuery('#end').val(dateAttr);
                //queryObj.end_date = '<?php echo $_POST['end_date'] ?>';
            <?php endif; ?>


            //EVENTS
            jQuery('.j-multiple-select, input').on('change', function(e) {
                jQuery('.c-result-item:not(.u-template)').remove();
                jQuery('.c-result-item.u-template').show();

                var client = jQuery('.j-multiple-select[name=client]').val();
                var industry = jQuery('.j-multiple-select[name=industry]').val();
                var country = jQuery('.j-multiple-select[name=country]').val();
                var skills = jQuery('.j-multiple-select[name=skills]').val();
                var project_owner = jQuery('.j-multiple-select[name=project_owner]').val();
                var queryObj = {
                    'action': 'search', 
                    'subaction': 'filter', 
                }
                console.log(jQuery(this));


                if(client != 'all' && client != null){
                    queryObj.client_name = client.join('|');
                }
                if(industry != 'all' && industry != null){
                    queryObj.client_industry = industry.join('|');
                }
                if(country != 'all' && country != null){
                    queryObj.country = country.join('|');
                }
                if(skills != 'all' && skills != null){
                    queryObj.skills = skills.join('|');
                }
                if(project_owner != 'all' && project_owner != null){
                    queryObj.project_owner = project_owner.join('|');
                }
                console.log(queryObj);

                <?php if(isset($_POST['project_name'])): ?>queryObj.project_name = '<?php echo $_POST['project_name'] ?>';<?php endif; ?>
                <?php if(isset($_POST['tag'])): ?>queryObj.tag = '<?php echo $_POST['tag'] ?>';<?php endif; ?>
                <?php if(isset($_POST['team'])): ?>queryObj.team = '<?php echo $_POST['team'] ?>';<?php endif; ?>
                
                startDateAttr = jQuery('#start').val().split('/')[2] + '-'+jQuery('#start').val().split('/')[1] +'-'+jQuery('#start').val().split('/')[0] 
                queryObj.start_date = startDateAttr;
                endDateAttr = jQuery('#end').val().split('/')[2] + '-'+jQuery('#end').val().split('/')[1] +'-'+jQuery('#end').val().split('/')[0] 
                queryObj.end_date = endDateAttr;


                jQuery.post(
                '/wp-admin/admin-ajax.php', queryObj,
                function(response) {
                    var respJSON = JSON.parse(response);
                    console.log('The server responded: ', respJSON);

                    for(var i in respJSON['data']){
                        var newItem = jQuery('.c-result-item.u-template').clone().removeClass('u-template');
                        var parser = new DOMParser;
                        var dom = parser.parseFromString( '<!doctype html><body>' + respJSON['data'][i]['project_name'],'text/html');
                        var decodedString = dom.body.textContent;
                        var projUrl = '?id='+respJSON['data'][i]['project_id'];
                        newItem.attr('href', '<?php echo $myLakePages['viewpage_url']?>'+projUrl);
                        newItem.find('.c-result-item__title strong').text(decodedString);
                        newItem.find('.c-result-item__client').text(respJSON['data'][i]['client_name']);
                        newItem.find('.c-result-item__poname').text(respJSON['data'][i]['email']);

                        newItem.appendTo('.c-results');
                    }
                    jQuery('.c-result-item.u-template').hide();

                    
                } );
                console.log(queryObj)
            });


            //CAMPI CHECKED
            <?php if(isset($_POST['client']) && $_POST['client'] != ''): ?>
                var str = '<?php echo($_POST['client']); ?>';
                var arr = str.split('|');     
                for(var c in arr){
                    jQuery('select[name=client]').val(arr);
                }
                jQuery('select[name=client]').trigger('change');

                jQuery('select[name=client]').siblings('span.select2').append("<span class='selected selected--default'>"+jQuery('select[name=client]').attr('data-datatype')+" ("+arr.length+")</span>").on('click',function(){
                    jQuery('select[name=client]').select2('open');
                });

            <?php endif; ?>
            <?php if(isset($_POST['industry']) && $_POST['industry'] != ''): ?>
                var str = '<?php echo($_POST['industry']); ?>';
                var arr = str.split('|');
                for(var c in arr){
                    jQuery('select[name=industry]').val(arr);
                }
                jQuery('select[name=industry]').trigger('change');

                jQuery('select[name=industry]').siblings('span.select2').append("<span class='selected selected--default'>"+jQuery('select[name=industry]').attr('data-datatype')+" ("+arr.length+")</span>").on('click',function(){
                    jQuery('select[name=industry]').select2('open');
                });

            <?php endif; ?>
            <?php if(isset($_POST['country']) && $_POST['country'] != ''): ?>
                var str = '<?php echo($_POST['country']); ?>';
                var arr = str.split('|');
                for(var c in arr){
                    jQuery('select[name=country]').val(arr);
                }
                jQuery('select[name=country]').trigger('change');

                jQuery('select[name=country]').siblings('span.select2').append("<span class='selected selected--default'>"+jQuery('select[name=country]').attr('data-datatype')+" ("+arr.length+")</span>").on('click',function(){
                    jQuery('select[name=country]').select2('open');
                });

            <?php endif; ?>
            <?php if(isset($_POST['skills']) && $_POST['skills'] != ''): ?>
                var str = '<?php echo($_POST['skills']); ?>';
                var arr = str.split('|');
                for(var c in arr){
                    jQuery('select[name=skills]').val(arr);
                }
                jQuery('select[name=skills]').trigger('change');

                jQuery('select[name=skills]').siblings('span.select2').append("<span class='selected selected--default'>("+arr.length+") "+jQuery('select[name=skills]').attr('data-datatype')+"</span>").on('click',function(){
                    jQuery('select[name=skills]').select2('open');
                });

            <?php endif; ?>
            <?php if(isset($_POST['client_owner']) && $_POST['client_owner'] != ''): ?>
                var str = '<?php echo($_POST['client_owner']); ?>';
                var arr = str.split('|');
                for(var c in arr){
                    jQuery('select[name=client_owner]').val(arr);
                }
                jQuery('select[name=client_owner]').trigger('change');

                jQuery('select[name=client_owner]').siblings('span.select2').append("<span class='selected selected--default'>("+arr.length+") "+jQuery('select[name=client_owner]').attr('data-datatype')+"</span>").on('click',function(){
                    jQuery('select[name=client_owner]').select2('open');
                });
            <?php endif; ?>

            //TRIGGER QUERY
            jQuery('select').trigger('change');
        });
    });
</script>



<?php if(isset($_POST['searchterm'])): ?>
<script>
    jQuery(document).ready(function($) {
        jQuery.post(
            '/wp-admin/admin-ajax.php', {
                'action': 'search',
                'subaction': 'fulltext',
                'search': '<?php if(isset($_POST['searchterm'])){ echo $_POST['searchterm']; } ?>'
            },
            function (response) {
                var respJSON = JSON.parse(response);
                console.log('The server responded: ', respJSON);

                for(var i in respJSON['data']){
                    var newItem = jQuery('.c-result-item.u-template').clone().removeClass('u-template');
                    var parser = new DOMParser;
                    var dom = parser.parseFromString( '<!doctype html><body>' + respJSON['data'][i]['project_name'],
    'text/html');
                    var decodedString = dom.body.textContent;

                    var projUrl = '?id='+respJSON['data'][i]['project_id'];
                        newItem.attr('href', '<?php echo $myLakePages['viewpage_url']?>'+projUrl);

                    newItem.find('.c-result-item__title strong').text(decodedString);
                    newItem.find('.c-result-item__client').text(respJSON['data'][i]['client_name']);
                    newItem.find('.c-result-item__poname').text(respJSON['data'][i]['email']);

                    newItem.appendTo('.c-results');
                }
                jQuery('.c-result-item.u-template').hide();
            }
        );
    });
</script>
<?php endif;?>

<header class="c-hero l-container">
    <div class="c-hero__content">
        <div class="c-hero__content__text">
            <h2 class="c-hero__content__text__lead t-title3">Results for</h2>
            <h1 class="c-hero__content__text__title t-title1"><?php if(isset($_POST['searchterm'])){ echo $_POST['searchterm']; } ?></h1>
        </div>                        
    </div>
</header>
<section class="c-content-block l-container">
    <div class="c-content-block__content c-advanced-search">
        <form class="c-form c-form--filter">
            <header class="c-form__header">
                <h3 class="c-form__header__title t-title4">Filter</h3>
            </header>
            <div class="c-form__content l-2col">
                <div class="c-form__field c-form__field--yellow">
                    <div class="c-form__field-inline c-multiple-select">
                        <h3 class="c-multiple-select__title">Clients <span class="icon">v</span></h3>
                        <div class="c-select-container">
                            <select class="c-select j-multiple-select" name="client" data-placeholder="All clients" data-datatype="Clients" data-select2-id="1" multiple="" tabindex="-1" aria-hidden="true">
                            </select>
                        </div>
                    </div> 

                    <div class="c-form__field-inline c-multiple-select">
                        <h3 class="c-multiple-select__title">Industries <span class="icon">v</span></h3>
                        <div class="c-select-container">
                            <select class="c-select j-multiple-select" name="industry" data-placeholder="All industries" data-datatype="Industries" data-select2-id="4" multiple="" tabindex="-1" aria-hidden="true">
                            </select>
                        </div>
                    </div> 
                </div>
                <div class="c-form__field c-form__field--yellow">
                    <div class="c-form__field-inline c-multiple-select">
                        <h3 class="c-multiple-select__title">Countries <span class="icon">v</span></h3>
                        <div class="c-select-container">
                            <select class="c-select j-multiple-select" name="country" data-placeholder="All countries" data-datatype="Countries" data-select2-id="7" multiple="" tabindex="-1" aria-hidden="true">
                            </select>
                        </div>
                    </div> 
                    <div class="c-form__field-inline c-multiple-select">
                        <h3 class="c-multiple-select__title">Business unit <span class="icon">v</span></h3>
                        <div class="c-select-container">
                            <select class="c-select j-multiple-select" name="skills" data-placeholder="All business unit" data-datatype="Business units" data-select2-id="10" multiple="" tabindex="-1" aria-hidden="true">
                            </select>
                        </div>
                    </div> 
                </div>
                
                <div class="c-form__field c-form__field--calendar">
                    <div class="c-form__field-inline c-multiple-select">
                        <h3 class="c-multiple-select__title">Client owner <span class="icon">v</span></h3>
                        <div class="c-select-container">
                            <select class="c-select j-multiple-select" name="client_owner" data-placeholder="Any client owner" data-datatype="Client owners" data-select2-id="13" multiple="" tabindex="-1" aria-hidden="true">
                            </select>
                        </div>
                    </div> 
                    
                    <div class="c-form__field-inline c-calendar">
                        <label for="start">From</label>
                            <input type="text" id="start" placeholder="dd/mm/yyyy">
                    </div>                                    
                </div>
                <div class="c-form__field c-calendar c-calendar--end">
                    <div class="c-form__field-inline">
                        <label for="end">To</label>
                        <input type="text" id="end" placeholder="dd/mm/yyyy">
                    </div>
                </div>
            </div>
            <footer class="c-form__footer">
                <a href="#" class="c-reset">Reset all </a>
                <button class="c-button c-button--small j-filter-toggle">View more <span class="icon icon-circle-arrow-down"></span><span class="icon icon-filter"></span></button>            </footer>
        </form>

        <div class="c-results">
            <a href="#" class="c-result-item u-template">
                <h3 class="c-result-item__title t-title4"><strong></strong></h3>
                <p><span class="c-result-item__client">Vodafone Italy</span>  |  <span class="c-result-item__poname">P.O. Name</span></p>
            </a>
        </div>
    </div>
</section>
<?php get_footer(); ?>