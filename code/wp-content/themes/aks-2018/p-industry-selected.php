<?php 
/* Template Name: Industry selected */
session_start();
require_once ABSPATH . '/script/pdocrud.php';
$projects = null;
if(isset($_SESSION["pdocrud_obj"])) {
    $projects = unserialize($_SESSION["pdocrud_obj"]);
}else {
    $projects = new PDOCrud();
}

if(isset($_SESSION['myLakePages'])) {
    $myLakePages = $_SESSION['myLakePages'];
}else{
    $myLakePages = $projects->loadMyLakePages();
    $_SESSION['myLakePages'] = $myLakePages;
}

get_header();
?>
<script>
    jQuery(document).ready(function($) {
        //DATI PAGINA
        var queryObj = {
            'action': 'search', 
            'subaction': 'filter', 
            'industry_selected' : '<?php echo $_GET["industry_selected"] ?>'
        }


        //PREPOPOLAMENTO FILTRI
        jQuery.post(
        '/wp-admin/admin-ajax.php', {
            'action': 'search',
            'subaction': 'hydrate'
        },
        function (response) {
            var respJSON = JSON.parse(response);
            for (var i in respJSON)
            {
                console.log(respJSON[i]['type_']);
                jQuery('select[name='+respJSON[i]['type_']+']').append('<option value="'+respJSON[i]['NAME']+'">'+respJSON[i]['NAME']+'</option>')
            }
            
            //INSERIRE CHANGE FILTRI
            jQuery('.j-multiple-select, input').on('change', function(e) {
                jQuery('.c-spinner').show();
                jQuery(".c-projects").css('opacity',0);

                jQuery('.c-project-item:not(.u-template)').remove();
                jQuery('.c-project-item.u-template').show();
                jQuery(".c-projects").trigger("ss-destroy");

                queryObj = {
                    'action': 'search', 
                    'subaction': 'view_industry',
                    'industry_selected' : '<?php echo $_GET["industry_selected"] ?>'
                }
                
                var client = jQuery('.j-multiple-select[name=client]').val();
                var country = jQuery('.j-multiple-select[name=country]').val();
                var skills = jQuery('.j-multiple-select[name=skills]').val();
                var client_owner = jQuery('.j-multiple-select[name=client_owner]').val();
                if(jQuery('#start').val()){
                    startDateAttr = jQuery('#start').val().split('/')[2] + '-'+jQuery('#start').val().split('/')[1] +'-'+jQuery('#start').val().split('/')[0] 
                    queryObj.start_date = startDateAttr;
                }
                if(jQuery('#end').val()){
                    endDateAttr = jQuery('#end').val().split('/')[2] + '-'+jQuery('#end').val().split('/')[1] +'-'+jQuery('#end').val().split('/')[0] 
                    queryObj.end_date = endDateAttr;
                }
                if(client != 'all' && client != null){
                    queryObj.client_name = client.join('|');
                }
                if(country != 'all' && country != null){
                    queryObj.country = country.join('|');
                }
                if(skills != 'all' && skills != null){
                    queryObj.skills = skills.join('|');
                }
                if(client_owner != 'all' && client_owner != null){
                    queryObj.client_owner = client_owner.join('|');
                }
                console.log(queryObj);
                jQuery.post(
                    '/wp-admin/admin-ajax.php', queryObj,
                    function(response) {
                        var respJSON = JSON.parse(response);
                        var industryProjects = 0
                        if(respJSON['data'][0]['project_name'] !== ""){
                            industryProjects=respJSON['data'].length;
                        }

                        jQuery('.j-count').text(industryProjects);
                        if(respJSON['data'][0]){
                            jQuery('.c-hero__content__text__title, .c-breadcrumbs__item.current-item').text(respJSON['data'][0]['industry']);
                        }else{
                            jQuery('.c-hero__content__text__title, .c-breadcrumbs__item.current-item').text('');
                        }
                        
                        console.log('The server responded: ', respJSON);
                        for(var i in respJSON['data']){
                            var newItem = jQuery('.c-project-item.u-template').clone().removeClass('u-template');
                            var parser = new DOMParser;
                            var dom = parser.parseFromString( '<!doctype html><body>' + respJSON['data'][i]['project_name'],'text/html');
                            var decodedString = dom.body.textContent;

                            newItem.attr('href', '<?php echo $myLakePages['viewpage_url']?>?id='+respJSON['data'][i]['project_id']);

                            newItem.find('.c-project-item__header__title').text(decodedString);
                            newItem.find('.c-project-item__header__lead').text(respJSON['data'][i]['client_name']);
                            newItem.find('.c-project-item__figure').append('<img src="https://placeimg.com/480/'+Math.floor((Math.random() * 1000) + 200)+'/tech">');

                            newItem.find('.c-project-item__footer p').text(respJSON['data'][i]['email']);

                            newItem.appendTo('.c-projects');
                        }
                        jQuery('.c-project-item.u-template').hide();

                        //MASONRY
                        if(industryProjects > 0){
                            jQuery('.c-projects').imagesLoaded( function() {
                                console.log('all images loaded');
                                jQuery('.c-projects').attr('style','');
                                jQuery('.c-spinner').hide();
                                if (window.innerWidth < 1024) {
                                    jQuery(".c-projects").shapeshift({
                                        enableDrag: false,
                                        columns: 2
                                    });
                                }
                                else {
                                    jQuery(".c-projects").shapeshift({
                                        enableDrag: false,
                                        columns: 4
                                    });
                                }                        
                            });
                        }else{
                            jQuery('.c-spinner').hide();
                        }
                    }
                );
            
        
            });
            jQuery('select[name=client]').trigger('change');

        });
    });
</script>


<header class="c-hero l-container">
                <div class="c-hero__content">
                    <div class="c-hero__content__text">
                        <div class="c-breadcrumbs">
                            <a class="c-breadcrumbs__item" href="<?php echo $myLakePages['loadpage_url']?>">Home</a>
                            <a class="c-breadcrumbs__item" href="<?php echo $myLakePages['industrypage_url']?>">Industry</a>
                            <a class="c-breadcrumbs__item current-item" href="#"></a>
                        </div>
                        
                        <h1 class="c-hero__content__text__title t-title1"></h1>
                        <h2 class="c-hero__content__text__lead t-title6">Last updated:</h2>

                    </div>
                </div>
            </header>
            <section class="c-content-block l-container">
                <div class="c-content-block__content c-advanced-search">
                    <form class="c-form c-form--filter">
                        <header class="c-form__header">
                            <h3 class="c-form__header__title t-title4">Filter</h3>
                        </header>
                        <div class="c-form__content l-2col">
                            <div class="c-form__field c-form__field--yellow">
                                <div class="c-form__field-inline c-multiple-select">
                                    <h3 class="c-multiple-select__title">Client <span class="icon icon-down"></span></h3>
                                    <div class="c-select-container">
                                        <select class="c-select j-multiple-select" name="client" data-placeholder="Client" data-datatype="Client" data-select2-id="1" multiple="" tabindex="-1" aria-hidden="true">
                                        </select>
                                    </div>
                                </div>

                                <div class="c-form__field-inline c-multiple-select">
                                    <h3 class="c-multiple-select__title">Country <span class="icon icon-down"></span></h3>
                                    <div class="c-select-container">
                                        <select class="c-select j-multiple-select" name="country" data-placeholder="Country" data-datatype="Country" data-select2-id="4" multiple="" tabindex="-1" aria-hidden="true">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="c-form__field c-form__field--yellow">
                                <div class="c-form__field-inline c-multiple-select">
                                    <h3 class="c-multiple-select__title">Skills <span class="icon icon-down"></span></h3>
                                    <div class="c-select-container">
                                        <select class="c-select j-multiple-select" name="skills" data-placeholder="Skills" data-datatype="Skills" data-select2-id="7" multiple="" tabindex="-1" aria-hidden="true">
                                        </select>
                                    </div>
                                </div>
                                <div class="c-form__field-inline c-multiple-select">
                                    <h3 class="c-multiple-select__title">Client Owner <span class="icon icon-down"></span></h3>
                                    <div class="c-select-container">
                                        <select class="c-select j-multiple-select" name="client_owner" data-placeholder="Client Owner" data-datatype="Client Owner" data-select2-id="10" multiple="" tabindex="-1" aria-hidden="true">
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="c-form__field c-form__field--calendar">
                                <div class="c-form__field-inline c-calendar">
                                    <label for="start">From</label>
                                    <input type="text" id="start" placeholder="dd/mm/yyyy">
                                </div>
                                <div class="c-form__field-inline c-calendar c-calendar--end">
                                    <label for="end">To</label>
                                    <input type="text" id="end" placeholder="dd/mm/yyyy">
                                </div>
                            </div>
                            
                        </div>
                        <footer class="c-form__footer">
                            <a href="#" class="c-reset">Reset all </a>
                            <button class="c-button c-button--small j-filter-toggle">View more <span class="icon icon-circle-arrow-down"></span><span class="icon icon-filter"></span></button>
                        </footer>
                    </form>

                    <div class="c-results">
                        <header class="c-results__header">
                            <h2 class="c-results__header__title c-results__header__title--default t-title3">Projects (<span class="j-count"></span>)</h2>
                            <a class="c-results__header__order" href="#">Order by A-Z<span class="icon icon-arrow-double"></span></a>
                        </header>
                        <div class="c-spinner">
                            <img src="<?php echo get_bloginfo('template_url');?>/img/loading.png" alt="">
                        </div>
                        <div class="c-projects" style="opacity:0;">
                                <a href="#" class="c-project-item u-template" >
                                    <figure class="c-project-item__figure">
                                    </figure>
                                    <header class="c-project-item__header">
                                        <h3 class="c-project-item__header__title t-title4"></h3>
                                        <h4 cslass="c-project-item__header__lead t-lead2"></h4>
                                    </header>
                                    <footer class="c-project-item__footer">
                                        <p></p>
                                    </footer>
                                </a>
                           
                            </div>

                        </div>
                </div>
            </section>

<?php get_footer(); ?>