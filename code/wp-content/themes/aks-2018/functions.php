<?php

define( 'THEME_URL', get_template_directory_uri() . '/' );
define( 'THEME_DIR', dirname(__FILE__).'/' );

require_once(THEME_DIR . 'lib/trap/scripts.php');
require_once(THEME_DIR . 'lib/alkemy/searchapi.php');

//MENU
add_theme_support( 'nav-menus' );
if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus( array('Primario' => __( 'Navigazione primaria') ) );
}


//THUMBNAILS
add_theme_support('post-thumbnails' );


function modify_jquery_version() {
    if (!is_admin()) {
        wp_deregister_script('jquery');
        wp_register_script('jquery',get_template_directory_uri() . '/js/jquery.min.js', false, '3.2.s');
        wp_enqueue_script('jquery');
    }
}
add_action('init', 'modify_jquery_version');

?>