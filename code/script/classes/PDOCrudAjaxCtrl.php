<?php

@session_start();

Class PDOCrudAjaxCtrl {

    public function handleRequest() {
        $instanceKey = $_REQUEST["pdocrud_instance"];
        $pdocrud = null;
        if(isset($_SESSION["pdocrud_sess"])){
            //echo "isSet";
            $pdocrud = unserialize($_SESSION["pdocrud_sess"][$instanceKey]);
        }else{
            /**
             * NOTA: accrocchio per la gestione dell'eccezione della sessione
             * se non dovesse funzionare modificare il metodo savePDOCrudObj (da public a private)
             */
            $pdocrud = new PDOCrud();
            $pdocrud->savePDOCrudObj();
            //echo "ajeje-handleRequest";
        }
        $action = $_POST["pdocrud_data"]["action"];
        $data = $_POST["pdocrud_data"];
        $post = $_POST;

        if (isset($_FILES))
            $post = array_merge($_FILES, $post);
        $data["post"] = $post;
        switch (strtoupper($action)) {
            case "VIEW":
                echo $pdocrud->render("VIEWFORM", $data);
                break;
            case "SORT":
                $data["action"] = "asc";
                echo $pdocrud->render("CRUD", $data);
                break;
            case "ASC":
                echo $pdocrud->render("CRUD", $data);
                break;
            case "DESC":
                echo $pdocrud->render("CRUD", $data);
                break;
            case "ADD":
                ?>
                <?php
                if(isset($_SESSION['function'])) {
                    if($_SESSION['function'] == 1 || $_SESSION['function'] == 2 || $_SESSION['function']==3) {
                        ?>
                        <script type="text/javascript">
                            document.getElementById('menu-item-41').style.display = 'inline-block';
                            document.getElementById('menu-item-146').style.display = 'none';
                        </script>
                        <?php
                    }
                }
                ?>
                <script type="text/javascript">
                    document.getElementById('filterContainer').style.display='none';
                    window.scrollTo(0,0);
                </script>
                <?php
                echo $pdocrud->render("INSERTFORM", $data);
                $this->enableAutoComplete();
                break;
            case "INSERT":
                $pdocrud->render("INSERT", $post);
                break;
            case "INSERT_BACK":
                $pdocrud->setBackOperation();
                $pdocrud->render("INSERT", $post);
                echo $pdocrud->render("CRUD", $data);
                break;
            case "BACK":
                $pdocrud->setBackOperation();
                echo $pdocrud->render("CRUD", $data);
                break;
            case "EDIT":
                $pdocrud->setInlineEdit(false);
                echo $pdocrud->render("EDITFORM", $data);
                $this->enableAutoComplete();
                break;
            case "INLINE_EDIT":
                $pdocrud->setBackOperation();
                $pdocrud->setInlineEdit(true);
                echo $pdocrud->render("EDITFORM", $data);
                break;
            case "ONEPAGEEDIT":
                $pdocrud->setInlineEdit(false);
                echo $pdocrud->render("ONEPAGE", $data);
                break;
            case "INLINE_BACK":
                $pdocrud->render("UPDATE", $post);
                echo $pdocrud->render("CRUD", $data);
                break;
            case "UPDATE":
                $pdocrud->render("UPDATE", $post);
                break;
            case "UPDATE_BACK":
                $pdocrud->setBackOperation();
                $pdocrud->render("UPDATE", $post);
                echo $pdocrud->render("CRUD", $data);
                break;
            case "DELETE":
                $pdocrud->render("DELETE", $data);
                echo $pdocrud->render("CRUD", $data);
                break;
            case "DELETE_SELECTED":
                $pdocrud->render("DELETE_SELECTED", $data);
                echo $pdocrud->render("CRUD", $data);
                break;
            case "PAGINATION":
                $pdocrud->currentPage($data["page"]);
                echo $pdocrud->render("CRUD", $data);
                break;
            case "RECORDS_PER_PAGE":
                $pdocrud->currentPage(1);
                $pdocrud->recordsPerPage($data["records"]);
                echo $pdocrud->render("CRUD", $data);
                break;
            case "SEARCH":
                $pdocrud->currentPage(1);
                echo $pdocrud->render("CRUD", $data);
                break;
            case "EXPORTTABLE":
                echo $pdocrud->render("EXPORTTABLE", $data);
                break;
            case "EXPORTFORM":
                $pdocrud->render("EXPORTFORM", $data);
                break;
            case "SWITCH":
                $pdocrud->render("SWITCH", $data);
                echo $pdocrud->render("CRUD", $data);
                break;
            case "BTNSWITCH":
                $pdocrud->render("BTNSWITCH", $data);
                echo $pdocrud->render("CRUD", $data);
                break;
            case "LOADDEPENDENT":
                echo $pdocrud->render("LOADDEPENDENT", $data);
                break;
            case "EMAIL" : echo $pdocrud->render("EMAIL", $post);
                break;
            case "SELECT":
                echo $pdocrud->render("CRUD", $data);
                break;
            case "SELECTFORM":
                echo $pdocrud->render("SELECT", $post);
                break;
            case "FILTER":
                $pdocrud->currentPage(1);
                $pdocrud->handleOperation("CRUD", $data);
                if(isset($_POST["scope"]) && $_POST["scope"]==="serp"){
                    $_SESSION["searched_pdocrud"] = serialize($pdocrud);
                }
                ?>
                <script>window.location.href='result';</script>
    <?php
//                else{
//                    $pdocrud->currentPage(1);
//                    echo $pdocrud->render("CRUD", $data);
//                }

                break;
            case "RELOAD":
                echo $pdocrud->render("CRUD", $data);
                break;
            case "SAVE_CRUD_TABLE_DATA":
                $pdocrud->render("SAVE_CRUD_DATA", $data);
                echo $pdocrud->render("CRUD", $data);
                break;
            case "RENDER_ADV_SEARCH":
                $pdocrud->currentPage(1);
                echo $pdocrud->render("CRUD", $data);
                break;
            case "DATE_RANGE_REPORT":
                $pdocrud->currentPage(1);
                echo $pdocrud->render("CRUD", $data);
                break;
            case "CLONE":
                echo $pdocrud->render("CLONEFORM", $data);
                break;
            default:
                break;
        }
    }

    private function enableAutoComplete(){
        ?>
        <script type="text/javascript">
            $("#project_sheets\\#\\$client_name").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../clientsearch",
                        type: "GET",
                        dataType: "json",
                        data: {
                            term: $("#project_sheets\\#\\$client_name").val()
                        },
                        success: function (data) {
                            response($.map(data, function (value, index) {
                                return {
                                    label: value["value"],
                                    value: value["value"]
                                }
                            }));
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                }
            });
            $("#project_sheets\\#\\$client_industry").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "../industriessearch",
                        type: "GET",
                        dataType: "json",
                        data: {
                            term: $("#project_sheets\\#\\$client_industry").val()
                        },
                        success: function (data) {
                            response($.map(data, function (value, index) {
                                return {
                                    label: value["value"],
                                    value: value["value"]
                                }
                            }));
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                        }
                    });
                },
                minLength: 3,
                select: function (event, ui) {
                }
            });
        </script>
        <?php
    }

}
