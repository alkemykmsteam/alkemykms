<?php


class AksProjectUtility
{

    public static function buildDataProject($source = array()){
        /**
         * NOTA: per l'update di un progetto bisogna inserire nella query di estrazione anche gli id delle varie
         * tabelle collegate (sheets_status, team_projects e le future tabelle per skills e tag)
         * poichè l'id va settato esplicitamente come condizione per l'aggiornamento
         */
        $data = array();
        $insertData = array();

        $data[AksColumns::ColumnProjectName] = $source[AksColumns::ColumnProjectName];

        $data[AksColumns::ColumnCountry] = $source[AksColumns::ColumnCountry];
        $data[AksColumns::ColumnEmail] = wp_get_current_user()->user_email; //$source[AksColumns::ColumnEmail];
        $data[AksColumns::ColumnClientId] = $source[AksColumns::ColumnClient];
        //$data[AksColumns::ColumnTag] = "#ajeje2";
        $data[AksColumns::ColumnSituation] = str_replace('\\', '', $source[AksColumns::ColumnSituation]);
        $data[AksColumns::ColumnProblem] = str_replace('\\', '',$source[AksColumns::ColumnProblem]);
        $data[AksColumns::ColumnComplicationSolution] = str_replace('\\', '',$source[AksColumns::ColumnComplicationSolution]);
        $data[AksColumns::ColumnResults] = str_replace('\\', '', $source[AksColumns::ColumnResults]);
        //$data[AksColumns::ColumnDeleted] = "N";
        $data[AksColumns::ColumnClientOwner] = $source[AksColumns::ColumnClientOwner];

        if(isset($source[AksColumns::ColumnProjectId]) && empty($source[AksColumns::ColumnProjectId]) === false) {
            $data[AksColumns::ColumnProjectId] = $source[AksColumns::ColumnProjectId];
            $data[AksColumns::ColumnLastUpdateDate] = date("Y-m-d");
            $data[AksColumns::ColumnOperationType] = $source[AksColumns::ColumnOperationType];
            //conversione date (in edit non arrivano negli appostiti campi hidden-date
            $date_array = explode("/",$source[AksColumns::ColumnStartDate]); // split the array
            $var_day = $date_array[0]; //day seqment
            $var_month = $date_array[1]; //month segment
            $var_year = $date_array[2]; //year segment
            $startDate = "$var_year-$var_month-$var_day"; // join them together
            //conversione date (in edit non arrivano negli appostiti campi hidden-date
            $date_array = explode("/",$source[AksColumns::ColumnEndDate]); // split the array
            $var_day = $date_array[0]; //day seqment
            $var_month = $date_array[1]; //month segment
            $var_year = $date_array[2]; //year segment
            $endDate = "$var_year-$var_month-$var_day"; // join them together

            $data[AksColumns::ColumnStartDate] = $startDate;
            $data[AksColumns::ColumnEndDate] = $endDate;
            //update || insert
            $pdoCrudData['action'] = "update";
        }else{
            $data[AksColumns::ColumnOperationType] = AksColumns::ColumnOperationTypeInsert;
            $currentDate = date("Y-m-d");
            $data[AksColumns::ColumnCreationDate] = $currentDate;
            $data[AksColumns::ColumnLastUpdateDate] = $currentDate;
            $data[AksColumns::ColumnStartDate] = $source[AksColumns::ColumnHiddenStartDate];
            $data[AksColumns::ColumnEndDate] = $source[AksColumns::ColumnHiddenEndDate];
            //update || insert
            $pdoCrudData['action'] = "insert";
        }

        $insertData[AksProjectTables::TableProjects] = $data;
        //projects team
        $data = array();
        $tags = explode('|', $source[AksColumns::ColumnHiddenTeam]);
        foreach($tags as $key=>$value) {
            $data[AksColumns::ColumnTeamProjectId][$key] = $tags[$key];
        }
        $insertData[AksProjectTables::TableTeamProjects] = $data;


        $insertData['pdocrud_data'] = $pdoCrudData;


        //projects tags
        $data = array();
        $tags = explode('|', $source[AksColumns::ColumnHiddenTag]);
        foreach($tags as $key=>$value) {
            $data[AksColumns::ColumnTag][$key] = $tags[$key];
        }
        $insertData[AksProjectTables::TableProjectsTags] = $data;

        //projects skills
        $data = array();
        $skills = array();
        $skillsArray = explode('|', $source[AksColumns::ColumnHiddenSkills]);
        foreach($skillsArray as $key=>$value) {
            $skills[$key] = $value;
        }
        $data[AksColumns::ColumnSkillId] = $skills;
        $insertData[AksProjectTables::TableProjectsSkills] = $data;

        return $insertData;
    }

}