<div class="row pdocrud-filters-container" data-objkey="<?php echo $objKey; ?>" >
    <div class="col-sm-12">
        <div class="col-sm-12" id="filterContainer">
        <?php if (isset($filters) && count($filters)) { ?>
            <div class="pdocrud-filters-options">
                <div class="pdocrud-filter-label">
<!--                    <b>Search parameters:<br></b>-->
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-9">
                        <?php echo $filters[AksFilterID::FullTextFilter]; ?>
                    </div>
                    <div class="col-sm-3">
                        <button type="button" id="idButtonSimpleSearch" >Search</button>
                    </div>
                    <div class="col-sm-12" id="advanced-search" style="display: none">
                        <div id="advanced-filters">
                        <div class="col-sm-10">
                            <b>Filtri ricerca avanzata</b>
                        </div>
                        <?php
                        echo '<div class="col-sm-6">';
                        echo $filters[AksFilterID::ProjectNameFilter];
                        echo '</div>';
                        echo '<div class="col-sm-6">';
                        echo $filters[AksFilterID::TagFilter];
                        echo '</div>';
                        echo '<div class="col-sm-3">';
                        echo $filters[AksFilterID::ClientFilter];
                        echo '</div>';
                        echo '<div class="col-sm-3">';
                        echo $filters[AksFilterID::IndustriesFilter];
                        echo '</div>';
                        echo '<div class="col-sm-3">';
                        echo $filters[AksFilterID::CountryFilter];
                        echo '</div>';
                        echo '<div class="col-sm-3">';
                        echo $filters[AksFilterID::BusinessUnitFilter];
                        echo '</div>';
                        echo '<div class="col-sm-3">';
                        echo $filters[AksFilterID::ProjectOwnerFilter];
                        echo '</div>';
                        echo '<div class="col-sm-3">';
                        echo $filters[AksFilterID::DateFromFilter];
                        echo '</div>';
                        echo '<div class="col-sm-3">';
                        echo $filters[AksFilterID::DateToFilter];
                        echo '</div>';
                        echo '<div class="col-sm-12">';
                        echo $filters[AksFilterID::TeamFilter];
                        echo '</div>';
                        ?>
                        </div>
                        <div class="pdocrud-filter-selected">
                            <span id ="idClearAll" class="pdocrud-filter-option-remove"><?php echo $lang["clear_all"] ?></span>
                            <button type="button" id="idButtonSearch">
                                <span><?php _e( 'Search' ); ?></span>
                            </button>
                        </div>
                    </div>

                </div>
            </div>
            <?php
        }
        ?>
        </div>
    </div>
    <div class="col-sm-12">
        <?php echo $data ?>
    </div>
</div>