<div class="component addrow pull-right">                    
    <div class="control-group">                        
        <div class="controls">
            <a class="pdocrud-actions pdocrud-button pdocrud-button-add-row" href="javascript:;" data-action="add_row">
                <i class="fa fa-plus-circle" aria-hidden="true"></i> <?php echo $lang["addMember"]; ?>
            </a>
        </div>
    </div>
</div>
<?php
$body = "";
//$index=1;
foreach ($data as $rows) {
    $header = "";
    $body .= "<tr>";
    foreach ($rows as $row) {
        //$row["element"] = str_replace('team_projects#$team[]', ''.$index, $row["element"]);
        $header .= "<th>" . $row["lable"] . $row["tooltip"] . "</th>";
        $body .= '<td>'. $row["element"].
        '<script>'.
            //'$("#'.$index.'").autocomplete({'.team_projects#$team[]
            //'$("#team_projects\\\#\\\$team\\\[\\\]").autocomplete({'.
            '$("#joinTable tr:last input").autocomplete({'.
                'source: function (request, response) {'.
                    '$.ajax({'.
                        'url: "../../wp-content/themes/aks/teamsearch",'.
                        'type: "GET",'.
                        'dataType: "json",'.
                        'data: {'.
                            //'term: $("#'.$index.'").val()'.
                            'term: $("#joinTable tr:last input").val()'.
                        '},'.
                        'success: function (data) {'.
                            'response($.map(data, function (value, index) {'.
                                'return {'.
                                    'label: value["value"],'.
                                    'value: value["value"]'.
                                '}'.
                            '}));'.
                        '},'.
                        'error: function (XMLHttpRequest, textStatus, errorThrown) {'.
                        '}'.
                    '});'.
                '},'.
                'minLength: 3,'.
                'select: function (event, ui) {'.
                '}'.
            '});'.
        '</script>'.
        '</td>';
    }
    $body .= ' <td class="text-right"><a href="javascript:;" class="pdocrud-actions" data-action="delete_row"><i class="fa fa-remove"></i> ' . '</a></td>';
    $body .= "</tr>";
   // $index++;
}
?>
<table class="table pdocrud-left-join responsive" id="joinTable">
    <thead class="bottom_add_project">
        <tr>
            <?php if (isset($header)) echo $header; ?>
        </tr>
    </thead>
    <tbody class="team_project">
        <?php if (isset($body)) echo $body; ?>
    </tbody>
</table>