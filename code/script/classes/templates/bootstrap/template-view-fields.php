<table class="pdocrud-table pdocrud-table-view table table-bordered table-striped table-condensed">
    <tbody class="table_view">
        <?php
        $tmpKey='';
        $isAllegati = false;
        $tagDivFieldOpen="<div  style=\"margin-left: -9%;margin-top: 4%;\" class='lab_new'>";
        $tagDivFieldOpenNoImg="<div style=\"margin-left: -2%;margin-top: 4%;\">";
        $tagDivValueOpen="<div style=\"margin-left: -2%;margin-bottom: 4%;\"  class='lab_new'>";
        $tagDivClosed="</div>";
        $trColumnClass="";
        $trValuenClass="";
        foreach ($data as $row) {


                ?>
                <tr <?php echo $trColumnClass ?> >
                    <td class="label_view">
                        <?php
                        if (isset($columns[$row["fieldName"]])) {
                            //echo '<b>'.$columns[$row["fieldName"]]["colname"].'</b>';
                            if (strlen($row["fieldName"]) > 0 && array_key_exists($row["fieldName"], $imgCatalog)) {
                                echo $tagDivFieldOpen.'<img src="/wp-content/uploads/2018/09/' . $imgCatalog[$row["fieldName"]] . '"/>  <b>' . $columns[$row["fieldName"]]["colname"] . '</b>'.$tagDivClosed;
                            }else {
                                echo $tagDivFieldOpenNoImg.'<b>' . $columns[$row["fieldName"]]["colname"] . '</b>'.$tagDivClosed;
                            }
                        }else {
                            echo $tagDivFieldOpen.'<b>' . $row["fieldName"] . '</b>'.$tagDivClosed;
                        }
                        if(strcmp($row["fieldName"], $columnAllegati)==0){
                            $isAllegati=true;
                        }
                        ?>
                    </td>

                </tr>
                <tr <?php echo  $trValuenClass ?>>
                    <td>
                <?php
                if(is_array($row)){
                        //echo $row["addOnBefore"];
                        //if($row["element"]!=null || $isAllegati == false) {
                            if($isAllegati){
                                //$file = explode(",", $row["element"]);
                                if(empty($alfrescoAttachments)){
                                    echo $tagDivValueOpen."-".$tagDivClosed;
                                }else {

                                    foreach ($alfrescoAttachments as $allegato) {
                                        echo $tagDivValueOpen . '<a href="' . $row["element"] . '" target="_blank" rel="noopener noreferrer" title="'. $allegato .'">' . (strlen($allegato) > $attachmentsCharactersLimit ? substr($allegato,0,$attachmentsCharactersLimit)."..." : $allegato) . '</a>' . $tagDivClosed . "<br/>";
                                    }
                                    $isAllegati = false;
                                }
                            }else
                            echo $tagDivValueOpen . wordwrap($row["element"], $characterLimit, "<br />\n") .$tagDivClosed;

                        //}else echo $tagDivValueOpen."-".$tagDivClosed;
                        //echo $row["addOnAfter"];
                }
                else {
                    echo "<div>".$row."</div>";
                }?>
                    </td>
                </tr>
                <?php
        }
        if (count($leftJoinData)) {
            $key = array_keys($leftJoinData);

            $loop = 0;
            foreach ($leftJoinData as $data) {
                foreach ($data as $rows) {
                    if ($loop === 0) {
                        $leftCols = array_keys($rows);
                        //rimuovo le colonne team_id e project_id (mantengo solo la colonna team)
                        unset($leftCols[0]);
                        unset($leftCols[count($leftCols)]);
                        ?>
                        <tr>
                                <?php
                                foreach ($leftCols as $col) {
                                    ?>
                                <td>
                                <?php if (isset($columns[$col]))
                                    echo $columns[$col]["colname"];
                                else {
                                    //echo '2-' . $col;
                                    //se la colonna è mappata nel catalogo immagini
                                    if(strlen($col)>0 && array_key_exists($col, $imgCatalog)) {
                                        //visualizzo la coppia immagine-nome colonna
                                        echo $tagDivFieldOpen.'<img src="/wp-content/uploads/2018/09/' . $imgCatalog[$col] . '"/>  <b>' . ucfirst($col) . '</b>'.$tagDivClosed;
                                        /*
                                         salvo il nome della colonna left join in una variabile
                                        */
                                        $tmpKey = $col;
                                    }
                                    else
                                        echo $tagDivFieldOpen.'<b>'.$col.'</b>'.$tagDivClosed;
                                }
                                ?>
                                </td>

                            <?php }
                            ?>
                        </tr> 
                            <?php } $loop++;
                            ?>
                    <tr>
                        <?php
                        //foreach ($rows as $row) {
                            ?>  
                            <td>
                        <?php echo $tagDivValueOpen.$rows[$tmpKey].$tagDivClosed;; ?>
                            </td>
                    <?php //}
                    ?>
                    </tr>
                        <?php
                    }
                }
            }
            ?>
    </tbody>
</table>
<table>
    <tfoot>
        <tr>
            <td  class="text-left">
<!--                <div class="pdocrud-action-buttons pdocrud-print-hide">-->
                    <?php if (isset($settings["viewDelButton"]) && $settings["viewDelButton"] === true) { ?>
                        <button class="mydelete btn btn-info pdocrud-form-control pdocrud-button pdocrud-view-delete" data-id="<?php echo $id;?>" data-action="delete" type="button"><?php echo $lang["delete"] ?></button>
                    <?php } ?>
                    <?php if (isset($settings["viewEditButton"]) && $settings["viewEditButton"] === true) { ?>
                        <button class="myedit btn btn-info pdocrud-form-control pdocrud-button pdocrud-view-edit" data-id="<?php echo $id;?>" data-action="edit" type="button"><?php echo $lang["edit"] ?></button>
                    <?php } ?>
<!--                </div>                         -->
            </td>
        </tr>
        <tr>
            <td  class="text-left">
                    <table>
                        <tr>
                    <?php
                    $printClass = "myprint btn btn-info pdocrud-form-control pdocrud-button pdocrud-view-print exclusive";
                    if (isset($settings["viewDwnldCsvButton"]) && $settings["viewDwnldCsvButton"] === true) {
                        $printClass = "myprint btn btn-info pdocrud-form-control pdocrud-button pdocrud-view-print";
                        ?>
                        <td>
                        <button data-action="exporttable" data-export-type="csv" class="mydownload btn btn-info btn-pdocrud-actions pdocrud-button pdocrud-button-export" data-objkey="<?php echo $objKey; ?>" type="button"><?php echo $lang["download"] ?></button>
                        </td>
                    <?php } ?>

                    <?php if (isset($settings["viewPrintButton"]) && $settings["viewPrintButton"] === true) {
//                        $printClass = "";
                        ?>
                        <td>
                        <button data-action="print" class="<?php echo $printClass; ?>" type="button"><?php echo $lang["print"] ?></button>
                        </td>
                    <?php } ?>
                        </tr>
                    </table>
            </td>
        </tr>
        <tr>
            <td  class="text-right">
<!--                <div class="pdocrud-action-buttons pdocrud-print-hide">-->
                    <!--<a title="<?php //echo $lang["go_to_search"]; ?>" class="pdocrud-button" href="./load" data-objkey="<?php //echo $objKey; ?>"><?php //echo $lang["go_to_search"]; ?></a>-->
                    <button class="mysearch_view btn btn-info pdocrud-form-control pdocrud-button pdocrud-view-gosearch" data-id="<?php echo $id;?>" data-action="back" type="button"><?php echo $lang["go_to_search"] ?></button>
<!--                </div>-->
            </td>
        </tr>
    </tfoot>

</table>