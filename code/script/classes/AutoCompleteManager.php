<?php

require_once ABSPATH . '/script/pdocrud.php';
/**
 * Created by PhpStorm.
 * User: cavaliere
 * Date: 09/10/18
 * Time: 16.46
 */
class AutoCompleteManager
{

    private $pdoCrud;
    private $queryParam;
    private $dbHost;
    private $dbUsername;
    private $dbPassword;
    private $dbName;
    private $db;
    private $output;


    public function __construct()
    {
        $this->pdoCrud = new PDOCrud();
    }

    public function setQueryParam($queryParam){
        $this->queryParam = $queryParam;
    }

    public function openConnection(){
        $this->dbHost     = $this->pdoCrud->getSettings("hostname");
        $this->dbUsername = $this->pdoCrud->getSettings("username");
        $this->dbPassword = $this->pdoCrud->getSettings("password");
        $this->dbName     = $this->pdoCrud->getSettings("database");
        //
        // Connect with the database
        $this->db = new mysqli($this->dbHost, $this->dbUsername, $this->dbPassword, $this->dbName);
        if($this->db->connect_error) {
            echo 'Database connection failed...' . 'Error: ' . $this->db->connect_errno . ' ' . $this->db->connect_error;
            exit;
        } else {
            $this->db->set_charset('utf8');
        }
    }

    public function makeClientQuery(){
        // Get matched data from skills table
        $query = $this->db->query("SELECT id, name FROM clients WHERE name LIKE '%".$this->queryParam."%' ORDER BY name ASC");
        // Generate skills data array
        $skillData = array();
        if($query->num_rows > 0){
            while($row = $query->fetch_assoc()){
                $data['id'] = $row['id'];
                $data['value'] = $row['name'];
                array_push($skillData, $data);
            }
        }
        // Return results as json encoded array
        $this->output = json_encode($skillData);
        flush();
        $this->db->close();

    }

    public function makeIndustriesQuery(){
        // Get matched data from skills table
        $query = $this->db->query("SELECT id, name FROM industries WHERE name LIKE '%".$this->queryParam."%' ORDER BY name ASC");
        // Generate skills data array
        $skillData = array();
        if($query->num_rows > 0){
            while($row = $query->fetch_assoc()){
                $data['id'] = $row['id'];
                $data['value'] = $row['name'];
                array_push($skillData, $data);
            }
        }
        // Return results as json encoded array
        $this->output = json_encode($skillData);
        flush();
        $this->db->close();
    }

    public function makeTeamQuery(){
        // Get matched data from skills table
        $query = $this->db->query("SELECT team_id, cognome_nome FROM team_members WHERE cognome_nome LIKE '%".$this->queryParam."%' ORDER BY cognome_nome ASC");
        $this->queryParam='';
        // Generate skills data array
        $skillData = array();
        if($query->num_rows > 0){
            while($row = $query->fetch_assoc()){
                $data['id'] = $row['team_id'];
                $data['value'] = $row['cognome_nome'];
                array_push($skillData, $data);
            }
        }
        // Return results as json encoded array
        $this->output = json_encode($skillData);
        flush();
        $this->db->close();
    }

    public function getOutput(){return $this->output;}

}