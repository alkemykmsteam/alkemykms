<?php

class AKSQuery
{

    public static $GET_TOTAL_PROJECTS='select count(*) as total from project_sheets';
    public static $GET_VIEWS_BY_PROJECT='select count(*) as total from sheets_operations where project_id = ';
    public static $GET_TOTAL_VIEWS_PROJECTS='select count(*) as views, date(data) as months from sheets_operations where operations = \'V\' group by date(data);';//TODO
    public static $GET_TOTAL_DOWNLOADS_PROJECTS='select count(*) as downloads, date(data) as months from sheets_operations where operations = \'D\' group by date(data);';//TODO
    public static $ALIAS_TOTAL_PROJECTS='total';
    public static $ALIAS_TOTAL_VIEWS='views';
    public static $ALIAS_TOTAL_DOWNLOADS='downloads';
    public static $ALIAS_TOTAL_MONTHS='months';

}