<?php

class AksPageLabels
{

    const LabelCountry = "Country (*)";
    //const LabelValue = "Value";
    const LabelStartDate = "Start date (*)";
    const LabelEndDate = "End date (*)";
    const LabelSkills = "Skills (*)";
    const LabelClientIndustries = "Client Industries (*)";
    const LabelStatus = "Status (*)";
    const LabelClientOwner = "Client Owner (*)";
    const LabelClientName = "Client Name (*)";
    const LabelAuthor = "Author (*)";
    const LabelProjectName = "Project Name (*)";
    const LabelTag = "Tag (*)";
    const LabelSituation = "Situation (*)";
    const LabelSolution = "Solution (*)";
    const LabelResults = "Results (*)";
    const LabelComplication = "Complication (*)";
    const LabelTeam = "Team (*)";
}
?>

