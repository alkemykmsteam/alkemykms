<?php

/**
 * Created by PhpStorm.
 * User: cavaliere
 * Date: 04/10/18
 * Time: 9.14
 */
require_once(__DIR__ . '/php-cmis-client-master/vendor/autoload.php');

Class AlfrescoAttachmentsRecover
{


    private $alfrescoFolderID;
    public static $REPOSITORY_ID_QUERY = "select repository_id as repo from project_sheets where project_id= ";
    public static $REPOSITORY_ID_ALIAS="repo";
    private $alfrescoParameters;
    private $alfrescoSession;
    private $settings;
    private $alfrescoHttpInvoker;
    private $pdocrudErrCtrl;
    private $uploadedAttachments;

    public function __construct()
    {
        $this->initializeSettings();
        $this->loadLangData();
        $this->pdocrudErrCtrl = new PDOCrudErrorCtrl();
    }

    public function initListFiles($projectID){

        $pdoModelObj = $this->getPDOModelObj();
        $result = $pdoModelObj->executeQuery($this::$REPOSITORY_ID_QUERY . $projectID);
        $this->alfrescoFolderID = $result[0][$this::$REPOSITORY_ID_ALIAS];
        //echo '-'.$result[0][$this->repositoryIdAlias].'-';
        if($this->alfrescoFolderID === null || strcmp($this->alfrescoFolderID, "0") == 0 || strcmp($this->alfrescoFolderID, "") ==0) {
            //echo '-ajeje-';
            $this->uploadedAttachments = array();
        }else{
            $this->loadAlfrescoProperties();
            $this->initAlfrescoCommonObjects();
            $sessionFactory = new \Dkd\PhpCmis\SessionFactory();
            // If no repository id is defined use the first repository
            if (CMIS_REPOSITORY_ID === null) {
                $repositories = $sessionFactory->getRepositories($this->alfrescoParameters);
                $this->alfrescoParameters[\Dkd\PhpCmis\SessionParameter::REPOSITORY_ID] = $repositories[0]->getId();
            } else {
                $this->alfrescoParameters[\Dkd\PhpCmis\SessionParameter::REPOSITORY_ID] = CMIS_REPOSITORY_ID;
            }
            $this->alfrescoSession = $sessionFactory->createSession($this->alfrescoParameters);
            // Get the root folder of the repository
            // $rootFolder = $session->getRootFolder();
            //creo l'ObjectID per recuperare i dati della cartella remota
            $objId = new Dkd\PhpCmis\DataObjects\ObjectId($this->alfrescoFolderID);

            $rootFolder = $this->alfrescoSession->getObject($objId);
            //$sessionFactory->
            $this->initUploadedAttachments($rootFolder);
        }
    }

    public function getAttachments()
    {
        return $this->uploadedAttachments;
    }

    private function loadAlfrescoProperties()
    {
        date_default_timezone_set($this->getSettings("defaultTimezoneSet"));
        define('CMIS_BROWSER_URL', $this->getSettings("alfrescoBrowserUrl"));
        define('CMIS_BROWSER_USER', $this->getSettings("alfrescoUser"));
        define('CMIS_BROWSER_PASSWORD', $this->getSettings("alfrescoPassword"));
        define('CMIS_REPOSITORY_ID', null);//e54b7ca2-c653-4dc2-9158-90b22117d8cd
    }

    private function initAlfrescoCommonObjects()
    {
        $this->alfrescoHttpInvoker = new \GuzzleHttp\Client(
            ['auth' =>
                [
                    CMIS_BROWSER_USER,
                    CMIS_BROWSER_PASSWORD
                ]
            ]);
        $this->alfrescoParameters = [
            \Dkd\PhpCmis\SessionParameter::BINDING_TYPE => \Dkd\PhpCmis\Enum\BindingType::BROWSER,
            \Dkd\PhpCmis\SessionParameter::BROWSER_URL => CMIS_BROWSER_URL,
            \Dkd\PhpCmis\SessionParameter::BROWSER_SUCCINCT => false,
            \Dkd\PhpCmis\SessionParameter::HTTP_INVOKER_OBJECT => $this->alfrescoHttpInvoker,
        ];
        $this->alfrescoSessionFactory = new \Dkd\PhpCmis\SessionFactory();
        // If no repository id is defined use the first repository
        if (CMIS_REPOSITORY_ID === null) {
            $this->alfrescoRepositories = $this->alfrescoSessionFactory->getRepositories($this->alfrescoParameters);
            $this->alfrescoParameters[\Dkd\PhpCmis\SessionParameter::REPOSITORY_ID] = $this->alfrescoRepositories[0]->getId();
        } else {
            $this->alfrescoParameters[\Dkd\PhpCmis\SessionParameter::REPOSITORY_ID] = CMIS_REPOSITORY_ID;
        }
        $this->alfrescoSession = $this->alfrescoSessionFactory->createSession($this->alfrescoParameters);
    }

    private function initUploadedAttachments(\Dkd\PhpCmis\Data\FolderInterface $folder)
    {
        $i = 0;
        foreach ($folder->getChildren() as $children) {
            if ($children instanceof \Dkd\PhpCmis\Data\DocumentInterface) {
                $this->uploadedAttachments[$i] = $children->getName();
                //echo '-> '.$this->uploadedAttachments[$i].'<br/>';
                $i++;
            }
        }
    }

    /**
     * Get particular configuaration setting
     * @param    string $setting Config Key for which setting needs to be retreived
     * return    mixed                          Configuaration setting value
     */
    private function getSettings($setting)
    {
        if (isset($this->settings[$setting]))
            return $this->settings[$setting];
        else
            return $this->getLangData("no_settings_found");
    }

    /**
     * Return language data
     * @param   string $param Get data for language
     * return   sting                                     language translation for the parameter
     */
    private function getLangData($param)
    {
        if (isset($this->langData[$param]))
            return $this->langData[$param];
        return $param;
    }

    /**
     * Initialize Settings when object of class created, from the config.php settings
     */
    private function initializeSettings()
    {
        global $config;
        $this->settings = $config;
        $this->fileUploadPath = $this->settings["uploadFolder"];
        $this->fileSavePath = $this->settings["downloadFolder"];
        $this->currentLang = $this->settings["lang"];
        $this->currentpage = 1;
    }

    private function loadLangData()
    {
        $file = PDOCrudABSPATH . '/languages/' . $this->currentLang . '.ini';
        if (!file_exists($file)) {
            $this->currentLang = "en";
            $file = PDOCrudABSPATH . '/languages/' . $this->currentLang . '.ini';
        }

        $this->langData = parse_ini_file($file);
    }

    private function getPDOModelObj()
    {
        $pdoModelObj = new PDOModel();
        $pdoModelObj->setErrorCtrl($this->pdocrudErrCtrl);
        if ($pdoModelObj->connect($this->settings["hostname"], $this->settings["username"], $this->settings["password"], $this->settings["database"], $this->settings["dbtype"], $this->settings["characterset"])) {
            return $pdoModelObj;
        } else {
            $this->addError($this->getLangData("db_connection_error"));
            die();
        }
    }
    private function addError($error)
    {
        return $this->pdocrudErrCtrl->addError($error, false);
    }



    
}