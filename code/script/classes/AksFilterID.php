<?php

class AksFilterID {

    const FullTextFilter = "fullTextFilter";
    const ProjectNameFilter ="projectNameFilter";
    const TagFilter ="tagFilter";
    const ClientFilter = "clientFilter";
    const IndustriesFilter = "industriesFilter";
    const CountryFilter = "countryFilter";
    const BusinessUnitFilter = "businessUnitFilter";
    const ProjectOwnerFilter = "projectOwnerFilter";
    const DateFromFilter = "dateFromFilter";
    const DateToFilter = "dateToFilter";
    const TeamFilter = "teamFilter";
}

?>