<?php

class AksColumns{
    const ColumnProjectId = "project_id";
    const ColumnGhost="fulltext";
    const ColumnProjectName = "project_name";

    const ColumnClientId = "client_id";
    const ColumnStartDate = "start_date";
    const ColumnEndDate = "end_date";
    const ColumnCreationDate = "creation_date";

    const ColumnClientIndustryId = "client_industry_id";
    const ColumnCountry = "country";
    const ColumnStatus = "status";
    const ColumnEmail = "email";
    const ColumnClientOwner = "client_owner";
    const ColumnTag = "tag";
    const ColumnProjectTagId = "project_tag_id";
    const ColumnSkills = "skills";
    const ColumnSkillId = "skill_id";
    const ColumnTeam = "team";
    const ColumnSituation = "situation";
    const ColumnProblem = "complication";
    const ColumnComplicationSolution = "solution";
    const ColumnResults = "results";
    const ColumnAllegati = "attachments";
    const ColumnRepositoryID = "repository_id";
    const ColumnDeleted = "deleted";
    const ColumnLastUpdateDate="last_update_date";
    const ColumnOperationType="operation_type";
    const ColumnOperationTypeInsert="I";
    const ColumnOperationTypeBozza="B";
    const ColumnOperationTypeApproval="A";
    const ColumnOperationTypeDelete="D";
    //private $columnValue = "value";

    const ColumnSheetStatusId="sheet_status_id";
    const ColumnTeamProjectId="team_id";

    //i seguenti campi NON sono colonne sul db ma vengono usati a scopo di utilità
    const OperationType = "operationType";
    const ColumnClient = "client";
    const ColumnIndustry = "industry";
    const ColumnHiddenTag = "hidden-tag";
    const ColumnHiddenSkills = "hidden-skills";
    const ColumnHiddenTeam = "hidden-team";
    const ColumnHiddenStartDate = "hidden-start_date";
    const ColumnHiddenEndDate = "hidden-end_date";
}

?>