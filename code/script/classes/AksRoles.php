<?php

class AksRoles{
    /*
     *
     */
    const WpAdmin = "administrator";

    /*
     Ha privilegi di scrittura e può:
       - inserire schede
       - salvare le sue schede in bozza
       - pubblicare le sue schede
       - annullare la pubblicazione delle sue schede
       - editare schede scritte da editor di cui arriva la notifica
       - salvare come bozza schede scritte da editor di cui arriva la notifica
       - pubblicare schede scritte da editor di cui arriva la notifica
       - annullare la pubblicazione di schede scritte da editor di cui arriva la notifica

     */
    const MyLakePublisher = "mylakepublisher";

    /*
     * Ha privilegi in scrittura e può inserire schede, salvarle in bozza e inviarle per approvazione.
     * L'approvazione dovrà essere inviata al Client Owner (da individuarsi tra VP e Industry leader)  inserito nella scheda.
     */
    const MyLakeEditor = "mylakeeditor";

    /*
     * Non ha privilegi di scrittura e può navigare la piattaforma e vedere le schede inserite.
     */
    const MyLakeVisitor = "mylakevisitor";


}