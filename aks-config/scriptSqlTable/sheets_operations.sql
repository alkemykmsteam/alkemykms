-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Creato il: Nov 14, 2018 alle 16:41
-- Versione del server: 5.7.24
-- Versione PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mydb`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `sheets_operations`
--

CREATE TABLE `sheets_operations` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `operations` char(1) NOT NULL COMMENT 'V=view, D=download',
  `data` datetime NOT NULL,
  `username` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `sheets_operations`
--
/*
INSERT INTO `sheets_operations` (`id`, `project_id`, `operations`, `data`, `username`) VALUES
(1, 24, 'V', '2018-11-14 14:32:33', 'admin'),
(2, 26, 'V', '2018-11-14 16:02:16', 'admin'),
(3, 7, 'D', '2018-11-14 16:58:07', 'admin');
*/
--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `sheets_operations`
--
ALTER TABLE `sheets_operations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sheets_operation_proj_id` (`project_id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `sheets_operations`
--
ALTER TABLE `sheets_operations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `sheets_operations`
--
ALTER TABLE `sheets_operations`
  ADD CONSTRAINT `fk_sheets_operation_proj_id` FOREIGN KEY (`project_id`) REFERENCES `project_sheets` (`project_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
