DELIMITER //
CREATE TRIGGER `tr_sheets_creation` AFTER INSERT ON `project_sheets`
 FOR EACH ROW BEGIN
insert into sheets_creation (project_id, creation_date) values (new.project_id, now());
END//
DELIMITER ;