-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: mydb
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `project_sheets`
--

DROP TABLE IF EXISTS `project_sheets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_sheets` (
  `project_name` varchar(500) NOT NULL,
  `client_name` varchar(500) NOT NULL,
  `client_industry` varchar(200) NOT NULL COMMENT 'TMT, Consumer Goods &amp;amp; Retail, Financial Services, Energy &amp;amp; Utilities, Fashion &amp;amp; Luxury, Industrial, Pharma, Services, altro',
  `country` varchar(200) NOT NULL COMMENT 'Italia, Spagna, SEE, altro',
  `client_owner` varchar(200) NOT NULL COMMENT 'VP o Industry Leader',
  `skills` text COMMENT 'Strategy, Communication, Technology, eCommerce, Performance, Digital Factory, Data, Play',
  `tag` text,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `email` varchar(200) NOT NULL,
  `situation` text,
  `complication` text,
  `solution` text,
  `results` text,
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `attachments` text,
  `repository_id` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`project_id`),
  UNIQUE KEY `PROJ_SHTS_NAME_UNIQ_IDX` (`project_name`),
  FULLTEXT KEY `PROJ_SHTS_NAMETAG_FULLT` (`project_name`,`client_name`,`client_industry`,`country`,`email`,`client_owner`,`tag`,`skills`,`situation`,`complication`,`solution`,`results`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_sheets`
--

LOCK TABLES `project_sheets` WRITE;
/*!40000 ALTER TABLE `project_sheets` DISABLE KEYS */;
INSERT INTO `project_sheets` VALUES ('D-All Project','MISE','Other','Spain','Industry Leader','Strategy,Technology,Digital Factory','Big Data2','2017-01-05','2019-12-23','a.andreuzzi@alkemytech.it','Il progetto intende sfruttare in modo innovativo e operativo le nuove tecniche sperimentali di gestione della grande produzione di informazioni della rete: la grande maggioranza dei contenuti online è prodotta dagli utenti. La funzionalità che ne deriva dall’enorme quantità di dati \"human generated\", quali posts, tweets, feeds, immagini, video, blog (5 Exabytes di dati prodotti ogni 2 giorni), a cui si aggiungono quelli provenienti da sensori, smart object, wearable device e, in generale, da componenti dell’Internet of Everything, è quella di estrarre conoscenze specifiche per la produzione di innovativi strumenti d’indagine e lo sviluppo di nuovi sistemi di gestione integrati, trasversali e multimediali.','D-All nasce dall\'esigenza da parte di amministrazioni, aziende, privati di gestire e ottimizzare la grande mole di dati che collezionano o a cui possono avere accesso. I vantaggi che i clienti finali si aspettano di ottenere dalla piattaforma sono: \r\n1) arricchire i proprio dati con fonti eterogenee raccolte dalla Big Data Platform: \r\nUser Footprints\r\nSocial Media Data\r\nWearable Data\r\nOpen Data\r\nConnected devices data\r\nIoT data\r\nIndustry data\r\n2) Conoscere in dettaglio il proprio target e offrire soluzioni personalizzate, prodotti vincenti e iniziative sicure. \r\n3) Creare valore e vantaggio competitivo attraverso gli algoritmi con cui vengono elaborati i Big Data raccolti:\r\nBig Data Analytics \r\nComplex Network Analysis\r\nDeep Learning/AI per il riconoscimento di contenuti visuali\r\nNatural language processing\r\nModelli di User Experience e Data Visualization omnichannel\r\n4)Approfittare di un sistema sicuro e per la raccolta, l’analisi e l’elaborazione di dati\r\nAttraverso le tecniche e gli algoritmi di anonimizzazione, data posting, data fusion e di identity matching studiate e sviluppate all’interno del progetto D-All, i clienti potranno approfittare di un layer sicuro per lo scambio dei dati in formato anonimo, perfettamente in linea con la nuova normativa europea del GDPR e arricchite da nuove tecnologie abilitanti per l’universo della sicurezza come la blockchain e l’Internet of Value. \r\n','Il progetto D-All costruisce una piattaforma innovativa di Big Data Insight e Predictive Analysis che, aggregando dati provenienti da fonti eterogenee, permetta di realizzare soluzioni Industry 4.0 basate su Big Data Analysis &amp; Digital Presence, Internet Of Things, Artificial Intelligence, Deep Learning e Decision-Making. L’obiettivo della piattaforma D-All è quello di creare un’alleanza basata sullo scambio di dati e tecnologie così da estrarre nuove conoscenze e valore competitivo.  \r\n','Il risultato finale del progetto può essere sintetizzato nei seguenti componenti e servizi che ciascun singolo partner intende patrimonializzare e commercializzare:\r\nSeri Jakala:\r\n1) Piattaforma Big Data analytics per Loyalty &amp; Customer Engagement services\r\n2) Data monetization platform\r\n3) Audience 360° - data enrichment\r\n4) Audience propensity service\r\n5) Hyper-targeting service\r\n6) Audience buying service\r\nAlkemy:\r\n1) Big Data Enhanced Suite - Suite full stack per AI, Dataviz, Real Time Reporting\r\n2) Artificial Intelligence Platform\r\n3) Real Time Reporting Platform\r\n4) Dataviz Platform\r\n5) Data Ingestion Platform\r\n6) Operation as a service  line su BIG Data Suite / Platform modules\r\n\r\nSubcom:\r\n1) PaaS Dashboarding Big Data Analytics Suite Platform \r\n2) Servizi VAS - Privacy and Security service line\r\n3) Servizi VAS - IoT OS monitoring service line\r\n\r\n\r\nIl risultato finale atteso del progetto è dunque una prodotto prototipale composto da diversi componenti correlati e interoperabili attraverso modelli di interoperabilità standard e apposite API di scambio. \r\n',7,'http://192.168.165.70:8080/share/page/repository#filter=path%7C%2FD-All Project','69345735-2ac7-49b5-8d7c-405e823fbb5e'),('trigga','Sky Italia Srl','Consumer Goods &amp; Retail','Spain','VP','Performance','1','2018-11-01','2018-11-21','a@b.c','2','3','4','5',26,'http://192.168.165.70:8080/share/page/repository#filter=path%7C%2Ftrigga','');
/*!40000 ALTER TABLE `project_sheets` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-12 14:58:50
