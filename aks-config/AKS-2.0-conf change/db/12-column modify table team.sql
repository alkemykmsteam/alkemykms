ALTER TABLE `mydb`.`team_projects` 
DROP FOREIGN KEY `fk_team_projects_teamid`;
ALTER TABLE `mydb`.`team_projects` 
CHANGE COLUMN `team_id` `team_id` INT(11) NOT NULL ;
ALTER TABLE `mydb`.`team_projects` 
ADD CONSTRAINT `fk_team_projects_teamid`
  FOREIGN KEY (`team_id`)
  REFERENCES `mydb`.`team_members` (`team_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
--
ALTER TABLE `mydb`.`team_projects` DROP COLUMN `team`;


