insert into sheets_status (status, project_id, validation_date)
select 'Published', project_id, now() from project_sheets 
where EXTRACT(YEAR FROM creation_date) < 2019
order by project_id desc;
