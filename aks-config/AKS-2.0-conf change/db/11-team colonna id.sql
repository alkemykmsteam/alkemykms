-- TODO FARE MOLTA ATTENZIONE...qualcosa non quadra con i risultati COGNOME_NOME / NOME_COGNOME
--
ALTER TABLE `team_projects` ADD `team_project_id` INT NULL ;

update team_projects set team_project_id = team_id;

UPDATE team_projects a INNER JOIN team_members b ON a.team = b.cognome_nome SET a.team_id = b.team_id;
-- TODO prima di droppare la colonna "team" da team_projects, abbiamo valori team_id null...occorre bonificarli
ALTER TABLE `mydb`.`team_projects` 
CHANGE COLUMN `team_id` `team_id` INT(11) NOT NULL ,
CHANGE COLUMN `team_project_id` `team_project_id` INT(11) NOT NULL AUTO_INCREMENT ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`team_project_id`);
--
ALTER TABLE `mydb`.`team_projects` DROP COLUMN `team_id`;
--
ALTER TABLE `mydb`.`team_projects` 
ADD COLUMN `team_id` INT NULL AFTER `team_project_id`;
--
ALTER TABLE `mydb`.`team_projects` 
ADD INDEX `fk_team_projects_teamid_idx` (`team_id` ASC);
ALTER TABLE `mydb`.`team_projects` 
ADD CONSTRAINT `fk_team_projects_teamid`
  FOREIGN KEY (`team_id`)
  REFERENCES `mydb`.`team_members` (`team_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
--
UPDATE team_projects a INNER JOIN team_members b ON a.team = b.cognome_nome SET a.team_id = b.team_id;
--
ALTER TABLE `mydb`.`team_members` DROP INDEX `mail` ;
--
insert into team_members (cognome_nome, job_title, mail, phone, office, company) 
select distinct team, 'TBD', 'TBD', 'TBD', 'TBD', 'TBD' from team_projects 
where team_id is null 
and team not in ('Michela Noè, Francesco Pozza', 'NICCOLò PAGOTTO',
'NICCOLO\' PAGOTTO', 'Rossana La Rosa, Nicola Gotti, Leonora Giovanazzi, Angelo Sanvito, Nazzareno Squadroni')
order by team asc;
--
-- 2a iterazione
UPDATE team_projects a INNER JOIN team_members b ON a.team = b.cognome_nome SET a.team_id = b.team_id;
--TODO verificare correttezza id
/*
select * from team_projects 
where team_id is null 
and team in ('Michela Noè, Francesco Pozza', 'NICCOLò PAGOTTO',
'NICCOLO\' PAGOTTO', 'Rossana La Rosa, Nicola Gotti, Leonora Giovanazzi, Angelo Sanvito, Nazzareno Squadroni');
*/
update team_projects
set team = (select cognome_nome from team_members where cognome_nome like '%PAGOTTO%'),
team_id = (select team_id from team_members where cognome_nome like '%PAGOTTO%')
 where team_project_id=510;
--
update team_projects
set team = (select cognome_nome from team_members where cognome_nome like '%PAGOTTO%'),
team_id = (select team_id from team_members where cognome_nome like '%PAGOTTO%')
 where team_project_id=517;
--
delete from team_projects where team_project_id=642;
insert into team_projects (team, project_id, team_id)
select cognome_nome, 112, team_id from team_members where 
cognome_nome like ('%La Rosa%')-- , 'Nicola Gotti', 'Leonora Giovanazzi', 'Angelo Sanvito', 'Nazzareno Squadroni');
or cognome_nome like ('%Gotti%')
or cognome_nome like ('%Giovanazzi%')
or cognome_nome like ('%Sanvito%')
or cognome_nome like ('%Squadroni%');
--
delete from team_projects where team_project_id=726;
insert into team_projects (team, project_id, team_id)
select cognome_nome, 122, team_id from team_members where 
cognome_nome like ('%Noè%')-- , 'Nicola Gotti', 'Leonora Giovanazzi', 'Angelo Sanvito', 'Nazzareno Squadroni');
or cognome_nome like ('%Pozza%');


