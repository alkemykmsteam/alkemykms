CREATE TABLE `mydb`.`notifications` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `project_id` INT NOT NULL,
  `readed` VARCHAR(1) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_notifications_project_id_idx` (`project_id` ASC),
  CONSTRAINT `fk_notifications_project_id`
    FOREIGN KEY (`project_id`)
    REFERENCES `mydb`.`project_sheets` (`project_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
--
-- ALTER TABLE project_sheets MODIFY creation_date DATETIME;
ALTER TABLE `mydb`.`project_sheets` CHANGE COLUMN `creation_date` `creation_date` DATETIME NULL DEFAULT NULL ;
--
ALTER TABLE `mydb`.`notifications` 
DROP FOREIGN KEY `fk_notifications_project_id`;
--
ALTER TABLE `mydb`.`notifications` 
ADD CONSTRAINT `fk_notifications_project_id`
  FOREIGN KEY (`project_id`)
  REFERENCES `mydb`.`project_sheets` (`project_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
--
ALTER TABLE `mydb`.`projects_tags` 
DROP FOREIGN KEY `fk_projects_tags_proj_id`;
--
ALTER TABLE `mydb`.`projects_tags` 
ADD CONSTRAINT `fk_projects_tags_proj_id`
  FOREIGN KEY (`project_id`)
  REFERENCES `mydb`.`project_sheets` (`project_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
--
ALTER TABLE `mydb`.`projects_skills` 
DROP FOREIGN KEY `fk_proj_skills_pid`;
--
ALTER TABLE `mydb`.`projects_skills` 
ADD CONSTRAINT `fk_proj_skills_pid`
  FOREIGN KEY (`project_id`)
  REFERENCES `mydb`.`project_sheets` (`project_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
--
ALTER TABLE `mydb`.`projects_skills` 
DROP FOREIGN KEY `proj_skills_skid`;
ALTER TABLE `mydb`.`projects_skills` 
ADD CONSTRAINT `proj_skills_skid`
  FOREIGN KEY (`skill_id`)
  REFERENCES `mydb`.`skills` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
--
ALTER TABLE `mydb`.`project_sheets` 
ADD INDEX `fk_project_client_id_idx` (`client_id` ASC);
ALTER TABLE `mydb`.`project_sheets` 
ADD CONSTRAINT `fk_project_client_id`
  FOREIGN KEY (`client_id`)
  REFERENCES `mydb`.`clients` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
--
ALTER TABLE `mydb`.`project_sheets` 
ADD INDEX `fk_project_industry_id_idx` (`client_industry_id` ASC);
ALTER TABLE `mydb`.`project_sheets` 
ADD CONSTRAINT `fk_project_industry_id`
  FOREIGN KEY (`client_industry_id`)
  REFERENCES `mydb`.`industries` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
--
DELIMITER //
CREATE TRIGGER `tr_sheets_notification` AFTER INSERT ON `project_sheets`
 FOR EACH ROW BEGIN
insert into notifications (project_id, readed) values (new.project_id, 'N');
END//
DELIMITER ;
