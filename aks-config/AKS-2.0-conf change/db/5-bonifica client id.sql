--DDL
ALTER TABLE mydb.project_sheets ADD COLUMN client_id INT NULL AFTER client_name;
ALTER TABLE mydb.project_sheets ADD COLUMN client_industry_id INT NULL AFTER client_industry;


-- DML
UPDATE project_sheets a INNER JOIN clients b ON a.client_name = b.name SET a.client_id = b.id;
UPDATE project_sheets a INNER JOIN industries b ON a.client_industry = b.name SET a.client_industry_id = b.id;
--

-- bonifica client mancanti
insert into clients (name) select distinct client_name 
from project_sheets 
where client_id is null
order by client_name asc;
--
-- verifica:
select * from clients order by name asc;
--
UPDATE project_sheets a INNER JOIN clients b ON a.client_name = b.name SET a.client_id = b.id;
--
-- bonifica industry mancanti
--
-- verifica:
select * from industries order by name asc;
--
insert into industries (name) select distinct client_industry
from project_sheets 
where client_industry_id is null
order by client_industry asc;
--
UPDATE project_sheets a INNER JOIN industries b ON a.client_industry = b.name SET a.client_industry_id = b.id;

