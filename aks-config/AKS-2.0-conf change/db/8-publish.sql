ALTER TABLE `mydb`.`sheets_status` 
ADD COLUMN `validation_date` DATETIME NULL AFTER `project_id`;
--
delete from sheets_status where LOWER(status) != LOWER('Validated') or status is null;
