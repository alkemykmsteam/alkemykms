--DDL
CREATE TABLE `mydb`.`projects_tags` (
  `project_tag_id` INT NOT NULL AUTO_INCREMENT,
  `project_id` INT NOT NULL,
  `tag` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`project_tag_id`),
  INDEX `fk_projects_tags_proj_id_idx` (`project_id` ASC),
  CONSTRAINT `fk_projects_tags_proj_id`
    FOREIGN KEY (`project_id`)
    REFERENCES `mydb`.`project_sheets` (`project_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

-- migrazione dati pre-esistenti
insert into projects_tags (tag, project_id)
SELECT DISTINCT(LTRIM(LOWER(SUBSTRING_INDEX(SUBSTRING_INDEX(REPLACE(tag, ';', ','), ',', n.n), ',', -1)))) name, t.project_id
FROM project_sheets t CROSS JOIN
(SELECT a.N + b.N * 10 + 1 n FROM
(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9 UNION ALL SELECT 10) a ,
(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9 UNION ALL SELECT 10) b
ORDER BY n ) n
WHERE n.n <= 1 + (LENGTH(t.tag) - LENGTH(REPLACE(REPLACE(tag, ';', ','), ',', '')));
--
ALTER TABLE project_sheets DROP COLUMN tag;
