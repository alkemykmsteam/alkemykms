ALTER TABLE `mydb`.`project_sheets` 
ADD COLUMN `last_update_date` VARCHAR(45) NULL AFTER `client_industry_id`,
ADD COLUMN `operation_type` VARCHAR(45) NULL COMMENT 'Tipo operazione:\nB: bozza, I: insert, A: richiesta publish, D: delete' AFTER `last_update_date`;
--
CREATE TABLE `mydb`.`operation_project` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `project_id` INT NOT NULL,
  `operation_type` CHAR(1) NOT NULL COMMENT 'Tipo operazione: B: bozza, I: insert, A: richiesta publish, D: delete',
  `operation_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_operation_project_proj_id_idx` (`project_id` ASC),
  CONSTRAINT `fk_operation_project_proj_id`
    FOREIGN KEY (`project_id`)
    REFERENCES `mydb`.`project_sheets` (`project_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
--
ALTER TABLE `mydb`.`operation_project` 
ADD COLUMN `user` VARCHAR(45) NULL AFTER `operation_date`;
--
--
DELIMITER //
CREATE PROCEDURE UPDATE_PROJECT_OPERATION(IN project_id INT, IN operation_date DATETIME, IN operation_type CHAR(1), IN user VARCHAR(100))
BEGIN
	insert into operation_project (project_id, operation_type, operation_date, user) values (project_id, operation_type, operation_date, user);
END //
DELIMITER ;
--
--
DELIMITER //
CREATE TRIGGER `tr_project_insert` AFTER INSERT ON `project_sheets`
 FOR EACH ROW BEGIN
CALL UPDATE_PROJECT_OPERATION(new.project_id, new.last_update_date, new.operation_type, new.email);
END//
DELIMITER ;
--
DELIMITER //
CREATE TRIGGER `tr_project_update` AFTER UPDATE ON `project_sheets`
 FOR EACH ROW BEGIN
CALL UPDATE_PROJECT_OPERATION(new.project_id, new.last_update_date, new.operation_type, new.email);
END//
DELIMITER ;
--
DROP TRIGGER tr_sheets_notification;
--
/*DELIMITER //
CREATE TRIGGER `tr_sheets_notification` AFTER INSERT ON `operation_project`
 FOR EACH ROW BEGIN
IF(new.operation_type like 'A') THEN
	insert into notifications (project_id, readed) values (new.project_id, 'N');
END IF;
END//
DELIMITER ;*/
DELIMITER //
CREATE TRIGGER `tr_sheets_notification` AFTER INSERT ON `project_sheets`
 FOR EACH ROW BEGIN
insert into notifications (project_id, readed) values (new.project_id, 'N');
END//
DELIMITER ;
