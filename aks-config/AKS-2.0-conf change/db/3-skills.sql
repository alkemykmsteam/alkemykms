--DDL
CREATE TABLE `mydb`.`skills` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id`));
--
CREATE TABLE `mydb`.`projects_skills` (
  `project_id` INT NOT NULL,
  `skill_id` INT NOT NULL,
  PRIMARY KEY (`project_id`, `skill_id`),
  INDEX `proj_skills_skid_idx` (`skill_id` ASC),
  CONSTRAINT `fk_proj_skills_pid`
    FOREIGN KEY (`project_id`)
    REFERENCES `mydb`.`project_sheets` (`project_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `proj_skills_skid`
    FOREIGN KEY (`skill_id`)
    REFERENCES `mydb`.`skills` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
--
--DML
insert into mydb.skills(name) values ('Communication');
insert into mydb.skills(name) values ('Data');
insert into mydb.skills(name) values ('Digital Factory');
insert into mydb.skills(name) values ('eCommerce');
insert into mydb.skills(name) values ('Performance');
insert into mydb.skills(name) values ('Play');
insert into mydb.skills(name) values ('Strategy');
insert into mydb.skills(name) values ('Technology');
--
-- migrazione dati pre-esistenti
insert into projects_skills (project_id, skill_id)
SELECT 
-- DISTINCT(SUBSTRING_INDEX(SUBSTRING_INDEX(t.skills, ',', n.n), ',', -1)) name, 
t.project_id, s.id
FROM project_sheets t 
CROSS JOIN 
(SELECT a.N + b.N * 10 + 1 n 
FROM 
(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) a ,
(SELECT 0 AS N UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 UNION ALL SELECT 8 UNION ALL SELECT 9) b 
ORDER BY n ) n
INNER JOIN skills s on s.name = (SUBSTRING_INDEX(SUBSTRING_INDEX(t.skills, ',', n.n), ',', -1))
WHERE n.n <= 1 + (LENGTH(t.skills) - LENGTH(REPLACE(t.skills, ',', ''))) 
ORDER BY NAME;
--
ALTER TABLE project_sheets DROP COLUMN skills;

													
