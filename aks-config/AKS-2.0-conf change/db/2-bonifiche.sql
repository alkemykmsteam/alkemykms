--
-- bonifica caratteri speciali
-- project_name
UPDATE mydb.project_sheets
SET project_name = REPLACE(project_name, '&amp;', '&')
WHERE project_name LIKE '%&amp;%';
-- client_name
UPDATE mydb.project_sheets
SET client_name = REPLACE(client_name, '&amp;', '&')
WHERE client_name LIKE '%&amp;%';
-- tag
UPDATE mydb.project_sheets
SET tag = REPLACE(tag, '&amp;', '&')
WHERE tag LIKE '%&amp;%';
-- client_industry
UPDATE mydb.project_sheets
SET client_industry = REPLACE(client_industry, '&amp;', '&')
WHERE client_industry LIKE '%&amp;%';
--
UPDATE mydb.project_sheets
SET client_industry = TRIM(client_industry);
-- situation
UPDATE mydb.project_sheets
SET situation = REPLACE(situation, '&amp;', '&')
WHERE situation LIKE '%&amp;%';
-- complication
UPDATE mydb.project_sheets
SET complication = REPLACE(complication, '&amp;', '&')
WHERE complication LIKE '%&amp;%';
-- results
UPDATE mydb.project_sheets
SET results = REPLACE(results, '&amp;', '&')
WHERE results LIKE '%&amp;%';
-- solution
UPDATE mydb.project_sheets
SET solution = REPLACE(solution, '&amp;', '&')
WHERE solution LIKE '%&amp;%';
--
