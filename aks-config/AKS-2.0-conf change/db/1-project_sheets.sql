ALTER TABLE `project_sheets` ADD `creation_date` DATE NULL ;
ALTER TABLE `project_sheets` ADD `deleted` CHAR(1) NULL DEFAULT 'N' ;
--
select a.project_id, b.project_id, a.creation_date, b.creation_date FROM project_sheets a INNER JOIN sheets_creation b ON a.project_id = b.project_id;
UPDATE project_sheets SET creation_date = STR_TO_DATE('31/10/2018','%d/%m/%Y') where creation_date is null;
--
DROP TRIGGER IF EXISTS tr_sheets_creation;
drop table if exists sheets_creation;