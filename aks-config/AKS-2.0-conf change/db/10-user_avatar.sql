CREATE TABLE `mydb`.`mylake_user_info` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_user` BIGINT(20) UNSIGNED NOT NULL,
  `avatar` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_mylake_user_id_idx` (`id_user` ASC),
  CONSTRAINT `fk_mylake_user_id`
    FOREIGN KEY (`id_user`)
    REFERENCES `mydb`.`wp_users` (`ID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

--
